-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2018 at 05:44 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketplace`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE `api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'IQRaJL9mnT4l9MjVqN5rVhjlpvkrpt127aDFHm9OcQhhwIAaixn7Wgc8x1yO4jwdnWJQa6P64ObpCMvzhbzO66xJOaDIZ2W4x19UbgbcxubVHfCFiwCBBUJmGuUK0RdGPhsQfZlEdWgi6E7Jn06yMqkxTQLLjXlGQctpFXhTcD8B4zgE28U0Mw80JXNrSZ45aiJ9XiMAeXNOMSrgAWBCPz2YQHGgcMw7SWy35NvZeO5WrRfdE4AiyonaWxFPkkZm', 1, '2018-11-07 22:29:43', '2018-11-07 22:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `api_ip`
--

CREATE TABLE `api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_session`
--

CREATE TABLE `api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE `attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_description`
--

CREATE TABLE `attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_description`
--

INSERT INTO `attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(4, 1, 'test 1'),
(5, 1, 'test 2'),
(6, 1, 'test 3'),
(7, 1, 'test 4'),
(8, 1, 'test 5'),
(9, 1, 'test 6'),
(10, 1, 'test 7'),
(11, 1, 'test 8'),
(3, 1, 'Clockspeed');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group`
--

CREATE TABLE `attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_group`
--

INSERT INTO `attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group_description`
--

CREATE TABLE `attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_group_description`
--

INSERT INTO `attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`, `status`) VALUES
(6, 'HP Products', 1),
(7, 'Home Page Slideshow', 1),
(8, 'Manufacturers', 1);

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

CREATE TABLE `banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_image`
--

INSERT INTO `banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(79, 7, 1, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/demo/banners/iPhone6.jpg', 0),
(87, 6, 1, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(94, 8, 1, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(95, 8, 1, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(96, 8, 1, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(91, 8, 1, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(92, 8, 1, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(93, 8, 1, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(88, 8, 1, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(89, 8, 1, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(90, 8, 1, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(80, 7, 1, 'MacBookAir', '', 'catalog/demo/banners/MacBookAir.jpg', 0),
(97, 8, 1, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(98, 8, 1, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(25, '', 0, 1, 1, 3, 1, '2009-01-31 01:04:25', '2011-05-30 12:14:55'),
(27, '', 20, 0, 0, 2, 1, '2009-01-31 01:55:34', '2010-08-22 06:32:15'),
(20, 'catalog/demo/compaq_presario.jpg', 0, 1, 1, 1, 1, '2009-01-05 21:49:43', '2011-07-16 02:14:42'),
(24, '', 0, 1, 1, 5, 1, '2009-01-20 02:36:26', '2011-05-30 12:15:18'),
(18, 'catalog/demo/hp_2.jpg', 0, 1, 0, 2, 1, '2009-01-05 21:49:15', '2011-05-30 12:13:55'),
(17, '', 0, 1, 1, 4, 1, '2009-01-03 21:08:57', '2011-05-30 12:15:11'),
(28, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:12', '2010-08-22 06:32:46'),
(26, '', 20, 0, 0, 1, 1, '2009-01-31 01:55:14', '2010-08-22 06:31:45'),
(29, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:37', '2010-08-22 06:32:39'),
(30, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:59', '2010-08-22 06:33:00'),
(31, '', 25, 0, 0, 1, 1, '2009-02-03 14:17:24', '2010-08-22 06:33:06'),
(32, '', 25, 0, 0, 1, 1, '2009-02-03 14:17:34', '2010-08-22 06:33:12'),
(33, '', 0, 1, 1, 6, 1, '2009-02-03 14:17:55', '2011-05-30 12:15:25'),
(34, 'catalog/demo/ipod_touch_4.jpg', 0, 1, 4, 7, 1, '2009-02-03 14:18:11', '2011-05-30 12:15:31'),
(35, '', 28, 0, 0, 0, 1, '2010-09-17 10:06:48', '2010-09-18 14:02:42'),
(36, '', 28, 0, 0, 0, 1, '2010-09-17 10:07:13', '2010-09-18 14:02:55'),
(37, '', 34, 0, 0, 0, 1, '2010-09-18 14:03:39', '2011-04-22 01:55:08'),
(38, '', 34, 0, 0, 0, 1, '2010-09-18 14:03:51', '2010-09-18 14:03:51'),
(39, '', 34, 0, 0, 0, 1, '2010-09-18 14:04:17', '2011-04-22 01:55:20'),
(40, '', 34, 0, 0, 0, 1, '2010-09-18 14:05:36', '2010-09-18 14:05:36'),
(41, '', 34, 0, 0, 0, 1, '2010-09-18 14:05:49', '2011-04-22 01:55:30'),
(42, '', 34, 0, 0, 0, 1, '2010-09-18 14:06:34', '2010-11-07 20:31:04'),
(43, '', 34, 0, 0, 0, 1, '2010-09-18 14:06:49', '2011-04-22 01:55:40'),
(44, '', 34, 0, 0, 0, 1, '2010-09-21 15:39:21', '2010-11-07 20:30:55'),
(45, '', 18, 0, 0, 0, 1, '2010-09-24 18:29:16', '2011-04-26 08:52:11'),
(46, '', 18, 0, 0, 0, 1, '2010-09-24 18:29:31', '2011-04-26 08:52:23'),
(47, '', 34, 0, 0, 0, 1, '2010-11-07 11:13:16', '2010-11-07 11:13:16'),
(48, '', 34, 0, 0, 0, 1, '2010-11-07 11:13:33', '2010-11-07 11:13:33'),
(49, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:04', '2010-11-07 11:14:04'),
(50, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:23', '2011-04-22 01:16:01'),
(51, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:38', '2011-04-22 01:16:13'),
(52, '', 34, 0, 0, 0, 1, '2010-11-07 11:16:09', '2011-04-22 01:54:57'),
(53, '', 34, 0, 0, 0, 1, '2010-11-07 11:28:53', '2011-04-22 01:14:36'),
(54, '', 34, 0, 0, 0, 1, '2010-11-07 11:29:16', '2011-04-22 01:16:50'),
(55, '', 34, 0, 0, 0, 1, '2010-11-08 10:31:32', '2010-11-08 10:31:32'),
(56, '', 34, 0, 0, 0, 1, '2010-11-08 10:31:50', '2011-04-22 01:16:37'),
(57, '', 0, 1, 1, 3, 1, '2011-04-26 08:53:16', '2011-05-30 12:15:05'),
(58, '', 52, 0, 0, 0, 1, '2011-05-08 13:44:16', '2011-05-08 13:44:16');

-- --------------------------------------------------------

--
-- Table structure for table `category_description`
--

CREATE TABLE `category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_description`
--

INSERT INTO `category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(28, 1, 'Monitors', '', 'Monitors', '', ''),
(33, 1, 'Cameras', '', 'Cameras', '', ''),
(32, 1, 'Web Cameras', '', 'Web Cameras', '', ''),
(31, 1, 'Scanners', '', 'Scanners', '', ''),
(30, 1, 'Printers', '', 'Printers', '', ''),
(29, 1, 'Mice and Trackballs', '', 'Mice and Trackballs', '', ''),
(27, 1, 'Mac', '', 'Mac', '', ''),
(26, 1, 'PC', '', 'PC', '', ''),
(17, 1, 'Software', '', 'Software', '', ''),
(25, 1, 'Components', '', 'Components', '', ''),
(24, 1, 'Phones &amp; PDAs', '', 'Phones &amp; PDAs', '', ''),
(20, 1, 'Desktops', '&lt;p&gt;\r\n	Example of category description text&lt;/p&gt;\r\n', 'Desktops', 'Example of category description', ''),
(35, 1, 'test 1', '', 'test 1', '', ''),
(36, 1, 'test 2', '', 'test 2', '', ''),
(37, 1, 'test 5', '', 'test 5', '', ''),
(38, 1, 'test 4', '', 'test 4', '', ''),
(39, 1, 'test 6', '', 'test 6', '', ''),
(40, 1, 'test 7', '', 'test 7', '', ''),
(41, 1, 'test 8', '', 'test 8', '', ''),
(42, 1, 'test 9', '', 'test 9', '', ''),
(43, 1, 'test 11', '', 'test 11', '', ''),
(34, 1, 'MP3 Players', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', 'MP3 Players', '', ''),
(18, 1, 'Laptops &amp; Notebooks', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', 'Laptops &amp; Notebooks', '', ''),
(44, 1, 'test 12', '', 'test 12', '', ''),
(45, 1, 'Windows', '', 'Windows', '', ''),
(46, 1, 'Macs', '', 'Macs', '', ''),
(47, 1, 'test 15', '', 'test 15', '', ''),
(48, 1, 'test 16', '', 'test 16', '', ''),
(49, 1, 'test 17', '', 'test 17', '', ''),
(50, 1, 'test 18', '', 'test 18', '', ''),
(51, 1, 'test 19', '', 'test 19', '', ''),
(52, 1, 'test 20', '', 'test 20', '', ''),
(53, 1, 'test 21', '', 'test 21', '', ''),
(54, 1, 'test 22', '', 'test 22', '', ''),
(55, 1, 'test 23', '', 'test 23', '', ''),
(56, 1, 'test 24', '', 'test 24', '', ''),
(57, 1, 'Tablets', '', 'Tablets', '', ''),
(58, 1, 'test 25', '', 'test 25', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `category_filter`
--

CREATE TABLE `category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_path`
--

CREATE TABLE `category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_path`
--

INSERT INTO `category_path` (`category_id`, `path_id`, `level`) VALUES
(25, 25, 0),
(28, 25, 0),
(28, 28, 1),
(35, 25, 0),
(35, 28, 1),
(35, 35, 2),
(36, 25, 0),
(36, 28, 1),
(36, 36, 2),
(29, 25, 0),
(29, 29, 1),
(30, 25, 0),
(30, 30, 1),
(31, 25, 0),
(31, 31, 1),
(32, 25, 0),
(32, 32, 1),
(20, 20, 0),
(27, 20, 0),
(27, 27, 1),
(26, 20, 0),
(26, 26, 1),
(24, 24, 0),
(18, 18, 0),
(45, 18, 0),
(45, 45, 1),
(46, 18, 0),
(46, 46, 1),
(17, 17, 0),
(33, 33, 0),
(34, 34, 0),
(37, 34, 0),
(37, 37, 1),
(38, 34, 0),
(38, 38, 1),
(39, 34, 0),
(39, 39, 1),
(40, 34, 0),
(40, 40, 1),
(41, 34, 0),
(41, 41, 1),
(42, 34, 0),
(42, 42, 1),
(43, 34, 0),
(43, 43, 1),
(44, 34, 0),
(44, 44, 1),
(47, 34, 0),
(47, 47, 1),
(48, 34, 0),
(48, 48, 1),
(49, 34, 0),
(49, 49, 1),
(50, 34, 0),
(50, 50, 1),
(51, 34, 0),
(51, 51, 1),
(52, 34, 0),
(52, 52, 1),
(58, 34, 0),
(58, 52, 1),
(58, 58, 2),
(53, 34, 0),
(53, 53, 1),
(54, 34, 0),
(54, 54, 1),
(55, 34, 0),
(55, 55, 1),
(56, 34, 0),
(56, 56, 1),
(57, 57, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_to_layout`
--

CREATE TABLE `category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_to_store`
--

CREATE TABLE `category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_to_store`
--

INSERT INTO `category_to_store` (`category_id`, `store_id`) VALUES
(17, 0),
(18, 0),
(20, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_category`
--

CREATE TABLE `coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_history`
--

CREATE TABLE `coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_product`
--

CREATE TABLE `coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.61250001, 1, '2014-09-25 14:40:00'),
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, 1, '2018-11-07 16:38:43'),
(3, 'Euro', 'EUR', '', '€', '2', 0.78460002, 1, '2014-09-25 14:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_activity`
--

CREATE TABLE `customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_affiliate`
--

CREATE TABLE `customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_approval`
--

CREATE TABLE `customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

CREATE TABLE `customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_group`
--

INSERT INTO `customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_group_description`
--

CREATE TABLE `customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_group_description`
--

INSERT INTO `customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `customer_history`
--

CREATE TABLE `customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_ip`
--

CREATE TABLE `customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_login`
--

CREATE TABLE `customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_online`
--

CREATE TABLE `customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reward`
--

CREATE TABLE `customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_search`
--

CREATE TABLE `customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction`
--

CREATE TABLE `customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_wishlist`
--

CREATE TABLE `customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field`
--

CREATE TABLE `custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_customer_group`
--

CREATE TABLE `custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_description`
--

CREATE TABLE `custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_value`
--

CREATE TABLE `custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_value_description`
--

CREATE TABLE `custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download_description`
--

CREATE TABLE `download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'amazon_login', 'catalog/controller/account/logout/after', 'extension/module/amazon_login/logout', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `extension`
--

CREATE TABLE `extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extension`
--

INSERT INTO `extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'module', 'advanced_grid'),
(43, 'module', 'blog_category'),
(44, 'module', 'blog_latest'),
(45, 'module', 'blog_popular'),
(46, 'module', 'blog_tags'),
(47, 'module', 'blog_related_post'),
(48, 'module', 'blog_search'),
(49, 'module', 'breadcrumb_background_image'),
(50, 'module', 'camera_slider'),
(51, 'module', 'carousel_item'),
(52, 'module', 'category_wall'),
(53, 'module', 'cookie'),
(54, 'module', 'custom_module'),
(55, 'module', 'faq'),
(56, 'module', 'fastor'),
(57, 'module', 'filter_product'),
(58, 'module', 'full_screen_background_slider'),
(59, 'module', 'header_notice'),
(60, 'module', 'megamenu_manager_links'),
(61, 'module', 'megamenu'),
(62, 'module', 'newsletter'),
(63, 'module', 'popup'),
(64, 'module', 'product_blocks'),
(65, 'module', 'product_questions'),
(66, 'module', 'product_tabs'),
(67, 'module', 'revslideropencart'),
(68, 'module', 'revolution_slider'),
(69, 'module', 'divido_calculator'),
(70, 'module', 'google_hangouts'),
(71, 'module', 'html'),
(72, 'module', 'klarna_checkout_module'),
(73, 'module', 'latest'),
(74, 'module', 'pp_login'),
(75, 'module', 'amazon_login');

-- --------------------------------------------------------

--
-- Table structure for table `extension_install`
--

CREATE TABLE `extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `extension_path`
--

CREATE TABLE `extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE `filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_description`
--

CREATE TABLE `filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_group`
--

CREATE TABLE `filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_group_description`
--

CREATE TABLE `filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `geo_zone`
--

CREATE TABLE `geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geo_zone`
--

INSERT INTO `geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `information_description`
--

CREATE TABLE `information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information_description`
--

INSERT INTO `information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n', 'About Us', '', ''),
(5, 1, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 1, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(6, 1, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', 'Delivery Information', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `information_to_layout`
--

CREATE TABLE `information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `information_to_store`
--

CREATE TABLE `information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information_to_store`
--

INSERT INTO `information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `layout`
--

CREATE TABLE `layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `layout`
--

INSERT INTO `layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search');

-- --------------------------------------------------------

--
-- Table structure for table `layout_module`
--

CREATE TABLE `layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `layout_module`
--

INSERT INTO `layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(67, 1, 'carousel.29', 'content_top', 3),
(66, 1, 'slideshow.27', 'content_top', 1),
(65, 1, 'featured.28', 'content_top', 2),
(72, 3, 'category', 'column_left', 1),
(73, 3, 'banner.30', 'column_left', 2);

-- --------------------------------------------------------

--
-- Table structure for table `layout_route`
--

CREATE TABLE `layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `layout_route`
--

INSERT INTO `layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(44, 3, 0, 'product/category'),
(42, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search');

-- --------------------------------------------------------

--
-- Table structure for table `length_class`
--

CREATE TABLE `length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `length_class`
--

INSERT INTO `length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `length_class_description`
--

CREATE TABLE `length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `length_class_description`
--

INSERT INTO `length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'HTC', 'catalog/demo/htc_logo.jpg', 0),
(6, 'Palm', 'catalog/demo/palm_logo.jpg', 0),
(7, 'Hewlett-Packard', 'catalog/demo/hp_logo.jpg', 0),
(8, 'Apple', 'catalog/demo/apple_logo.jpg', 0),
(9, 'Canon', 'catalog/demo/canon_logo.jpg', 0),
(10, 'Sony', 'catalog/demo/sony_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer_to_store`
--

CREATE TABLE `manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturer_to_store`
--

INSERT INTO `manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `marketing`
--

CREATE TABLE `marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modification`
--

CREATE TABLE `modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{\"name\":\"Category\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(29, 'Home Page', 'carousel', '{\"name\":\"Home Page\",\"banner_id\":\"8\",\"width\":\"130\",\"height\":\"100\",\"status\":\"1\"}'),
(28, 'Home Page', 'featured', '{\"name\":\"Home Page\",\"product\":[\"43\",\"40\",\"42\",\"30\"],\"limit\":\"4\",\"width\":\"200\",\"height\":\"200\",\"status\":\"1\"}'),
(27, 'Home Page', 'slideshow', '{\"name\":\"Home Page\",\"banner_id\":\"7\",\"width\":\"1140\",\"height\":\"380\",\"status\":\"1\"}'),
(31, 'Banner 1', 'banner', '{\"name\":\"Banner 1\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE `option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `option_description`
--

CREATE TABLE `option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option_description`
--

INSERT INTO `option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `option_value`
--

CREATE TABLE `option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option_value`
--

INSERT INTO `option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `option_value_description`
--

CREATE TABLE `option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option_value_description`
--

INSERT INTO `option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE `order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_option`
--

CREATE TABLE `order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_recurring`
--

CREATE TABLE `order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_recurring_transaction`
--

CREATE TABLE `order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_shipment`
--

CREATE TABLE `order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired');

-- --------------------------------------------------------

--
-- Table structure for table `order_total`
--

CREATE TABLE `order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_voucher`
--

CREATE TABLE `order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(28, 'Product 1', '', '', '', '', '', '', '', 939, 7, 'catalog/demo/htc_touch_hd_1.jpg', 5, 1, '100.0000', 200, 9, '2009-02-03', '146.40000000', 2, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 16:06:50', '2011-09-30 01:05:39'),
(29, 'Product 2', '', '', '', '', '', '', '', 999, 6, 'catalog/demo/palm_treo_pro_1.jpg', 6, 1, '279.9900', 0, 9, '2009-02-03', '133.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, 0, '2009-02-03 16:42:17', '2011-09-30 01:06:08'),
(30, 'Product 3', '', '', '', '', '', '', '', 7, 6, 'catalog/demo/canon_eos_5d_1.jpg', 9, 1, '100.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 16:59:00', '2011-09-30 01:05:23'),
(31, 'Product 4', '', '', '', '', '', '', '', 1000, 6, 'catalog/demo/nikon_d300_1.jpg', 0, 1, '80.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, 0, '2009-02-03 17:00:10', '2011-09-30 01:06:00'),
(32, 'Product 5', '', '', '', '', '', '', '', 999, 6, 'catalog/demo/ipod_touch_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 17:07:26', '2011-09-30 01:07:22'),
(33, 'Product 6', '', '', '', '', '', '', '', 1000, 6, 'catalog/demo/samsung_syncmaster_941bw.jpg', 0, 1, '200.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 17:08:31', '2011-09-30 01:06:29'),
(34, 'Product 7', '', '', '', '', '', '', '', 1000, 6, 'catalog/demo/ipod_shuffle_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 18:07:54', '2011-09-30 01:07:17'),
(35, 'Product 8', '', '', '', '', '', '', '', 1000, 5, '', 0, 0, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 18:08:31', '2011-09-30 01:06:17'),
(36, 'Product 9', '', '', '', '', '', '', '', 994, 6, 'catalog/demo/ipod_nano_1.jpg', 8, 0, '100.0000', 100, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 18:09:19', '2011-09-30 01:07:12'),
(40, 'product 11', '', '', '', '', '', '', '', 970, 5, 'catalog/demo/iphone_1.jpg', 8, 1, '101.0000', 0, 9, '2009-02-03', '10.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 21:07:12', '2011-09-30 01:06:53'),
(41, 'Product 14', '', '', '', '', '', '', '', 977, 5, 'catalog/demo/imac_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 21:07:26', '2011-09-30 01:06:44'),
(42, 'Product 15', '', '', '', '', '', '', '', 990, 5, 'catalog/demo/apple_cinema_30.jpg', 8, 1, '100.0000', 400, 9, '2009-02-04', '12.50000000', 1, '1.00000000', '2.00000000', '3.00000000', 1, 1, 2, 0, 1, 0, '2009-02-03 21:07:37', '2011-09-30 00:46:19'),
(43, 'Product 16', '', '', '', '', '', '', '', 929, 5, 'catalog/demo/macbook_1.jpg', 8, 0, '500.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 21:07:49', '2011-09-30 01:05:46'),
(44, 'Product 17', '', '', '', '', '', '', '', 1000, 5, 'catalog/demo/macbook_air_1.jpg', 8, 1, '1000.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 21:08:00', '2011-09-30 01:05:53'),
(45, 'Product 18', '', '', '', '', '', '', '', 998, 5, 'catalog/demo/macbook_pro_1.jpg', 8, 1, '2000.0000', 0, 100, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 21:08:17', '2011-09-15 22:22:01'),
(46, 'Product 19', '', '', '', '', '', '', '', 1000, 5, 'catalog/demo/sony_vaio_1.jpg', 10, 1, '1000.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 21:08:29', '2011-09-30 01:06:39'),
(47, 'Product 21', '', '', '', '', '', '', '', 1000, 5, 'catalog/demo/hp_1.jpg', 7, 1, '100.0000', 400, 9, '2009-02-03', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 0, 1, 0, 1, 0, '2009-02-03 21:08:40', '2011-09-30 01:05:28'),
(48, 'product 20', 'test 1', '', '', '', '', '', 'test 2', 995, 5, 'catalog/demo/ipod_classic_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-08', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-08 17:21:51', '2011-09-30 01:07:06'),
(49, 'SAM1', '', '', '', '', '', '', '', 0, 8, 'catalog/demo/samsung_tab_1.jpg', 0, 1, '199.9900', 0, 9, '2011-04-25', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 0, '2011-04-26 08:57:34', '2011-09-30 01:06:23');

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE `product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attribute`
--

INSERT INTO `product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(43, 2, 1, '1'),
(47, 4, 1, '16GB'),
(43, 4, 1, '8gb'),
(42, 3, 1, '100mhz'),
(47, 2, 1, '4');

-- --------------------------------------------------------

--
-- Table structure for table `product_description`
--

CREATE TABLE `product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_description`
--

INSERT INTO `product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(35, 1, 'Product 8', '&lt;p&gt;\r\n	Product 8&lt;/p&gt;\r\n', '', 'Product 8', '', ''),
(48, 1, 'iPod Classic', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;More room to move.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Sleeker design.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Beautiful, durable, and sleeker than ever, iPod classic now features an anodized aluminum and polished stainless steel enclosure with rounded edges.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'iPod Classic', '', ''),
(40, 1, 'iPhone', '&lt;p class=&quot;intro&quot;&gt;\r\n	iPhone is a revolutionary new mobile phone that allows you to make a call by simply tapping a name or number in your address book, a favorites list, or a call log. It also automatically syncs all your contacts from a PC, Mac, or Internet service. And it lets you select and listen to voicemail messages in whatever order you want just like email.&lt;/p&gt;\r\n', '', 'iPhone', '', ''),
(28, 1, 'HTC Touch HD', '&lt;p&gt;\r\n	HTC Touch - in High Definition. Watch music videos and streaming content in awe-inspiring high definition clarity for a mobile experience you never thought possible. Seductively sleek, the HTC Touch HD provides the next generation of mobile functionality, all at a simple touch. Fully integrated with Windows Mobile Professional 6.1, ultrafast 3.5G, GPS, 5MP camera, plus lots more - all delivered on a breathtakingly crisp 3.8&amp;quot; WVGA touchscreen - you can take control of your mobile world with the HTC Touch HD.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Processor Qualcomm&amp;reg; MSM 7201A&amp;trade; 528 MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Operating System&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Memory: 512 MB ROM, 288 MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Dimensions: 115 mm x 62.8 mm x 12 mm / 146.4 grams&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.8-inch TFT-LCD flat touch-sensitive screen with 480 x 800 WVGA resolution&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/WCDMA: Europe/Asia: 900/2100 MHz; Up to 2 Mbps up-link and 7.2 Mbps down-link speeds&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM/GPRS/EDGE: Europe/Asia: 850/900/1800/1900 MHz (Band frequency, HSUPA availability, and data speed are operator dependent.)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Device Control via HTC TouchFLO&amp;trade; 3D &amp;amp; Touch-sensitive front panel buttons&lt;/li&gt;\r\n	&lt;li&gt;\r\n		GPS and A-GPS ready&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth&amp;reg; 2.0 with Enhanced Data Rate and A2DP for wireless stereo headsets&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Wi-Fi&amp;reg;: IEEE 802.11 b/g&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HTC ExtUSB&amp;trade; (11-pin mini-USB 2.0)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		5 megapixel color camera with auto focus&lt;/li&gt;\r\n	&lt;li&gt;\r\n		VGA CMOS color camera&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in 3.5 mm audio jack, microphone, speaker, and FM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Ring tone formats: AAC, AAC+, eAAC+, AMR-NB, AMR-WB, QCP, MP3, WMA, WAV&lt;/li&gt;\r\n	&lt;li&gt;\r\n		40 polyphonic and standard MIDI format 0 and 1 (SMF)/SP MIDI&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Rechargeable Lithium-ion or Lithium-ion polymer 1350 mAh battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Expansion Slot: microSD&amp;trade; memory card (SD 2.0 compatible)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		AC Adapter Voltage range/frequency: 100 ~ 240V AC, 50/60 Hz DC output: 5V and 1A&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Special Features: FM Radio, G-Sensor&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', '	 HTC Touch HD', '', ''),
(44, 1, 'MacBook Air', '&lt;div&gt;\r\n	MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&amp;rsquo;t lose inches and pounds overnight. It&amp;rsquo;s the result of rethinking conventions. Of multiple wireless innovations. And of breakthrough design. With MacBook Air, mobile computing suddenly has a new standard.&lt;/div&gt;\r\n', '', 'MacBook Air', '', ''),
(45, 1, 'MacBook Pro', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Latest Intel mobile architecture&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Powered by the most advanced mobile processors from Intel, the new Core 2 Duo MacBook Pro is over 50% faster than the original Core Duo MacBook Pro and now supports up to 4GB of RAM.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Leading-edge graphics&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			The NVIDIA GeForce 8600M GT delivers exceptional graphics processing power. For the ultimate creative canvas, you can even configure the 17-inch model with a 1920-by-1200 resolution display.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Designed for life on the road&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Innovations such as a magnetic power connection and an illuminated keyboard with ambient light sensor put the MacBook Pro in a class by itself.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Connect. Create. Communicate.&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Quickly set up a video conference with the built-in iSight camera. Control presentations and media from up to 30 feet away with the included Apple Remote. Connect to high-bandwidth peripherals with FireWire 800 and DVI.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Next-generation wireless&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Featuring 802.11n wireless technology, the MacBook Pro delivers up to five times the performance and up to twice the range of previous-generation technologies.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'MacBook Pro', '', ''),
(29, 1, 'Palm Treo Pro', '&lt;p&gt;\r\n	Redefine your workday with the Palm Treo Pro smartphone. Perfectly balanced, you can respond to business and personal email, stay on top of appointments and contacts, and use Wi-Fi or GPS when you&amp;rsquo;re out and about. Then watch a video on YouTube, catch up with news and sports on the web, or listen to a few songs. Balance your work and play the way you like it, with the Palm Treo Pro.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Edition&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Qualcomm&amp;reg; MSM7201 400MHz Processor&lt;/li&gt;\r\n	&lt;li&gt;\r\n		320x320 transflective colour TFT touchscreen&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/UMTS/EDGE/GPRS/GSM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Tri-band UMTS &amp;mdash; 850MHz, 1900MHz, 2100MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM &amp;mdash; 850/900/1800/1900&lt;/li&gt;\r\n	&lt;li&gt;\r\n		802.11b/g with WPA, WPA2, and 801.1x authentication&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in GPS&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth Version: 2.0 + Enhanced Data Rate&lt;/li&gt;\r\n	&lt;li&gt;\r\n		256MB storage (100MB user available), 128MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		2.0 megapixel camera, up to 8x digital zoom and video capture&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Removable, rechargeable 1500mAh lithium-ion battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Up to 5.0 hours talk time and up to 250 hours standby&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroSDHC card expansion (up to 32GB supported)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroUSB 2.0 for synchronization and charging&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.5mm stereo headset jack&lt;/li&gt;\r\n	&lt;li&gt;\r\n		60mm (W) x 114mm (L) x 13.5mm (D) / 133g&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', 'Palm Treo Pro', '', ''),
(36, 1, 'iPod Nano', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Video in your pocket.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Its the small iPod with one very big idea: video. The worlds most popular music player now lets you enjoy movies, TV shows, and more on a two-inch display thats 65% brighter than before.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Sleek and colorful.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With an anodized aluminum and polished stainless steel enclosure and a choice of five colors, iPod nano is dressed to impress.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;iTunes.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Available as a free download, iTunes makes it easy to browse and buy millions of songs, movies, TV shows, audiobooks, and games and download free podcasts all at the iTunes Store. And you can import your own music, manage your whole media library, and sync your iPod or iPhone with ease.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'iPod Nano', '', ''),
(46, 1, 'Sony VAIO', '&lt;div&gt;\r\n	Unprecedented power. The next generation of processing technology has arrived. Built into the newest VAIO notebooks lies Intel&amp;#39;s latest, most powerful innovation yet: Intel&amp;reg; Centrino&amp;reg; 2 processor technology. Boasting incredible speed, expanded wireless connectivity, enhanced multimedia support and greater energy efficiency, all the high-performance essentials are seamlessly combined into a single chip.&lt;/div&gt;\r\n', '', 'Sony VAIO', '', ''),
(47, 1, 'HP LP3065', '&lt;p&gt;\r\n	Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel Monitor. This flagship monitor features best-in-class performance and presentation features on a huge wide-aspect screen while letting you work as comfortably as possible - you might even forget you&amp;#39;re at the office&lt;/p&gt;\r\n', '', 'HP LP3065', '', ''),
(32, 1, 'iPod Touch', '&lt;p&gt;\r\n	&lt;strong&gt;Revolutionary multi-touch interface.&lt;/strong&gt;&lt;br /&gt;\r\n	iPod touch features the same multi-touch screen technology as iPhone. Pinch to zoom in on a photo. Scroll through your songs and videos with a flick. Flip through your library by album artwork with Cover Flow.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Gorgeous 3.5-inch widescreen display.&lt;/strong&gt;&lt;br /&gt;\r\n	Watch your movies, TV shows, and photos come alive with bright, vivid color on the 320-by-480-pixel display.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Music downloads straight from iTunes.&lt;/strong&gt;&lt;br /&gt;\r\n	Shop the iTunes Wi-Fi Music Store from anywhere with Wi-Fi.1 Browse or search to find the music youre looking for, preview it, and buy it with just a tap.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Surf the web with Wi-Fi.&lt;/strong&gt;&lt;br /&gt;\r\n	Browse the web using Safari and watch YouTube videos on the first iPod with Wi-Fi built in&lt;br /&gt;\r\n	&amp;nbsp;&lt;/p&gt;\r\n', '', '	 iPod Touch', '', ''),
(41, 1, 'iMac', '&lt;div&gt;\r\n	Just when you thought iMac had everything, now there&acute;s even more. More powerful Intel Core 2 Duo processors. And more memory standard. Combine this with Mac OS X Leopard and iLife &acute;08, and it&acute;s more all-in-one than ever. iMac packs amazing performance into a stunningly slim space.&lt;/div&gt;\r\n', '', 'iMac', '', ''),
(33, 1, 'Samsung SyncMaster 941BW', '&lt;div&gt;\r\n	Imagine the advantages of going big without slowing down. The big 19&amp;quot; 941BW monitor combines wide aspect ratio with fast pixel response time, for bigger images, more room to work and crisp motion. In addition, the exclusive MagicBright 2, MagicColor and MagicTune technologies help deliver the ideal image in every situation, while sleek, narrow bezels and adjustable stands deliver style just the way you want it. With the Samsung 941BW widescreen analog/digital LCD monitor, it&amp;#39;s not hard to imagine.&lt;/div&gt;\r\n', '', 'Samsung SyncMaster 941BW', '', ''),
(34, 1, 'iPod Shuffle', '&lt;div&gt;\r\n	&lt;strong&gt;Born to be worn.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Clip on the worlds most wearable music player and take up to 240 songs with you anywhere. Choose from five colors including four new hues to make your musical fashion statement.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Random meets rhythm.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With iTunes autofill, iPod shuffle can deliver a new musical experience every time you sync. For more randomness, you can shuffle songs during playback with the slide of a switch.&lt;/p&gt;\r\n	&lt;strong&gt;Everything is easy.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Charge and sync with the included USB dock. Operate the iPod shuffle controls with one hand. Enjoy up to 12 hours straight of skip-free music playback.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'iPod Shuffle', '', ''),
(43, 1, 'MacBook', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Intel Core 2 Duo processor&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Powered by an Intel Core 2 Duo processor at speeds up to 2.16GHz, the new MacBook is the fastest ever.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;1GB memory, larger hard drives&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		The new MacBook now comes with 1GB of memory standard and larger hard drives for the entire line perfect for running more of your favorite applications and storing growing media collections.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Sleek, 1.08-inch-thin design&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		MacBook makes it easy to hit the road thanks to its tough polycarbonate case, built-in wireless technologies, and innovative MagSafe Power Adapter that releases automatically if someone accidentally trips on the cord.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Built-in iSight camera&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Right out of the box, you can have a video chat with friends or family,2 record a video at your desk, or take fun pictures with Photo Booth&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'MacBook', '', ''),
(31, 1, 'Nikon D300', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		Engineered with pro-level features and performance, the 12.3-effective-megapixel D300 combines brand new technologies with advanced features inherited from Nikon&amp;#39;s newly announced D3 professional digital SLR camera to offer serious photographers remarkable performance combined with agility.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		Similar to the D3, the D300 features Nikon&amp;#39;s exclusive EXPEED Image Processing System that is central to driving the speed and processing power needed for many of the camera&amp;#39;s new features. The D300 features a new 51-point autofocus system with Nikon&amp;#39;s 3D Focus Tracking feature and two new LiveView shooting modes that allow users to frame a photograph using the camera&amp;#39;s high-resolution LCD monitor. The D300 shares a similar Scene Recognition System as is found in the D3; it promises to greatly enhance the accuracy of autofocus, autoexposure, and auto white balance by recognizing the subject or scene being photographed and applying this information to the calculations for the three functions.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 reacts with lightning speed, powering up in a mere 0.13 seconds and shooting with an imperceptible 45-millisecond shutter release lag time. The D300 is capable of shooting at a rapid six frames per second and can go as fast as eight frames per second when using the optional MB-D10 multi-power battery pack. In continuous bursts, the D300 can shoot up to 100 shots at full 12.3-megapixel resolution. (NORMAL-LARGE image setting, using a SanDisk Extreme IV 1GB CompactFlash card.)&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 incorporates a range of innovative technologies and features that will significantly improve the accuracy, control, and performance photographers can get from their equipment. Its new Scene Recognition System advances the use of Nikon&amp;#39;s acclaimed 1,005-segment sensor to recognize colors and light patterns that help the camera determine the subject and the type of scene being photographed before a picture is taken. This information is used to improve the accuracy of autofocus, autoexposure, and auto white balance functions in the D300. For example, the camera can track moving subjects better and by identifying them, it can also automatically select focus points faster and with greater accuracy. It can also analyze highlights and more accurately determine exposure, as well as infer light sources to deliver more accurate white balance detection.&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'Nikon D300', '', ''),
(49, 1, 'Samsung Galaxy Tab 10.1', '&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1, is the world&amp;rsquo;s thinnest tablet, measuring 8.6 mm thickness, running with Android 3.0 Honeycomb OS on a 1GHz dual-core Tegra 2 processor, similar to its younger brother Samsung Galaxy Tab 8.9.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 gives pure Android 3.0 experience, adding its new TouchWiz UX or TouchWiz 4.0 &amp;ndash; includes a live panel, which lets you to customize with different content, such as your pictures, bookmarks, and social feeds, sporting a 10.1 inches WXGA capacitive touch screen with 1280 x 800 pixels of resolution, equipped with 3 megapixel rear camera with LED flash and a 2 megapixel front camera, HSPA+ connectivity up to 21Mbps, 720p HD video recording capability, 1080p HD playback, DLNA support, Bluetooth 2.1, USB 2.0, gyroscope, Wi-Fi 802.11 a/b/g/n, micro-SD slot, 3.5mm headphone jack, and SIM slot, including the Samsung Stick &amp;ndash; a Bluetooth microphone that can be carried in a pocket like a pen and sound dock with powered subwoofer.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 will come in 16GB / 32GB / 64GB verities and pre-loaded with Social Hub, Reader&amp;rsquo;s Hub, Music Hub and Samsung Mini Apps Tray &amp;ndash; which gives you access to more commonly used apps to help ease multitasking and it is capable of Adobe Flash Player 10.2, powered by 6860mAh battery that gives you 10hours of video-playback time.&amp;nbsp;&amp;auml;&amp;ouml;&lt;/p&gt;\r\n', '', 'Samsung Galaxy Tab 10.1', '', ''),
(42, 1, 'Apple Cinema 30&quot;', '&lt;p&gt;\r\n	&lt;font face=&quot;helvetica,geneva,arial&quot; size=&quot;2&quot;&gt;&lt;font face=&quot;Helvetica&quot; size=&quot;2&quot;&gt;The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed specifically for the creative professional, this display provides more space for easier access to all the tools and palettes needed to edit, format and composite your work. Combine this display with a Mac Pro, MacBook Pro, or PowerMac G5 and there\'s no limit to what you can achieve. &lt;br&gt;\r\n	&lt;br&gt;\r\n	&lt;/font&gt;&lt;font face=&quot;Helvetica&quot; size=&quot;2&quot;&gt;The Cinema HD features an active-matrix liquid crystal display that produces flicker-free images that deliver twice the brightness, twice the sharpness and twice the contrast ratio of a typical CRT display. Unlike other flat panels, it\'s designed with a pure digital interface to deliver distortion-free images that never need adjusting. With over 4 million digital pixels, the display is uniquely suited for scientific and technical applications such as visualizing molecular structures or analyzing geological data. &lt;br&gt;\r\n	&lt;br&gt;\r\n	&lt;/font&gt;&lt;font face=&quot;Helvetica&quot; size=&quot;2&quot;&gt;Offering accurate, brilliant color performance, the Cinema HD delivers up to 16.7 million colors across a wide gamut allowing you to see subtle nuances between colors from soft pastels to rich jewel tones. A wide viewing angle ensures uniform color from edge to edge. Apple\'s ColorSync technology allows you to create custom profiles to maintain consistent color onscreen and in print. The result: You can confidently use this display in all your color-critical applications. &lt;br&gt;\r\n	&lt;br&gt;\r\n	&lt;/font&gt;&lt;font face=&quot;Helvetica&quot; size=&quot;2&quot;&gt;Housed in a new aluminum design, the display has a very thin bezel that enhances visual accuracy. Each display features two FireWire 400 ports and two USB 2.0 ports, making attachment of desktop peripherals, such as iSight, iPod, digital and still cameras, hard drives, printers and scanners, even more accessible and convenient. Taking advantage of the much thinner and lighter footprint of an LCD, the new displays support the VESA (Video Electronics Standards Association) mounting interface standard. Customers with the optional Cinema Display VESA Mount Adapter kit gain the flexibility to mount their display in locations most appropriate for their work environment. &lt;br&gt;\r\n	&lt;br&gt;\r\n	&lt;/font&gt;&lt;font face=&quot;Helvetica&quot; size=&quot;2&quot;&gt;The Cinema HD features a single cable design with elegant breakout for the USB 2.0, FireWire 400 and a pure digital connection using the industry standard Digital Video Interface (DVI) interface. The DVI connection allows for a direct pure-digital connection.&lt;br&gt;\r\n	&lt;/font&gt;&lt;/font&gt;&lt;/p&gt;\r\n&lt;h3&gt;\r\n	Features:&lt;/h3&gt;\r\n&lt;p&gt;\r\n	Unrivaled display performance&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		30-inch (viewable) active-matrix liquid crystal display provides breathtaking image quality and vivid, richly saturated color.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Support for 2560-by-1600 pixel resolution for display of high definition still and video imagery.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Wide-format design for simultaneous display of two full pages of text and graphics.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Industry standard DVI connector for direct attachment to Mac- and Windows-based desktops and notebooks&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Incredibly wide (170 degree) horizontal and vertical viewing angle for maximum visibility and color performance.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Lightning-fast pixel response for full-motion digital video playback.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Support for 16.7 million saturated colors, for use in all graphics-intensive applications.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	Simple setup and operation&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Single cable with elegant breakout for connection to DVI, USB and FireWire ports&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in two-port USB 2.0 hub for easy connection of desktop peripheral devices.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Two FireWire 400 ports to support iSight and other desktop peripherals&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	Sleek, elegant design&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Huge virtual workspace, very small footprint.&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Narrow Bezel design to minimize visual impact of using dual displays&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Unique hinge design for effortless adjustment&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Support for VESA mounting solutions (Apple Cinema Display VESA Mount Adapter sold separately)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;h3&gt;\r\n	Technical specifications&lt;/h3&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Screen size (diagonal viewable image size)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Apple Cinema HD Display: 30 inches (29.7-inch viewable)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Screen type&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Thin film transistor (TFT) active-matrix liquid crystal display (AMLCD)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Resolutions&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		2560 x 1600 pixels (optimum resolution)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		2048 x 1280&lt;/li&gt;\r\n	&lt;li&gt;\r\n		1920 x 1200&lt;/li&gt;\r\n	&lt;li&gt;\r\n		1280 x 800&lt;/li&gt;\r\n	&lt;li&gt;\r\n		1024 x 640&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Display colors (maximum)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		16.7 million&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Viewing angle (typical)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		170° horizontal; 170° vertical&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Brightness (typical)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		30-inch Cinema HD Display: 400 cd/m2&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Contrast ratio (typical)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		700:1&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Response time (typical)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		16 ms&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Pixel pitch&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		30-inch Cinema HD Display: 0.250 mm&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Screen treatment&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Antiglare hardcoat&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;User controls (hardware and software)&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Display Power,&lt;/li&gt;\r\n	&lt;li&gt;\r\n		System sleep, wake&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Brightness&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Monitor tilt&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Connectors and cables&lt;/b&gt;&lt;br&gt;\r\n	Cable&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		DVI (Digital Visual Interface)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		FireWire 400&lt;/li&gt;\r\n	&lt;li&gt;\r\n		USB 2.0&lt;/li&gt;\r\n	&lt;li&gt;\r\n		DC power (24 V)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	Connectors&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Two-port, self-powered USB 2.0 hub&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Two FireWire 400 ports&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Kensington security port&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;VESA mount adapter&lt;/b&gt;&lt;br&gt;\r\n	Requires optional Cinema Display VESA Mount Adapter (M9649G/A)&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Compatible with VESA FDMI (MIS-D, 100, C) compliant mounting solutions&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Electrical requirements&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Input voltage: 100-240 VAC 50-60Hz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Maximum power when operating: 150W&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Energy saver mode: 3W or less&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Environmental requirements&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Operating temperature: 50° to 95° F (10° to 35° C)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Storage temperature: -40° to 116° F (-40° to 47° C)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Operating humidity: 20% to 80% noncondensing&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Maximum operating altitude: 10,000 feet&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Agency approvals&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		FCC Part 15 Class B&lt;/li&gt;\r\n	&lt;li&gt;\r\n		EN55022 Class B&lt;/li&gt;\r\n	&lt;li&gt;\r\n		EN55024&lt;/li&gt;\r\n	&lt;li&gt;\r\n		VCCI Class B&lt;/li&gt;\r\n	&lt;li&gt;\r\n		AS/NZS 3548 Class B&lt;/li&gt;\r\n	&lt;li&gt;\r\n		CNS 13438 Class B&lt;/li&gt;\r\n	&lt;li&gt;\r\n		ICES-003 Class B&lt;/li&gt;\r\n	&lt;li&gt;\r\n		ISO 13406 part 2&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MPR II&lt;/li&gt;\r\n	&lt;li&gt;\r\n		IEC 60950&lt;/li&gt;\r\n	&lt;li&gt;\r\n		UL 60950&lt;/li&gt;\r\n	&lt;li&gt;\r\n		CSA 60950&lt;/li&gt;\r\n	&lt;li&gt;\r\n		EN60950&lt;/li&gt;\r\n	&lt;li&gt;\r\n		ENERGY STAR&lt;/li&gt;\r\n	&lt;li&gt;\r\n		TCO \'03&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;Size and weight&lt;/b&gt;&lt;br&gt;\r\n	30-inch Apple Cinema HD Display&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Height: 21.3 inches (54.3 cm)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Width: 27.2 inches (68.8 cm)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Depth: 8.46 inches (21.5 cm)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Weight: 27.5 pounds (12.5 kg)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;\r\n	&lt;b&gt;System Requirements&lt;/b&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Mac Pro, all graphic options&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MacBook Pro&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Power Mac G5 (PCI-X) with ATI Radeon 9650 or better or NVIDIA GeForce 6800 GT DDL or better&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Power Mac G5 (PCI Express), all graphics options&lt;/li&gt;\r\n	&lt;li&gt;\r\n		PowerBook G4 with dual-link DVI support&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Windows PC and graphics card that supports DVI ports with dual-link digital bandwidth and VESA DDC standard for plug-and-play setup&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', 'Apple Cinema 30', '', ''),
(30, 1, 'Canon EOS 5D', '&lt;p&gt;\r\n	Canon\'s press material for the EOS 5D states that it \'defines (a) new D-SLR category\', while we\'re not typically too concerned with marketing talk this particular statement is clearly pretty accurate. The EOS 5D is unlike any previous digital SLR in that it combines a full-frame (35 mm sized) high resolution sensor (12.8 megapixels) with a relatively compact body (slightly larger than the EOS 20D, although in your hand it feels noticeably \'chunkier\'). The EOS 5D is aimed to slot in between the EOS 20D and the EOS-1D professional digital SLR\'s, an important difference when compared to the latter is that the EOS 5D doesn\'t have any environmental seals. While Canon don\'t specifically refer to the EOS 5D as a \'professional\' digital SLR it will have obvious appeal to professionals who want a high quality digital SLR in a body lighter than the EOS-1D. It will also no doubt appeal to current EOS 20D owners (although lets hope they\'ve not bought too many EF-S lenses...) äë&lt;/p&gt;\r\n', '', 'sdf', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

CREATE TABLE `product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_discount`
--

INSERT INTO `product_discount` (`product_discount_id`, `product_id`, `customer_group_id`, `quantity`, `priority`, `price`, `date_start`, `date_end`) VALUES
(440, 42, 1, 30, 1, '66.0000', '0000-00-00', '0000-00-00'),
(439, 42, 1, 20, 1, '77.0000', '0000-00-00', '0000-00-00'),
(438, 42, 1, 10, 1, '88.0000', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `product_filter`
--

CREATE TABLE `product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2345, 30, 'catalog/demo/canon_eos_5d_2.jpg', 0),
(2321, 47, 'catalog/demo/hp_3.jpg', 0),
(2035, 28, 'catalog/demo/htc_touch_hd_2.jpg', 0),
(2351, 41, 'catalog/demo/imac_3.jpg', 0),
(1982, 40, 'catalog/demo/iphone_6.jpg', 0),
(2001, 36, 'catalog/demo/ipod_nano_5.jpg', 0),
(2000, 36, 'catalog/demo/ipod_nano_4.jpg', 0),
(2005, 34, 'catalog/demo/ipod_shuffle_5.jpg', 0),
(2004, 34, 'catalog/demo/ipod_shuffle_4.jpg', 0),
(2011, 32, 'catalog/demo/ipod_touch_7.jpg', 0),
(2010, 32, 'catalog/demo/ipod_touch_6.jpg', 0),
(2009, 32, 'catalog/demo/ipod_touch_5.jpg', 0),
(1971, 43, 'catalog/demo/macbook_5.jpg', 0),
(1970, 43, 'catalog/demo/macbook_4.jpg', 0),
(1974, 44, 'catalog/demo/macbook_air_4.jpg', 0),
(1973, 44, 'catalog/demo/macbook_air_2.jpg', 0),
(1977, 45, 'catalog/demo/macbook_pro_2.jpg', 0),
(1976, 45, 'catalog/demo/macbook_pro_3.jpg', 0),
(1986, 31, 'catalog/demo/nikon_d300_3.jpg', 0),
(1985, 31, 'catalog/demo/nikon_d300_2.jpg', 0),
(1988, 29, 'catalog/demo/palm_treo_pro_3.jpg', 0),
(1995, 46, 'catalog/demo/sony_vaio_5.jpg', 0),
(1994, 46, 'catalog/demo/sony_vaio_4.jpg', 0),
(1991, 48, 'catalog/demo/ipod_classic_4.jpg', 0),
(1990, 48, 'catalog/demo/ipod_classic_3.jpg', 0),
(1981, 40, 'catalog/demo/iphone_2.jpg', 0),
(1980, 40, 'catalog/demo/iphone_5.jpg', 0),
(2344, 30, 'catalog/demo/canon_eos_5d_3.jpg', 0),
(2320, 47, 'catalog/demo/hp_2.jpg', 0),
(2034, 28, 'catalog/demo/htc_touch_hd_3.jpg', 0),
(2350, 41, 'catalog/demo/imac_2.jpg', 0),
(1979, 40, 'catalog/demo/iphone_3.jpg', 0),
(1978, 40, 'catalog/demo/iphone_4.jpg', 0),
(1989, 48, 'catalog/demo/ipod_classic_2.jpg', 0),
(1999, 36, 'catalog/demo/ipod_nano_2.jpg', 0),
(1998, 36, 'catalog/demo/ipod_nano_3.jpg', 0),
(2003, 34, 'catalog/demo/ipod_shuffle_2.jpg', 0),
(2002, 34, 'catalog/demo/ipod_shuffle_3.jpg', 0),
(2008, 32, 'catalog/demo/ipod_touch_2.jpg', 0),
(2007, 32, 'catalog/demo/ipod_touch_3.jpg', 0),
(2006, 32, 'catalog/demo/ipod_touch_4.jpg', 0),
(1969, 43, 'catalog/demo/macbook_2.jpg', 0),
(1968, 43, 'catalog/demo/macbook_3.jpg', 0),
(1972, 44, 'catalog/demo/macbook_air_3.jpg', 0),
(1975, 45, 'catalog/demo/macbook_pro_4.jpg', 0),
(1984, 31, 'catalog/demo/nikon_d300_4.jpg', 0),
(1983, 31, 'catalog/demo/nikon_d300_5.jpg', 0),
(1987, 29, 'catalog/demo/palm_treo_pro_2.jpg', 0),
(1993, 46, 'catalog/demo/sony_vaio_2.jpg', 0),
(1992, 46, 'catalog/demo/sony_vaio_3.jpg', 0),
(2327, 49, 'catalog/demo/samsung_tab_7.jpg', 0),
(2326, 49, 'catalog/demo/samsung_tab_6.jpg', 0),
(2325, 49, 'catalog/demo/samsung_tab_5.jpg', 0),
(2324, 49, 'catalog/demo/samsung_tab_4.jpg', 0),
(2323, 49, 'catalog/demo/samsung_tab_3.jpg', 0),
(2322, 49, 'catalog/demo/samsung_tab_2.jpg', 0),
(2317, 42, 'catalog/demo/canon_logo.jpg', 0),
(2316, 42, 'catalog/demo/hp_1.jpg', 0),
(2315, 42, 'catalog/demo/compaq_presario.jpg', 0),
(2314, 42, 'catalog/demo/canon_eos_5d_1.jpg', 0),
(2313, 42, 'catalog/demo/canon_eos_5d_2.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_option`
--

CREATE TABLE `product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_option`
--

INSERT INTO `product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(224, 35, 11, '', 1),
(225, 47, 12, '2011-04-22', 1),
(223, 42, 2, '', 1),
(217, 42, 5, '', 1),
(209, 42, 6, '', 1),
(218, 42, 1, '', 1),
(208, 42, 4, 'test', 1),
(219, 42, 8, '2011-02-20', 1),
(222, 42, 7, '', 1),
(221, 42, 9, '22:25', 1),
(220, 42, 10, '2011-02-20 22:25', 1),
(226, 30, 5, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_option_value`
--

CREATE TABLE `product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_option_value`
--

INSERT INTO `product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(1, 217, 42, 5, 41, 100, 0, '1.0000', '+', 0, '+', '1.00000000', '+'),
(6, 218, 42, 1, 31, 146, 1, '20.0000', '+', 2, '-', '20.00000000', '+'),
(7, 218, 42, 1, 43, 300, 1, '30.0000', '+', 3, '+', '30.00000000', '+'),
(5, 218, 42, 1, 32, 96, 1, '10.0000', '+', 1, '+', '10.00000000', '+'),
(4, 217, 42, 5, 39, 92, 1, '4.0000', '+', 0, '+', '4.00000000', '+'),
(2, 217, 42, 5, 42, 200, 1, '2.0000', '+', 0, '+', '2.00000000', '+'),
(3, 217, 42, 5, 40, 300, 0, '3.0000', '+', 0, '+', '3.00000000', '+'),
(8, 223, 42, 2, 23, 48, 1, '10.0000', '+', 0, '+', '10.00000000', '+'),
(10, 223, 42, 2, 44, 2696, 1, '30.0000', '+', 0, '+', '30.00000000', '+'),
(9, 223, 42, 2, 24, 194, 1, '20.0000', '+', 0, '+', '20.00000000', '+'),
(11, 223, 42, 2, 45, 3998, 1, '40.0000', '+', 0, '+', '40.00000000', '+'),
(12, 224, 35, 11, 46, 0, 1, '5.0000', '+', 0, '+', '0.00000000', '+'),
(13, 224, 35, 11, 47, 10, 1, '10.0000', '+', 0, '+', '0.00000000', '+'),
(14, 224, 35, 11, 48, 15, 1, '15.0000', '+', 0, '+', '0.00000000', '+'),
(16, 226, 30, 5, 40, 5, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(15, 226, 30, 5, 39, 2, 1, '0.0000', '+', 0, '+', '0.00000000', '+');

-- --------------------------------------------------------

--
-- Table structure for table `product_recurring`
--

CREATE TABLE `product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_related`
--

CREATE TABLE `product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_related`
--

INSERT INTO `product_related` (`product_id`, `related_id`) VALUES
(40, 42),
(41, 42),
(42, 40),
(42, 41);

-- --------------------------------------------------------

--
-- Table structure for table `product_reward`
--

CREATE TABLE `product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_reward`
--

INSERT INTO `product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(515, 42, 1, 100),
(519, 47, 1, 300),
(379, 28, 1, 400),
(329, 43, 1, 600),
(339, 29, 1, 0),
(343, 48, 1, 0),
(335, 40, 1, 0),
(539, 30, 1, 200),
(331, 44, 1, 700),
(333, 45, 1, 800),
(337, 31, 1, 0),
(425, 35, 1, 0),
(345, 33, 1, 0),
(347, 46, 1, 0),
(545, 41, 1, 0),
(351, 36, 1, 0),
(353, 34, 1, 0),
(355, 32, 1, 0),
(521, 49, 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `product_special`
--

CREATE TABLE `product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_special`
--

INSERT INTO `product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(419, 42, 1, 1, '90.0000', '0000-00-00', '0000-00-00'),
(439, 30, 1, 2, '90.0000', '0000-00-00', '0000-00-00'),
(438, 30, 1, 1, '80.0000', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE `product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`product_id`, `category_id`) VALUES
(28, 20),
(28, 24),
(29, 20),
(29, 24),
(30, 20),
(30, 33),
(31, 33),
(32, 34),
(33, 20),
(33, 28),
(34, 34),
(35, 20),
(36, 34),
(40, 20),
(40, 24),
(41, 27),
(42, 20),
(42, 28),
(43, 18),
(43, 20),
(44, 18),
(44, 20),
(45, 18),
(46, 18),
(46, 20),
(47, 18),
(47, 20),
(48, 20),
(48, 34),
(49, 57);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_download`
--

CREATE TABLE `product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_layout`
--

CREATE TABLE `product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_store`
--

CREATE TABLE `product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_store`
--

INSERT INTO `product_to_store` (`product_id`, `store_id`) VALUES
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0);

-- --------------------------------------------------------

--
-- Table structure for table `recurring`
--

CREATE TABLE `recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recurring_description`
--

CREATE TABLE `recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return`
--

CREATE TABLE `return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_action`
--

CREATE TABLE `return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_action`
--

INSERT INTO `return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `return_history`
--

CREATE TABLE `return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_reason`
--

CREATE TABLE `return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_reason`
--

INSERT INTO `return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `return_status`
--

CREATE TABLE `return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_status`
--

INSERT INTO `return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revslider_attachment_images`
--

CREATE TABLE `revslider_attachment_images` (
  `ID` int(10) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `subdir` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revslider_css`
--

CREATE TABLE `revslider_css` (
  `id` int(11) NOT NULL,
  `handle` text NOT NULL,
  `settings` longtext,
  `hover` longtext,
  `params` longtext NOT NULL,
  `advanced` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `revslider_css`
--

INSERT INTO `revslider_css` (`id`, `handle`, `settings`, `hover`, `params`, `advanced`) VALUES
(1, '.tp-caption.medium_grey', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-family\":\"Arial\",\"padding\":\"2px 4px\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"background-color\":\"#888\",\"white-space\":\"nowrap\"}', NULL),
(2, '.tp-caption.small_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"14px\",\"line-height\":\"20px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(3, '.tp-caption.medium_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(4, '.tp-caption.large_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"40px\",\"line-height\":\"40px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(5, '.tp-caption.very_large_text', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"0px 2px 5px rgba(0, 0, 0, 0.5)\",\"font-weight\":\"700\",\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\",\"letter-spacing\":\"-2px\"}', NULL),
(6, '.tp-caption.very_big_white', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\",\"padding\":\"0px 4px\",\"padding-top\":\"1px\",\"background-color\":\"#000\"}', NULL),
(7, '.tp-caption.very_big_black', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"700\",\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\",\"padding\":\"0px 4px\",\"padding-top\":\"1px\",\"background-color\":\"#fff\"}', NULL),
(8, '.tp-caption.modern_medium_fat', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"24px\",\"line-height\":\"20px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(9, '.tp-caption.modern_medium_fat_white', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"24px\",\"line-height\":\"20px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(10, '.tp-caption.modern_medium_light', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"24px\",\"line-height\":\"20px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(11, '.tp-caption.modern_big_bluebg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"800\",\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"padding\":\"3px 10px\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"background-color\":\"#4e5b6c\",\"letter-spacing\":\"0\"}', NULL),
(12, '.tp-caption.modern_big_redbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-family\":\"\\\"Open Sans\\\", sans-serif\",\"padding\":\"3px 10px\",\"padding-top\":\"1px\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"background-color\":\"#de543e\",\"letter-spacing\":\"0\"}', NULL),
(13, '.tp-caption.modern_small_text_dark', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#555\",\"text-shadow\":\"none\",\"font-size\":\"14px\",\"line-height\":\"22px\",\"font-family\":\"Arial\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-style\":\"none\",\"white-space\":\"nowrap\"}', NULL),
(14, '.tp-caption.boxshadow', NULL, NULL, '{\"-moz-box-shadow\":\"0px 0px 20px rgba(0, 0, 0, 0.5)\",\"-webkit-box-shadow\":\"0px 0px 20px rgba(0, 0, 0, 0.5)\",\"box-shadow\":\"0px 0px 20px rgba(0, 0, 0, 0.5)\"}', NULL),
(15, '.tp-caption.black', NULL, NULL, '{\"color\":\"#000\",\"text-shadow\":\"none\"}', NULL),
(16, '.tp-caption.noshadow', NULL, NULL, '{\"text-shadow\":\"none\"}', NULL),
(17, '.tp-caption.thinheadline_dark', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"rgba(0,0,0,0.85)\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"30px\",\"line-height\":\"30px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"transparent\"}', NULL),
(18, '.tp-caption.thintext_dark', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"rgba(0,0,0,0.85)\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"16px\",\"line-height\":\"26px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"transparent\"}', NULL),
(19, '.tp-caption.largeblackbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#000\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}', NULL),
(20, '.tp-caption.largepinkbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#db4360\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}', NULL),
(21, '.tp-caption.largewhitebg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#000\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#fff\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}', NULL),
(22, '.tp-caption.largegreenbg', NULL, NULL, '{\"position\":\"absolute\",\"color\":\"#fff\",\"text-shadow\":\"none\",\"font-weight\":\"300\",\"font-size\":\"50px\",\"line-height\":\"70px\",\"font-family\":\"\\\"Open Sans\\\"\",\"background-color\":\"#67ae73\",\"padding\":\"0px 20px\",\"-webkit-border-radius\":\"0px\",\"-moz-border-radius\":\"0px\",\"border-radius\":\"0px\"}', NULL),
(23, '.tp-caption.excerpt', NULL, NULL, '{\"font-size\":\"36px\",\"line-height\":\"36px\",\"font-weight\":\"700\",\"font-family\":\"Arial\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"rgba(0, 0, 0, 1)\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"letter-spacing\":\"-1.5px\",\"padding\":\"1px 4px 0px 4px\",\"width\":\"150px\",\"white-space\":\"normal !important\",\"height\":\"auto\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 255, 255)\",\"border-style\":\"none\"}', NULL),
(24, '.tp-caption.large_bold_grey', NULL, NULL, '{\"font-size\":\"60px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(102, 102, 102)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"padding\":\"1px 4px 0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(25, '.tp-caption.medium_thin_grey', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"30px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(102, 102, 102)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"1px 4px 0px\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(26, '.tp-caption.small_thin_grey', NULL, NULL, '{\"font-size\":\"18px\",\"line-height\":\"26px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(117, 117, 117)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"1px 4px 0px\",\"text-shadow\":\"none\",\"margin\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(27, '.tp-caption.lightgrey_divider', NULL, NULL, '{\"text-decoration\":\"none\",\"background-color\":\"rgba(235, 235, 235, 1)\",\"width\":\"370px\",\"height\":\"3px\",\"background-position\":\"initial initial\",\"background-repeat\":\"initial initial\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}', NULL),
(28, '.tp-caption.large_bold_darkblue', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(52, 73, 94)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(29, '.tp-caption.medium_bg_darkblue', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(52, 73, 94)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(30, '.tp-caption.medium_bold_red', NULL, NULL, '{\"font-size\":\"24px\",\"line-height\":\"30px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(227, 58, 12)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(31, '.tp-caption.medium_light_red', NULL, NULL, '{\"font-size\":\"21px\",\"line-height\":\"26px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(227, 58, 12)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(32, '.tp-caption.medium_bg_red', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(227, 58, 12)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(33, '.tp-caption.medium_bold_orange', NULL, NULL, '{\"font-size\":\"24px\",\"line-height\":\"30px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(243, 156, 18)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(34, '.tp-caption.medium_bg_orange', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(243, 156, 18)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(35, '.tp-caption.grassfloor', NULL, NULL, '{\"text-decoration\":\"none\",\"background-color\":\"rgba(160, 179, 151, 1)\",\"width\":\"4000px\",\"height\":\"150px\",\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}', NULL),
(36, '.tp-caption.large_bold_white', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(37, '.tp-caption.medium_light_white', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(38, '.tp-caption.mediumlarge_light_white', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(39, '.tp-caption.mediumlarge_light_white_center', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"#ffffff\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px 0px 0px 0px\",\"text-align\":\"center\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(40, '.tp-caption.medium_bg_asbestos', NULL, NULL, '{\"font-size\":\"20px\",\"line-height\":\"20px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(127, 140, 141)\",\"padding\":\"10px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(41, '.tp-caption.medium_light_black', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"36px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(42, '.tp-caption.large_bold_black', NULL, NULL, '{\"font-size\":\"58px\",\"line-height\":\"60px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(43, '.tp-caption.mediumlarge_light_darkblue', NULL, NULL, '{\"font-size\":\"34px\",\"line-height\":\"40px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(52, 73, 94)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(44, '.tp-caption.small_light_white', NULL, NULL, '{\"font-size\":\"17px\",\"line-height\":\"28px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"transparent\",\"padding\":\"0px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(45, '.tp-caption.roundedimage', NULL, NULL, '{\"border-width\":\"0px\",\"border-color\":\"rgb(34, 34, 34)\",\"border-style\":\"none\"}', NULL),
(46, '.tp-caption.large_bg_black', NULL, NULL, '{\"font-size\":\"40px\",\"line-height\":\"40px\",\"font-weight\":\"800\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(255, 255, 255)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(0, 0, 0)\",\"padding\":\"10px 20px 15px\",\"border-width\":\"0px\",\"border-color\":\"rgb(255, 214, 88)\",\"border-style\":\"none\"}', NULL),
(47, '.tp-caption.mediumwhitebg', NULL, NULL, '{\"font-size\":\"30px\",\"line-height\":\"30px\",\"font-weight\":\"300\",\"font-family\":\"\\\"Open Sans\\\"\",\"color\":\"rgb(0, 0, 0)\",\"text-decoration\":\"none\",\"background-color\":\"rgb(255, 255, 255)\",\"padding\":\"5px 15px 10px\",\"text-shadow\":\"none\",\"border-width\":\"0px\",\"border-color\":\"rgb(0, 0, 0)\",\"border-style\":\"none\"}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `revslider_layer_animations`
--

CREATE TABLE `revslider_layer_animations` (
  `id` int(9) NOT NULL,
  `handle` text NOT NULL,
  `params` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revslider_navigations`
--

CREATE TABLE `revslider_navigations` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `handle` varchar(191) NOT NULL,
  `css` longtext NOT NULL,
  `markup` longtext NOT NULL,
  `settings` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revslider_options`
--

CREATE TABLE `revslider_options` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `revslider_options`
--

INSERT INTO `revslider_options` (`id`, `name`, `value`) VALUES
(1, 'revslider-update-check-short', '1541608860'),
(2, 'revslider-templates-check', '1541608860'),
(3, 'rs-templates-new', '');
INSERT INTO `revslider_options` (`id`, `name`, `value`) VALUES
(4, 'rs-templates', '{\"slider\":{\"0\":{\"id\":\"1\",\"title\":\"Classic Carousel\",\"alias\":\"classic-carousel\",\"zip\":\"classic-carousel.zip\",\"uid\":\"146a01dd380c0cdee85c4456ee68cd84\",\"img\":\"classic-carousel\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/carousel-slider-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">Our classic, full-width carousel example. Drag, swipe or click to navigate!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content carousel. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-22 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"1\":{\"id\":\"2\",\"title\":\"Classic Slider\",\"alias\":\"classicslider\",\"zip\":\"classicslider.zip\",\"uid\":\"a0d6a9248c9066b404ba0f1cdadc5cf2\",\"img\":\"classicslider\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-slideshow-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\" <span class=\\\"ttm_content\\\">A classic slideshow example with timer, bullets and arrow navigation.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"2\":{\"id\":\"3\",\"title\":\"Content Tabs\",\"alias\":\"contenttabs\",\"zip\":\"contenttabs.zip\",\"uid\":\"e02e91604b690123a3d07a65582c4fd0\",\"img\":\"contenttabs\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-content-tabs-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1024\",\"height\":\"600\",\"description\":\" <span class=\\\"ttm_content\\\">An auto-size slider with a cool split-text animation effect. Navigate with vertical bullets and thumbs.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"3\":{\"id\":\"4\",\"title\":\"Facebook Feed\",\"alias\":\"facebook-feed\",\"zip\":\"facebook-feed.zip\",\"uid\":\"5506431d5b1babcb25dcf52c508d42e3\",\"img\":\"facebook-feed\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-facebook-feed\\/\",\"version\":\"1.0.1\",\"required\":\"5.0.5\",\"width\":\"800\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\"> This slider is automatically populated by a Facebook stream. Just one slide is needed to show a whole feed!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The content source is set to <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/special-content-sources\\/\\\" target=\\\"_blank\\\">\\\"Facebook Stream\\\"<\\/a>. Make sure to input your correct API information in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\",\"socialmedia\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"4\":{\"id\":\"5\",\"title\":\"Fashion\",\"alias\":\"fashion\",\"zip\":\"fashion.zip\",\"uid\":\"4f4b914d6db35e19101ff003c4e7ea3a\",\"img\":\"fashion\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-one-page-slider-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"  <span class=\\\"ttm_content\\\">A front-page slider that can be used as an intro for your website. Navigate by using mouse-wheel scroll or bullets.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"5\":{\"id\":\"6\",\"title\":\"Flickr Gallery\",\"alias\":\"flickr-gallery\",\"zip\":\"flickr-gallery.zip\",\"uid\":\"ad85cfac7acfa678e6a1b8febfee51ed\",\"img\":\"flickr-gallery\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-flickr-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"800\",\"height\":\"640\",\"description\":\" <span class=\\\"ttm_content\\\"> This slider is automatically populated by a Flickr stream. Just one slide is needed to show a whole feed!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The content source is set to <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/special-content-sources\\/\\\" target=\\\"_blank\\\">\\\"Flickr Stream\\\"<\\/a>. Make sure to input your correct API information in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\",\"socialmedia\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"6\":{\"id\":\"7\",\"title\":\"Gym\",\"alias\":\"gym\",\"zip\":\"gym.zip\",\"uid\":\"e4d81f13f96fb9bc905f4ad89615032b\",\"img\":\"gym\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/one-page-wordpress-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A front-page slider that can be used as an intro for your website. Navigate by using mouse-wheel scroll or the slider menu.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"7\":{\"id\":\"8\",\"title\":\"Highlight Carousel\",\"alias\":\"highlight-carousel\",\"zip\":\"highlight-carousel.zip\",\"uid\":\"ada52163f723a942f782351fa0396b3d\",\"img\":\"highlight-carousel\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-swipe-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"800\",\"height\":\"720\",\"description\":\"  <span class=\\\"ttm_content\\\">A swipe-controlled carousel with an additional tab-based navigation.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\\r\\n\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"8\":{\"id\":\"9\",\"title\":\"Highlight Showcase\",\"alias\":\"highlight-showcase\",\"zip\":\"highlight-showcase.zip\",\"uid\":\"2bfe0bd410fb48fec9d942eab1e21530\",\"img\":\"highlight-showcase\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-parallax-slideshow\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1230\",\"height\":\"720\",\"description\":\"<span class=\\\"ttm_content\\\">A tab-based navigation, a slideshow timer and cool animations make this slider interesting!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"9\":{\"id\":\"10\",\"title\":\"Image Hero\",\"alias\":\"image-hero\",\"zip\":\"imagehero.zip\",\"uid\":\"7db18781d44f2adc28c962440894aac1\",\"img\":\"imagehero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-hero-image\\/\",\"version\":\"1.0.2\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A commonly used full-width image hero block with texts.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"10\":{\"id\":\"11\",\"title\":\"Instagram Gallery\",\"alias\":\"insta-gallery\",\"zip\":\"insta-gallery.zip\",\"uid\":\"711732b0d42ec2b57818a2b9b1d86cba\",\"img\":\"insta-gallery\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-instagram-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"640\",\"height\":\"640\",\"description\":\"<span class=\\\"ttm_content\\\">This slider is automatically populated by a Instagram stream. Just one slide is needed to show a whole feed!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The content source is set to <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/special-content-sources\\/\\\" target=\\\"_blank\\\">\\\"Instagram Stream\\\"<\\/a>. Make sure to input your correct API information in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\",\"socialmedia\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"11\":{\"id\":\"12\",\"title\":\"Levano Restaurant Bar\",\"alias\":\"levanorestaurantbar\",\"zip\":\"levanorestaurantbar.zip\",\"uid\":\"4178f837db67d1b2eb6cb5840bbd0b42\",\"img\":\"levanorestaurantbar\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-front-page-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A front-page slider that can be used as an intro for your website. Navigate by using mouse-wheel scroll or bullets.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"12\":{\"id\":\"13\",\"title\":\"Main Feature Slider\",\"alias\":\"mainfeature\",\"zip\":\"mainfeature.zip\",\"uid\":\"1e002a3230ab00095bedc6f60393ee7f\",\"img\":\"mainfeature\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1230\",\"height\":\"750\",\"description\":\" <span class=\\\"ttm_content\\\">A slightly stripped down version of the main preview slider.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"13\":{\"id\":\"14\",\"title\":\"Media Gallery Two\",\"alias\":\"media-gallery-two\",\"zip\":\"media-gallery-two.zip\",\"uid\":\"d002f1b1b55805f9322c264c5504ba5a\",\"img\":\"media-gallery-two\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-media-gallery-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1230\",\"height\":\"692\",\"description\":\"<span class=\\\"ttm_content\\\">A media gallery example with Vimeo, HTML5 and Youtube videos.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change media and titles.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"14\":{\"id\":\"15\",\"title\":\"Media Carousel Autoplay\",\"alias\":\"media-carousel-autoplay\",\"zip\":\"media-carousel-autoplay.zip\",\"uid\":\"393d7875b1cc9d933378b35e4f645d76\",\"img\":\"media-carousel-autoplay\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-media-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"720\",\"height\":\"405\",\"description\":\"<span class=\\\"ttm_content\\\">A media carousel with \\\"autoplay\\\" activated. Swipe or click tabs to navigate the slider.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change media and caption texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"15\":{\"id\":\"16\",\"title\":\"News Background Video\",\"alias\":\"news-background-video\",\"zip\":\"news-bg-video.zip\",\"uid\":\"a0efe44ac3af0f958b3f84c816a08272\",\"img\":\"news-bg-video\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-content-blocks\\/\",\"version\":\"1.0.2\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"500\",\"description\":\"<span class=\\\"ttm_content\\\">A hero block with HTML5 background video that plays when entering the screen.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background video and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"16\":{\"id\":\"17\",\"title\":\"News Gallery\",\"alias\":\"news-gallery\",\"zip\":\"news-gallery.zip\",\"uid\":\"3a069c3b286dbb9ee435563f747e3300\",\"img\":\"news-gallery\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-news-rotator\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"500\",\"description\":\"<span class=\\\"ttm_content\\\">A news gallery slideshow with tab navigation. Great for any blog!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"18\":{\"id\":\"19\",\"title\":\"News Hero\",\"alias\":\"news-hero\",\"zip\":\"news-hero.zip\",\"uid\":\"96a0385538a17c8c81ed8175740f70ea\",\"img\":\"news-hero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-content-blocks\\/\",\"version\":\"1.0.1\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"500\",\"description\":\"<span class=\\\"ttm_content\\\">A image hero block with ken burns effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"19\":{\"id\":\"20\",\"title\":\"News Video\",\"alias\":\"news-video\",\"zip\":\"news-video.zip\",\"uid\":\"f901e9e16e0363248156c2209eb584e9\",\"img\":\"news-video\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-content-blocks\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"500\",\"description\":\"<span class=\\\"ttm_content\\\">A Vimeo background video hero block with play \\/ pause buttons.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the video and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"20\":{\"id\":\"21\",\"title\":\"Newsletter Hero\",\"alias\":\"newsletter-hero\",\"zip\":\"newsletter-hero.zip\",\"uid\":\"6290a9864d8c4c6311784586ed1cc5fe\",\"img\":\"newsletter-hero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-newsletter-signup\\/\",\"version\":\"1.0.1\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">This hero block uses a custom styled Mailchimp newsletter signup field.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image, texts, and <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/how-to-setup-mailchimp-for-the-newsletter-hero-template\\/\\\" target=\\\"_blank\\\">Mailchimp code<\\/a>.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"21\":{\"id\":\"22\",\"title\":\"Not Generic\",\"alias\":\"notgeneric\",\"zip\":\"notgeneric.zip\",\"uid\":\"9d87ba95e02210a9f82387add2ceadf9\",\"img\":\"notgeneric\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-fullscreen-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with a layer based navigation and sleek content elements.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"22\":{\"id\":\"23\",\"title\":\"Photography\",\"alias\":\"photography\",\"zip\":\"photography.zip\",\"uid\":\"1b2072547afb75e49f33b016751ed360\",\"img\":\"photography\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-photography-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A front-page slider that can be used as an intro for your website. Navigate by using mouse-wheel scroll or the layer-based menu.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"23\":{\"id\":\"24\",\"title\":\"Photography Carousel\",\"alias\":\"photography-carousel\",\"zip\":\"photography-carousel.zip\",\"uid\":\"9a84b859ba23dc49ba8784e3a86545fa\",\"img\":\"photography-carousel\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-photo-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1024\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A front-page carousel that can be used as an intro for your website. Navigate by using mouse-wheel scroll or the layer-based menu.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"24\":{\"id\":\"25\",\"title\":\"Search Form Hero\",\"alias\":\"search-form-hero\",\"zip\":\"search-form-hero.zip\",\"uid\":\"e09eb1bd0f22b3a2b02a1aa251dd1f3e\",\"img\":\"search-form-hero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-search-form-hero\\/\",\"version\":\"1.0.1\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">This hero block uses a custom styled input field to show search results on a WordPress site.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image, texts, and search field code.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"25\":{\"id\":\"26\",\"title\":\"Showcase Carousel\",\"alias\":\"showcasecarousel\",\"zip\":\"showcasecarousel.zip\",\"uid\":\"c5ca218398331bd2c064efc2f62eae56\",\"img\":\"showcasecarousel\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-cover-flow-gallery\\/\",\"version\":\"1.0.1\",\"required\":\"5.0.5\",\"width\":\"600\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A fancy carousel that rotates, fades and scales slider items.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\\r\\n\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content carousel. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"26\":{\"id\":\"27\",\"title\":\"Sports Hero\",\"alias\":\"sports-hero\",\"zip\":\"sportshero.zip\",\"uid\":\"8de7a145f32a362d618d2595ffa2f724\",\"img\":\"sportshero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-image-hero\\/\",\"version\":\"1.0.2\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"720\",\"description\":\"<span class=\\\"ttm_content\\\">An sports themed image hero block with buttons.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"27\":{\"id\":\"28\",\"title\":\"Twitter Feed\",\"alias\":\"twitter-feed\",\"zip\":\"twitter-feed.zip\",\"uid\":\"efbfc2af5da5258e7b7bed8598e483cc\",\"img\":\"twitter-feed\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-twitter-feed\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"800\",\"height\":\"640\",\"description\":\"<span class=\\\"ttm_content\\\"> This slider is automatically populated by a Twitter stream. Just one slide is needed to show a whole feed!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The content source is set to <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/special-content-sources\\/\\\" target=\\\"_blank\\\">\\\"Twitter Stream\\\"<\\/a>. Make sure to input your correct API information in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\",\"socialmedia\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"28\":{\"id\":\"29\",\"title\":\"Vimeo Gallery\",\"alias\":\"vimeo-gallery\",\"zip\":\"vimeo-gallery.zip\",\"uid\":\"fa824ce1ff3942ec268fc9eda60df539\",\"img\":\"vimeo-gallery\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-vimeo-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1230\",\"height\":\"692\",\"description\":\"<span class=\\\"ttm_content\\\">This slider is automatically populated by a Vimeo stream. Just one slide is needed to show a whole feed!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The content source is set to <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/special-content-sources\\/\\\" target=\\\"_blank\\\">\\\"Vimeo Stream\\\"<\\/a>. Make sure to input your correct API information in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\",\"socialmedia\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"29\":{\"id\":\"30\",\"title\":\"Vimeo Hero\",\"alias\":\"vimeohero\",\"zip\":\"vimeohero.zip\",\"uid\":\"c575575f96173d88589cddcb06120b77\",\"img\":\"vimeohero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-hero-vimeo\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A commonly used Vimeo video background hero block.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background video and texts.<br\\/><br\\/><\\/span>\\r\\nScreencast for <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=I4qGepR4aWw\\\" target=\\\"_blank\\\">mobile video<\\/a> considerations.<\\/a>\\r\\n<br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"30\":{\"id\":\"31\",\"title\":\"Web Product Dark\",\"alias\":\"web-product-dark\",\"zip\":\"web-product-dark.zip\",\"uid\":\"39b872cf0608e63c3a503e58374dc30a\",\"img\":\"web-product-dark\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-frontpage-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1400\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A front-page slider that can be used as an intro for your website. Navigate by using mouse-wheel scroll or the layer-based menu.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"31\":{\"id\":\"32\",\"title\":\"Web Product Dark Hero\",\"alias\":\"web-product-dark-hero\",\"zip\":\"web-product-dark-hero.zip\",\"uid\":\"b6784e8925221f36677217979d26e6f0\",\"img\":\"web-product-dark-hero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-header-image\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1400\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">A modern hero block with three common devices. The screens of each device can be changed easily!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"32\":{\"id\":\"33\",\"title\":\"Web Product Light Hero\",\"alias\":\"web-product-light-hero\",\"zip\":\"web-product-light-hero.zip\",\"uid\":\"428e65d6aaa6ef775429989d50516492\",\"img\":\"web-product-light-hero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/hero-image-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1400\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">A modern hero block with three common devices. The screens of each device can be changed easily!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"33\":{\"id\":\"34\",\"title\":\"Web Product Light\",\"alias\":\"webproductlight\",\"zip\":\"webproductlight.zip\",\"uid\":\"fa23dab5bf1139c6393828647a9de4e0\",\"img\":\"webproductlight\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-cover-page\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1400\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A front-page slider that can be used as an intro for your website. Navigate by using mouse-wheel scroll or the layer-based menu.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Instructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-burger-menu\\/\\\" target=\\\"_blank\\\">changing the menu<\\/a>.<\\/span><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"34\":{\"id\":\"35\",\"title\":\"Youtube Gallery\",\"alias\":\"youtube-gallery\",\"zip\":\"youtube-gallery.zip\",\"uid\":\"ee9e4928ac74f5f0c0b697ce708f5aa7\",\"img\":\"youtube-gallery\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-youtube-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\"> This slider is automatically populated by a YouTube stream. Just one slide is needed to show a whole feed!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The content source is set to <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/special-content-sources\\/\\\" target=\\\"_blank\\\">\\\"YouTube Stream\\\"<\\/a>. Make sure to input your correct API information in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"slider\",\"socialmedia\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"35\":{\"id\":\"36\",\"title\":\"Youtube Hero\",\"alias\":\"youtubehero\",\"zip\":\"youtubehero.zip\",\"uid\":\"e0b2c12a45841bdf21cb96305f2c85bf\",\"img\":\"youtubehero\\/slider.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-youtube-header\\/\",\"version\":\"1.0.2\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A YouTube video background hero block.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background video and texts.<br\\/><br\\/><\\/span>\\r\\nScreencast for <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=I4qGepR4aWw\\\" target=\\\"_blank\\\">mobile video<\\/a> considerations.<\\/a>\\r\\n<br\\/><br\\/>\\r\\nLearn how to <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/enable-sound-for-the-youtube-hero-template\\/\\\" target=\\\"_blank\\\">enable sound for the background video<\\/a><br \\/><br \\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Revolution Base\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"36\":{\"id\":\"37\",\"title\":\"Scroll Effect\",\"alias\":\"scroll-effect\",\"zip\":\"scroll-effect.zip\",\"uid\":\"417f59e9db87aa7e47c8509eb88d4af6\",\"img\":\"scrolleffect\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/big-bold-fullscreen-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with a unique scroll effect and big, bold text.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>For further configuration options please view the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">\\\"Custom CSS\\/JS\\\"<\\/a> section in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"37\":{\"id\":\"38\",\"title\":\"Content Zoom\",\"alias\":\"content-zoom\",\"zip\":\"content-zoom.zip\",\"uid\":\"42ef8cdb70d42ec6ff6fa3b69a027b5f\",\"img\":\"contentzoom\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/content-zoom-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A very unique full-width slider that uses <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> to their potential.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"38\":{\"id\":\"39\",\"title\":\"Food Carousel\",\"alias\":\"food-carousel\",\"zip\":\"food-carousel.zip\",\"uid\":\"a7bf54527b6658a0a308c70c729779fe\",\"img\":\"foodcarousel\\/slider_cover.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/food-carousel-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A fancy carousel with detail content on each slide. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> are used to show and hide layers.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"39\":{\"id\":\"40\",\"title\":\"Rotating Words\",\"alias\":\"rotating-words\",\"zip\":\"rotating-words.zip\",\"uid\":\"70190463d9376f278c8197b7b2ef6c1b\",\"img\":\"rotatingwords\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/rotating-words-hero\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen hero block with unique animated text and blur effects.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" hero slider. Edit the slide to change the background media and texts.<br\\/>For further configuration options please view the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">\\\"Custom CSS\\/JS\\\"<\\/a> section in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"40\":{\"id\":\"41\",\"title\":\"Travel Static Captions\",\"alias\":\"travel-static-captions\",\"zip\":\"travel-static-captions.zip\",\"uid\":\"bbe7d61c7c741ebc7aa1ce1082b0cb71\",\"img\":\"travel-static-captions\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/travel-slideshow\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">A slideshow example with static captions layers above a rotating image background.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"41\":{\"id\":\"42\",\"title\":\"Concept\",\"alias\":\"concept\",\"zip\":\"concept.zip\",\"uid\":\"47cb06083e87503762f6746725117a3c\",\"img\":\"concept\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/fullscreen-wordpress-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider that uses <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> to their potential.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"42\":{\"id\":\"43\",\"title\":\"True Fullscreen\",\"alias\":\"fullscreen-button\",\"zip\":\"fullscreen-button.zip\",\"uid\":\"618a43bdf89333b555531f6d6ecde359\",\"img\":\"fullscreen-button\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-sidebar-slideshow\\/\",\"version\":\"1.0.0\",\"required\":\"5.1\",\"width\":\"1240\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">A slider with a full-screen button that uses <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> to their potential.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change the background images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"43\":{\"id\":\"44\",\"title\":\"Creative Freedom\",\"alias\":\"creativefreedom\",\"zip\":\"creativefreedom.zip\",\"uid\":\"8902bf6c93126c2c6323626b9e872621\",\"img\":\"creativefreedom\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-touch-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.1\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with a unique vertical navigation and 3D-parallax effects.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images, videos and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"44\":{\"id\":\"45\",\"title\":\"3D Parallax Scene\",\"alias\":\"parallaxscene\",\"zip\":\"parallaxscene.zip\",\"uid\":\"51566f1ce649708e97a0f5dfaf05ea19\",\"img\":\"parallaxscene\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-3d-parallax\\/\",\"version\":\"1.0.2\",\"required\":\"5.1\",\"width\":\"1240\",\"height\":\"700\",\"description\":\"<span class=\\\"ttm_content\\\">A 3D-parallax hero scene with mouse-controlled effects. Smart image arrangement creates a convincing 3D effect. Tooltips are shown using <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a>.<\\/span>\\r\\n<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"45\":{\"id\":\"46\",\"title\":\"Wow Factor\",\"alias\":\"slidingoverlays\",\"zip\":\"slidingoverlays.zip\",\"uid\":\"18002d17e8bc7ca61b0951f5305a759e\",\"img\":\"slidingoverlays\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/responsive-wordpress-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.1\",\"width\":\"1240\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">A 3D-parallax effect full-screen slider.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\nScreencast for <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=I4qGepR4aWw\\\" target=\\\"_blank\\\">mobile video<\\/a> considerations.<\\/a>\\r\\n<br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"46\":{\"id\":\"47\",\"title\":\"3D Web Product\",\"alias\":\"web-product-light-hero-3d\",\"zip\":\"web-product-light-hero-3d.zip\",\"uid\":\"efd345c5da805414093e46066cefd751\",\"img\":\"web-product-light-hero-3d\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-3d-scene\\/\",\"version\":\"1.0.0\",\"required\":\"5.1\",\"width\":\"1400\",\"height\":\"668\",\"description\":\"<span class=\\\"ttm_content\\\">A 3D-parallax hero scene slider with a convincing depth-effect and animated clouds.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"49\":{\"id\":\"50\",\"title\":\"Restaurant Header\",\"alias\":\"finedining\",\"zip\":\"finedining.zip\",\"uid\":\"03481a9e258501fbe3888b48830ea845\",\"img\":\"finedining\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/restaurant-hero-header\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen hero block slider that shows different background layers on button hover using <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a>.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\nScreencast for <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=I4qGepR4aWw\\\" target=\\\"_blank\\\">mobile video<\\/a> considerations.<\\/a>\\r\\n<br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"50\":{\"id\":\"51\",\"title\":\"Agency Website Header\",\"alias\":\"agency-intro\",\"zip\":\"agency-intro.zip\",\"uid\":\"e9be2afdd55a0c8d0d8d0cc12ba4c82f\",\"img\":\"agency-intro\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/agency-website-hero-header\\/\",\"version\":\"1.0.0\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\" <span class=\\\"ttm_content\\\">A full-screen hero block slider with ken burns effect and video modal on button click. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> are used to achieve this effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"51\":{\"id\":\"52\",\"title\":\"Team Slider\",\"alias\":\"ourteam\",\"zip\":\"ourteam.zip\",\"uid\":\"d8eb1fec07a79202d1118bb17f70c233\",\"img\":\"ourteam\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/thumbnail-hover-slider-revolution-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.4\",\"width\":\"1400\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">A team slider that changes slides and displays additional information on thumbnail hover. It uses <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> to achieve this effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\nLearn how to <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-thumbnails-for-our-team-template\\/\\\" target=\\\"_blank\\\">modify the thumbnails<\\/a><br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"52\":{\"id\":\"53\",\"title\":\"Team Carousel\",\"alias\":\"our-team-carousel\",\"zip\":\"our-team-carousel.zip\",\"uid\":\"37656d5dcab1f265c025cb2a3d669809\",\"img\":\"ourteamcarousel\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/thumbnail-hover-carousel-slider-revolution-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.4\",\"width\":\"1000\",\"height\":\"800\",\"description\":\" <span class=\\\"ttm_content\\\">A team carousel that changes slides and displays additional information on thumbnail hover. It uses <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> to achieve this effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content carousel. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\nLearn how to <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/modify-thumbnails-for-our-team-template\\/\\\" target=\\\"_blank\\\">modify the thumbnails<\\/a><br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"53\":{\"id\":\"54\",\"title\":\"4K Youtube Slider\",\"alias\":\"betteryoutube\",\"zip\":\"betteryoutube.zip\",\"uid\":\"d7c5fef9f257037d7a148038aa2a1857\",\"img\":\"betteryoutube\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/fullscreen-4k-youtube-video-gallery-slider-revolution-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.4\",\"width\":\"1200\",\"height\":\"675\",\"description\":\" <span class=\\\"ttm_content\\\">A full-screen youtube gallery with 4K videos as an example. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> are used to breathe life into navigation elements.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change videos and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/><br\\/><\\/span>\\r\\nScreencast for <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=I4qGepR4aWw\\\" target=\\\"_blank\\\">mobile video<\\/a> considerations.<\\/a>\\r\\n<br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"54\":{\"id\":\"55\",\"title\":\"Agency Website Header\",\"alias\":\"agencywebsiteheader\",\"zip\":\"agencywebsiteheader.zip\",\"uid\":\"c11d272c87277a7a38d9e7fd13e27947\",\"img\":\"agencywebsiteheader\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/fullscreen-hero-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\" <span class=\\\"ttm_content\\\">A very unique full-screen slider that uses <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> to their potential.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"55\":{\"id\":\"56\",\"title\":\"Coming Soon\",\"alias\":\"comingsoon\",\"zip\":\"comingsoon.zip\",\"uid\":\"0f89edf995512e6d6e864a2ee7221829\",\"img\":\"comingsoon\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/coming-soon-wordpress-under-construction-site\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider that can be used as a \\\"Coming Soon\\\" page. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a> are used to achieve various interaction possibilities.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>For further configuration options please view the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">\\\"Custom CSS\\/JS\\\"<\\/a> section in the slider settings.<br\\/><br\\/><\\/span>\\r\\nInstructions for <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/change-date-for-coming-soon-template\\/\\\" target=\\\"_blank\\\">how to change the date.<\\/a><br \\/><br \\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"56\":{\"id\":\"57\",\"title\":\"Christmas Snow Scene\",\"alias\":\"snowscene\",\"zip\":\"snowscene.zip\",\"uid\":\"a747bf0837caff86c62419a746690209\",\"img\":\"snowscene\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/christmas-snow-scene\\/\",\"version\":\"1.0.5\",\"required\":\"5.0.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A slider with multiple christmas themed slides and a neat snow fall effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>For further configuration options please view the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">\\\"Custom CSS\\/JS\\\"<\\/a> section in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"57\":{\"id\":\"58\",\"title\":\"Rock Band Music\",\"alias\":\"rockband\",\"zip\":\"rockband.zip\",\"uid\":\"5f557a5fad2fd6ca982a6ae6e9619637\",\"img\":\"rockband\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/rock-band-music-theme-slider-revolution-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\" <span class=\\\"ttm_content\\\">A rock band themed hero block slider with an embedded SoundCloud element.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background video and texts.<br\\/><br\\/><\\/span>\\r\\nScreencast for <a href=\\\"https:\\/\\/www.youtube.com\\/watch?v=I4qGepR4aWw\\\" target=\\\"_blank\\\">mobile video<\\/a> considerations.<\\/a>\\r\\n<br\\/><br\\/>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"2016-02-24 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"58\":{\"id\":\"59\",\"title\":\"Sleek Landing Page\",\"alias\":\"sleeklandingpage\",\"zip\":\"sleeklandingpage.zip\",\"uid\":\"07f7f2608936a8ae226213b7d99eee3f\",\"img\":\"sleeklandingpage\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/landing-page-free-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.1.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider that can be used as an App-Landing-Page. The object layers and custom animations create a unique effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-02-22 13:21:33\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"59\":{\"id\":\"60\",\"title\":\"App Landing Page\",\"alias\":\"applandingpage\",\"zip\":\"applandingpage.zip\",\"uid\":\"38eb600893c1484ab8fd2a3dce3678d7\",\"img\":\"applandingpage\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/app-landing-page-free-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen hero block that can be used as an app landing page. The detail view is build using <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a>.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>It might be helpful to use the \\\"hide\\/show layer\\\" feature to efficiently edit all content.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"60\":{\"id\":\"61\",\"title\":\"Desk Scene\",\"alias\":\"deskscene\",\"zip\":\"deskscene.zip\",\"uid\":\"172a1a89a3e3b333cb4dbeb238f534c5\",\"img\":\"deskscene\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/hero-desk-scene\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A beautiful desk scene with a cool reveal effect when scrolling down.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"61\":{\"id\":\"62\",\"title\":\"Clean News Slider\",\"alias\":\"cleannewsslider\",\"zip\":\"cleannewsslider.zip\",\"uid\":\"82c3f521fe31b80984be0f19e1a9899e\",\"img\":\"cleannewsslider\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-news-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A slideshow example with clean content and thumbnail navigation.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\\r\\n\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"62\":{\"id\":\"63\",\"title\":\"Image Gallery\",\"alias\":\"imagegallery\",\"zip\":\"imagegallery.zip\",\"uid\":\"cef1d75357df2cb53b990c74a214813e\",\"img\":\"imagegallery\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/wordpress-photo-gallery-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.0\",\"width\":\"1000\",\"height\":\"667\",\"description\":\"<span class=\\\"ttm_content\\\">A photo gallery suitable to display your images using <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> to their potential.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"65\":{\"id\":\"66\",\"title\":\"Interactive Whiteboard\",\"alias\":\"interactivewhiteboard\",\"zip\":\"interactivewhiteboard.zip\",\"uid\":\"058b4af535d6c6c7bcec5225845e3eb4\",\"img\":\"interactivewhiteboard\\/slide1.jpg\",\"preview\":\"http:\\/\\/revolution.themepunch.com\\/interactive-whiteboard-animation-free-addon\\/\",\"version\":\"1.0.5\",\"required\":\"5.2.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This is the example slider from the Whiteboard add-on preview.<br\\/><a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> are used to achieve certain effects.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This slider template requires the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/add-on-whiteboard\\/\\\" target=\\\"_blank\\\">Whiteboard add-on<\\/a> to be installed. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-whiteboard-addon\\\\\\/revslider-whiteboard-addon.php\\\",\\\"name\\\":\\\"Whiteboard AddOn\\\",\\\"url\\\":\\\"http:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/interactive-whiteboard-animation-free-addon\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"66\":{\"id\":\"68\",\"title\":\"Innovation Slider\",\"alias\":\"innovation\",\"zip\":\"innovation.zip\",\"uid\":\"d3440b69126d9c186fddc713b18b0002\",\"img\":\"innovation\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/best-wordpress-slider-plugin-2016\\/\",\"version\":\"1.0.5\",\"required\":\"5.2.3.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with an elegant fade-out parallax effect. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> are used for links on layers.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images, videos and texts.<br\\/>For further configuration options please view the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">\\\"Custom CSS\\/JS\\\"<\\/a> section in the slider settings.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"2016-03-29 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"67\":{\"id\":\"69\",\"title\":\"Dark Content Block - Menu\",\"alias\":\"dark-fullsite-block-menu\",\"zip\":\"packs\\/dark-fullsite-block-menu.zip\",\"uid\":\"bf4bd226719d3c3d3146592976e31ca1\",\"img\":\"dark-fullsite-block-menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"0\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"68\":{\"id\":\"70\",\"title\":\"Dark Content Block - Hero\",\"alias\":\"dark-fullsite-block-1\",\"zip\":\"packs\\/dark-fullsite-block-1.zip\",\"uid\":\"d8859245fe600288ca932dc29aca751f\",\"img\":\"dark-fullsite-block-1\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"69\":{\"id\":\"71\",\"title\":\"Dark Content Block - About\",\"alias\":\"dark-fullsite-block-2-about\",\"zip\":\"packs\\/dark-fullsite-block-2-about.zip\",\"uid\":\"298401901521a197a8294c1b4f73df3e\",\"img\":\"dark-fullsite-block-2-about\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"70\":{\"id\":\"72\",\"title\":\"Dark Content Block - Services\",\"alias\":\"dark-fullsite-block-3-services\",\"zip\":\"packs\\/dark-fullsite-block-3-services.zip\",\"uid\":\"8da3ed605472ed6a26c745fb52f85f82\",\"img\":\"dark-fullsite-block-3-services\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"71\":{\"id\":\"73\",\"title\":\"Dark Content Block - Products\",\"alias\":\"dark-fullsite-block-4-products\",\"zip\":\"packs\\/dark-fullsite-block-4-products.zip\",\"uid\":\"99e4f40399d20261a6131043d0e50b0c\",\"img\":\"dark-fullsite-block-4-products\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\\r\\n\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"72\":{\"id\":\"74\",\"title\":\"Dark Content Block - Testimonials\",\"alias\":\"dark-fullsite-block-5-testimonials\",\"zip\":\"packs\\/dark-fullsite-block-5-testimonials.zip\",\"uid\":\"2f9121e0eedd51afe85e233d0743acab\",\"img\":\"dark-fullsite-block-5-testimonials\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"73\":{\"id\":\"75\",\"title\":\"Dark Content Block - Callout\",\"alias\":\"dark-fullsite-block-6-callout\",\"zip\":\"packs\\/dark-fullsite-block-6-callout.zip\",\"uid\":\"55fbecb113ff21f050be7adc08637329\",\"img\":\"dark-fullsite-block-6-callout\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"74\":{\"id\":\"76\",\"title\":\"Dark Content Block - Footer\",\"alias\":\"dark-fullsite-block-7-footer\",\"zip\":\"packs\\/dark-fullsite-block-7-footer.zip\",\"uid\":\"2ff517f5c7c54e3e0c2dd733cfd3400e\",\"img\":\"dark-fullsite-block-7-footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"75\":{\"id\":\"77\",\"title\":\"Light Content Block - Menu\",\"alias\":\"fullsite-block-menu\",\"zip\":\"packs\\/fullsite-block-menu.zip\",\"uid\":\"8010971f34387d5f94a1f4e577ef382a\",\"img\":\"fullsite-block-menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"0\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"76\":{\"id\":\"78\",\"title\":\"Light Content Block - Hero\",\"alias\":\"fullsite-block1\",\"zip\":\"packs\\/fullsite-block1.zip\",\"uid\":\"45bf7179843e01ce112e8ec754b0455c\",\"img\":\"fullsite-block1\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"77\":{\"id\":\"79\",\"title\":\"Light Content Block - About\",\"alias\":\"fullsite-block-2-about\",\"zip\":\"packs\\/fullsite-block-2-about.zip\",\"uid\":\"8402b460638a746d36433cb161440b7d\",\"img\":\"fullsite-block-2-about\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"78\":{\"id\":\"80\",\"title\":\"Light Content Block - Services\",\"alias\":\"fullsite-block-3-services\",\"zip\":\"packs\\/fullsite-block-3-services.zip\",\"uid\":\"7b8cea27f4d6ae17c5e1eae12457bd06\",\"img\":\"fullsite-block-3-services\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"79\":{\"id\":\"81\",\"title\":\"Light Content Block - Products\",\"alias\":\"fullsite-block-4-products\",\"zip\":\"packs\\/fullsite-block-4-products.zip\",\"uid\":\"d148935362d7122636cda0635eae4be7\",\"img\":\"fullsite-block-4-products\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"80\":{\"id\":\"82\",\"title\":\"Light Content Block - Testimonials\",\"alias\":\"fullsite-block-5-testimonials\",\"zip\":\"packs\\/fullsite-block-5-testimonials.zip\",\"uid\":\"06255bfd421e40f71fa8197b839dbf03\",\"img\":\"fullsite-block-5-testimonials\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"81\":{\"id\":\"83\",\"title\":\"Light Content Block - Callout\",\"alias\":\"fullsite-block-6-callout\",\"zip\":\"packs\\/fullsite-block-6-callout.zip\",\"uid\":\"8a88c3447676b613ec5db2fe5d63315c\",\"img\":\"fullsite-block-6-callout\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"82\":{\"id\":\"84\",\"title\":\"Light Content Block - Footer\",\"alias\":\"fullsite-block-7-footer\",\"zip\":\"packs\\/fullsite-block-7-footer.zip\",\"uid\":\"34300b4407a2093eb2e1e08868fa8319\",\"img\":\"fullsite-block-7-footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"83\":{\"id\":\"85\",\"title\":\"Tech Journal\",\"alias\":\"techjournal\",\"zip\":\"techjournal.zip\",\"uid\":\"7f92d70d1c108378f915b18c2ceb71d6\",\"img\":\"techjournal\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.1\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">A full-width slider with navigation direction-based transitions.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"84\":{\"id\":\"86\",\"title\":\"Car Dealership\",\"alias\":\"cardealership\",\"zip\":\"cardealership.zip\",\"uid\":\"bb010838855a8ae4d1dd68e139bf169e\",\"img\":\"cardealership\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/car-dealership-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.1\",\"width\":\"1240\",\"height\":\"650\",\"description\":\"<span class=\\\"ttm_content\\\">A full-width slider with navigation direction-based transitions.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"85\":{\"id\":\"87\",\"title\":\"FullScreen Menu Slider\",\"alias\":\"fullscreenmenu\",\"zip\":\"fullscreenmenu.zip\",\"uid\":\"6de81d74c4bf193e2e274d43038adc85\",\"img\":\"fullscreenmenu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-fullscreen-menu\\/\",\"version\":\"1.0.1\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider that contains a menu with slide blur effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"86\":{\"id\":\"88\",\"title\":\"Creative Frontpage\",\"alias\":\"creativefrontpage\",\"zip\":\"creativefrontpage.zip\",\"uid\":\"0636cb368af3bcaa2f68eb0ebefe6439\",\"img\":\"creativefrontpage\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/best-wordpress-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with hidden slides that are triggered via  <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\" target=\\\"_blank\\\">\\\"Layer Actions\\\"<\\/a>.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"87\":{\"id\":\"89\",\"title\":\"Website Builder Menu\",\"alias\":\"websitebuilder-menu\",\"zip\":\"packs\\/websitebuilder-menu.zip\",\"uid\":\"d9e6d9c961f123a3a4847b51fc3ba3a2\",\"img\":\"websitebuilder-menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"60\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"0\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"88\":{\"id\":\"90\",\"title\":\"Website Builder Hero\",\"alias\":\"websitebuilder-hero\",\"zip\":\"packs\\/websitebuilder-hero.zip\",\"uid\":\"6cb2df9a41ad2e7865bb9bbea7e39cb5\",\"img\":\"websitebuilder-hero\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1230\",\"height\":\"700\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"89\":{\"id\":\"91\",\"title\":\"Website Builder Clients\",\"alias\":\"websitebuilder-clients\",\"zip\":\"packs\\/websitebuilder-clients.zip\",\"uid\":\"050e59c43c9a693510d01f29532088cf\",\"img\":\"websitebuilder-clients\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"120\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"90\":{\"id\":\"92\",\"title\":\"Website Builder Services\",\"alias\":\"websitebuilder-services\",\"zip\":\"packs\\/websitebuilder-services.zip\",\"uid\":\"48d56d67615efce619ae973ab4358c07\",\"img\":\"websitebuilder-services\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.1\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"558\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"91\":{\"id\":\"93\",\"title\":\"Website Builder Discover\",\"alias\":\"websitebuilder-discover\",\"zip\":\"packs\\/websitebuilder-discover.zip\",\"uid\":\"425a08a7be338daea3df02a07ff5c316\",\"img\":\"websitebuilder-discover\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"955\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"92\":{\"id\":\"94\",\"title\":\"Website Builder Slider\",\"alias\":\"websitebuilder-slider\",\"zip\":\"packs\\/websitebuilder-slider.zip\",\"uid\":\"9d670b7335016accb590dc8d52bfb6f0\",\"img\":\"websitebuilder-slider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"500\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"93\":{\"id\":\"95\",\"title\":\"Website Builder CallToAction\",\"alias\":\"websitebuilder-calltoaction\",\"zip\":\"packs\\/websitebuilder-calltoaction.zip\",\"uid\":\"45851baf9e61f55ed9f5fa9d0beff77e\",\"img\":\"websitebuilder-calltoaction\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"960\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"94\":{\"id\":\"96\",\"title\":\"Website Builder Footer\",\"alias\":\"websitebuilder-footer\",\"zip\":\"packs\\/websitebuilder-footer.zip\",\"uid\":\"2732bbe66ef28219d6cc53ce8cc78863\",\"img\":\"websitebuilder-footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"95\":{\"id\":\"97\",\"title\":\"Focus Parallax Effect\",\"alias\":\"focusparallax\",\"zip\":\"focusparallax.zip\",\"uid\":\"13bd15fb3ddf8b9841cb55b89389cc73\",\"img\":\"focusparallax\\/slide1.jpg\",\"preview\":\" https:\\/\\/revolution.themepunch.com\\/parallax-effect-wordpress-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with mouse controlled parallax effects. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> are used for links on layers.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/>Some content is in <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">\\\"Static \\/ Global Layers\\\"<\\/a><br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"96\":{\"id\":\"98\",\"title\":\"Duotone Slider\",\"alias\":\"duotone\",\"zip\":\"duotone.zip\",\"uid\":\"494862ceb6cb7c6658ad7cd36848cccd\",\"img\":\"duotone\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-header-image-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.4\",\"width\":\"1200\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider with unique parallax scroll effect. <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> are used for links on layers.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"97\":{\"id\":\"99\",\"title\":\"Minimal Menu\",\"alias\":\"r_menu\",\"zip\":\"packs\\/r_menu.zip\",\"uid\":\"30d6a6895db1a5f29b1679061551c9f0\",\"img\":\"r_menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-website-template\\/\",\"version\":\"1.0.1\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"110\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">\\r\\nIn the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">Custom JavaScript<\\/a> section of the \\\"Minimal Menu\\\" Template you can scroll down to the color change section.<br\\/>\\r\\nFor any elements you want to background color change in addition to the sites body, add this class to the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-attributes\\/\\\" target=\\\"_blank\\\">Layers Attributes<\\/a> \\\".tp-colorchange\\\".<br\\/><br\\/>\\r\\nIf you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Minimal Website Template\",\"package_order\":\"0\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"4\",\"installed\":false},\"98\":{\"id\":\"100\",\"title\":\"Minimal Hero\",\"alias\":\"rhero\",\"zip\":\"packs\\/r_hero.zip\",\"uid\":\"b8b46186956f6e66ad0c08e4532bbbde\",\"img\":\"r_hero\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-website-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"700\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">\\r\\nIn the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">Custom JavaScript<\\/a> section of the \\\"Minimal Menu\\\" Template you can scroll down to the color change section.<br\\/>\\r\\nFor any elements you want to background color change in addition to the sites body, add this class to the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-attributes\\/\\\" target=\\\"_blank\\\">Layers Attributes<\\/a> \\\".tp-colorchange\\\".<br\\/><br\\/>\\r\\nIf you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Minimal Website Template\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"4\",\"installed\":false},\"99\":{\"id\":\"101\",\"title\":\"Minimal About\",\"alias\":\"r_about\",\"zip\":\"packs\\/r_about.zip\",\"uid\":\"343010c4b4c03f92888c3e9e95bc2bb1\",\"img\":\"r_about\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-website-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">\\r\\nIn the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">Custom JavaScript<\\/a> section of the \\\"Minimal Menu\\\" Template you can scroll down to the color change section.<br\\/>\\r\\nFor any elements you want to background color change in addition to the sites body, add this class to the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-attributes\\/\\\" target=\\\"_blank\\\">Layers Attributes<\\/a> \\\".tp-colorchange\\\".<br\\/><br\\/>\\r\\nIf you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Minimal Website Template\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"4\",\"installed\":false},\"100\":{\"id\":\"102\",\"title\":\"Minimal Products\",\"alias\":\"r_products\",\"zip\":\"packs\\/r_products.zip\",\"uid\":\"dda93e85c6a4456d132040147f087f39\",\"img\":\"r_products\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-website-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"1100\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">\\r\\nIn the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">Custom JavaScript<\\/a> section of the \\\"Minimal Menu\\\" Template you can scroll down to the color change section.<br\\/>\\r\\nFor any elements you want to background color change in addition to the sites body, add this class to the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-attributes\\/\\\" target=\\\"_blank\\\">Layers Attributes<\\/a> \\\".tp-colorchange\\\".<br\\/><br\\/>\\r\\nIf you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Minimal Website Template\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"4\",\"installed\":false},\"101\":{\"id\":\"103\",\"title\":\"Minimal Info\",\"alias\":\"r_info\",\"zip\":\"packs\\/r_info.zip\",\"uid\":\"d6e368e8fd4862174af6c980389ae530\",\"img\":\"r_info\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-website-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">\\r\\nIn the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">Custom JavaScript<\\/a> section of the \\\"Minimal Menu\\\" Template you can scroll down to the color change section.<br\\/>\\r\\nFor any elements you want to background color change in addition to the sites body, add this class to the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-attributes\\/\\\" target=\\\"_blank\\\">Layers Attributes<\\/a> \\\".tp-colorchange\\\".<br\\/><br\\/>\\r\\nIf you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Minimal Website Template\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"4\",\"installed\":false},\"102\":{\"id\":\"104\",\"title\":\"Inspiration Header\",\"alias\":\"inspirationheader\",\"zip\":\"inspirationheader.zip\",\"uid\":\"69f04b2be0b618fa7e184af83b86b7e7\",\"img\":\"inspirationheader\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme-header\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This full-screen hero slider features sequentially animated texts and cool parallax elements.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"103\":{\"id\":\"105\",\"title\":\"Magazine Slider\",\"alias\":\"magazineposts\",\"zip\":\"magazineposts.zip\",\"uid\":\"c562f3457e4edbd030959f7c5e6e0f7c\",\"img\":\"magazineposts\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-magazine-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1400\",\"height\":\"1000\",\"description\":\"<span class=\\\"ttm_content\\\">A full-width slider with a mouse-controlled 3D-Parallax effect. Enjoy! <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> are used for links on layers.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"104\":{\"id\":\"106\",\"title\":\"News Header\",\"alias\":\"explorationheader\",\"zip\":\"explorationheader.zip\",\"uid\":\"8f20d5a868c90ded08b835fb9e8307d7\",\"img\":\"explorationheader\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-news-header\\/\",\"version\":\"1.0.1\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A full-screen slider that is a perfect fit for displaying news on your websites header! Check out <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> to add links to buttons.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"105\":{\"id\":\"107\",\"title\":\"Minimal Website Package\",\"alias\":\"minimal-website-package\",\"zip\":\"\",\"uid\":\"minimal-website-package\",\"img\":\"packages\\/template_group_1_1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-website-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.6\",\"width\":\"1240\",\"height\":\"110\",\"description\":\"<span class=\\\"ttm_content\\\">The Minimal Website Template is a one-pager website with a unique background color change effect on scrolling down.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">\\r\\nIn the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/custom-css-or-javascript-for-version-5-0\\/\\\" target=\\\"_blank\\\">Custom JavaScript<\\/a> section of the \\\"Minimal Menu\\\" Template you can scroll down to the color change section.<br\\/>\\r\\nFor any elements you want to background color change in addition to the sites body, add this class to the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-attributes\\/\\\" target=\\\"_blank\\\">Layers Attributes<\\/a> \\\".tp-colorchange\\\".<br\\/><br\\/>\\r\\nIf you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Minimal Website Template\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"4\",\"installed\":false},\"106\":{\"id\":\"108\",\"title\":\"Website Builder Package\",\"alias\":\"websitebuilder-package\",\"zip\":\"\",\"uid\":\"websitebuilder-package\",\"img\":\"packages\\/template_group_2.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-website-builder\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5.3\",\"width\":\"1240\",\"height\":\"60\",\"description\":\"<span class=\\\"ttm_content\\\">The Website Builder Package is a clean, usable collection website modules that can be used all together or each on their own.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Website Builder\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"3\",\"installed\":false},\"107\":{\"id\":\"109\",\"title\":\"Dark Content Block Package\",\"alias\":\"dark-fullsite-block-package\",\"zip\":\"\",\"uid\":\"dark-fullsite-block-package\",\"img\":\"packages\\/template_group_4.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/dark-wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">This Minimal Website Template has some sleek parallax effects and on-demand loading modules.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite Dark\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"2\",\"installed\":false},\"108\":{\"id\":\"110\",\"title\":\"Light Content Block Package\",\"alias\":\"fullsite-block-package\",\"zip\":\"\",\"uid\":\"fullsite-block-package\",\"img\":\"packages\\/template_group_3.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-parallax-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.2.5\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">This Minimal Website Template has some sleek parallax effects and on-demand loading modules.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Fullsite\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"1\",\"installed\":false},\"109\":{\"id\":\"111\",\"title\":\"Typewriter Effect\",\"alias\":\"typewritereffect\",\"zip\":\"typewritereffect.zip\",\"uid\":\"d6f8bae06cc4a7b158d680c01e59ddc2\",\"img\":\"typewritereffect\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-typewriter-effect\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This is the example slider from the Typewriter add-on preview.<br\\/><a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/layer-settings\\/#layeractions\\\">\\\"Layer Actions\\\"<\\/a> are used for links on buttons.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This slider template requires the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/add-on-typewriter\\/\\\" target=\\\"_blank\\\">Typewriter add-on<\\/a> to be installed. Edit the slides to change images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-typewriter-addon\\\\\\/revslider-typewriter-addon.php\\\",\\\"name\\\":\\\"Typewriter AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/www.themepunch.com\\\\\\/revslider-doc\\\\\\/add-on-typewriter\\\\\\/\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"110\":{\"id\":\"112\",\"title\":\"Blend Mode Header\",\"alias\":\"blendmodeheader\",\"zip\":\"blendmodeheader.zip\",\"uid\":\"2e44e976596d757aab40ffa28086dcf9\",\"img\":\"blendmodeheader\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/blend-mode-hero-header\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"1000\",\"description\":\"<span class=\\\"ttm_content\\\">This full-screen hero header with HMTL5 background video, utilizes the new 5.3 blend mode effect.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"111\":{\"id\":\"113\",\"title\":\"Themeplicity Menu\",\"alias\":\"themeplicity_menu\",\"zip\":\"packs\\/themeplicity_menu.zip\",\"uid\":\"7d5c1e75eaafa63683895a32a62f4ce0\",\"img\":\"themeplicity_menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"90\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"0\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"112\":{\"id\":\"114\",\"title\":\"Themeplicity Header\",\"alias\":\"themeplicity_header\",\"zip\":\"packs\\/themeplicity_header.zip\",\"uid\":\"907091e4d58acc7d12f802de2f280b7d\",\"img\":\"themeplicity_header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"1000\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"113\":{\"id\":\"115\",\"title\":\"Themeplicity Offer\",\"alias\":\"themeplicity_offer\",\"zip\":\"packs\\/themeplicity_offer.zip\",\"uid\":\"1bd8f9aa2f3e340449664c65ba17fb51\",\"img\":\"themeplicity_offer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"330\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"114\":{\"id\":\"116\",\"title\":\"Themeplicity What We Do\",\"alias\":\"themeplicity_whatwedo\",\"zip\":\"packs\\/themeplicity_whatwedo.zip\",\"uid\":\"aa800a44b5a3eab30414dde1f32bfed7\",\"img\":\"themeplicity_whatwedo\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"813\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"115\":{\"id\":\"117\",\"title\":\"Themeplicity Projects\",\"alias\":\"themeplicity_projects\",\"zip\":\"packs\\/themeplicity_projects.zip\",\"uid\":\"5f61b3a13033ba6a51a18270163e0a50\",\"img\":\"themeplicity_projects\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"813\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"116\":{\"id\":\"118\",\"title\":\"Themeplicity Whats Great\",\"alias\":\"themeplicity_whatsgreat\",\"zip\":\"packs\\/themeplicity_whatsgreat.zip\",\"uid\":\"ce9faf8c55ed2e33e091b23667e7173b\",\"img\":\"themeplicity_whatsgreat\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"117\":{\"id\":\"119\",\"title\":\"Themeplicity Tables\",\"alias\":\"themeplicity_tables\",\"zip\":\"packs\\/themeplicity_tables.zip\",\"uid\":\"f28bb4cd20ec1f29a1de7da5ad09c293\",\"img\":\"themeplicity_tables\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"1059\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"118\":{\"id\":\"120\",\"title\":\"Themeplicity Contactform\",\"alias\":\"themeplicity_contactform\",\"zip\":\"packs\\/themeplicity_contactform.zip\",\"uid\":\"3e59da970f534490774fa8e053f5b5ed\",\"img\":\"themeplicity_contactform\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"1067\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"119\":{\"id\":\"121\",\"title\":\"Themeplicity Footer\",\"alias\":\"themeplicity_footer\",\"zip\":\"packs\\/themeplicity_footer.zip\",\"uid\":\"89eeb0b4b852c5f743e1bd76e3e8f2ef\",\"img\":\"themeplicity_footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"780\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"8\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"120\":{\"id\":\"122\",\"title\":\"Themeplicity Package\",\"alias\":\"themeplicity-package\",\"zip\":\"\",\"uid\":\"themeplicity-package\",\"img\":\"packages\\/templatepack_themeplicity.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Themeplicity Website Template uses the new groups & rows feature introduced in 5.3 to its full potential.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Themeplicity Website Template\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"5\",\"installed\":false},\"121\":{\"id\":\"123\",\"title\":\"Nice And Clean Menu\",\"alias\":\"NiceAndClean_Menu\",\"zip\":\"packs\\/NiceAndClean_Menu.zip\",\"uid\":\"3f8c01e26c7446428e045c4b1180776d\",\"img\":\"NiceAndClean_Menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"90\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"0\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"122\":{\"id\":\"124\",\"title\":\"Nice And Clean Header\",\"alias\":\"NiceAndClean_Header\",\"zip\":\"packs\\/NiceAndClean_Header.zip\",\"uid\":\"76931033addb20a62557c2845a4d6a11\",\"img\":\"NiceAndClean_Header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"123\":{\"id\":\"125\",\"title\":\"Nice And Clean Services\",\"alias\":\"NiceAndClean_Services\",\"zip\":\"packs\\/NiceAndClean_Services.zip\",\"uid\":\"02f72ec606800c8215bcadea09624e87\",\"img\":\"NiceAndClean_Services\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"360\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"124\":{\"id\":\"126\",\"title\":\"Nice And Clean About\",\"alias\":\"NiceAndClean_About\",\"zip\":\"packs\\/NiceAndClean_About.zip\",\"uid\":\"9510f6fdbb8e9473e8c22f692a6bc89f\",\"img\":\"NiceAndClean_About\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"125\":{\"id\":\"127\",\"title\":\"Nice And Clean Video\",\"alias\":\"niceandclean_video\",\"zip\":\"packs\\/niceandclean_video.zip\",\"uid\":\"2bb9e1ad329435cc500542d0c7025e15\",\"img\":\"niceandclean_video\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"126\":{\"id\":\"128\",\"title\":\"Nice And Clean Highlights\",\"alias\":\"niceandclean_highlights\",\"zip\":\"packs\\/niceandclean_highlights.zip\",\"uid\":\"ff396af163a79d2d5b35db17c1ea7aa6\",\"img\":\"niceandclean_highlights\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"127\":{\"id\":\"129\",\"title\":\"Nice And Clean Projects\",\"alias\":\"NiceAndClean_Projects\",\"zip\":\"packs\\/NiceAndClean_Projects.zip\",\"uid\":\"1e6695959ef83f8975b52289c08a4d44\",\"img\":\"NiceAndClean_Projects\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"600\",\"height\":\"600\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"128\":{\"id\":\"130\",\"title\":\"Nice And Clean TextBlocks\",\"alias\":\"niceandclean_textblocks\",\"zip\":\"packs\\/niceandclean_textblocks.zip\",\"uid\":\"1f33eb839c96ea3225faff1c8d382b05\",\"img\":\"niceandclean_textblocks\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"129\":{\"id\":\"131\",\"title\":\"Nice And Clean CallOut\",\"alias\":\"niceandclean_callout\",\"zip\":\"packs\\/niceandclean_callout.zip\",\"uid\":\"54d4d9bcf79d357de0e614700e909863\",\"img\":\"niceandclean_callout\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"500\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"8\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"130\":{\"id\":\"132\",\"title\":\"Nice And Clean Footer\",\"alias\":\"niceandclean_footer\",\"zip\":\"packs\\/niceandclean_footer.zip\",\"uid\":\"5492d7f72b3771a4e754f91bda063b15\",\"img\":\"niceandclean_footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"400\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">For using the social sharing buttons, please install the social sharing add-on.<br><br><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"9\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"131\":{\"id\":\"134\",\"title\":\"80s Style Intro\",\"alias\":\"80s\",\"zip\":\"80s.zip\",\"uid\":\"98881ab51f71b2fbdb220752d321b15a\",\"img\":\"80s\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/80s-style-intro\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This intro hero block takes you back to the 80\'s with some funky effects.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content hero slider. Just edit the slide to change the background image and text.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"132\":{\"id\":\"135\",\"title\":\"Blur Effect Slider\",\"alias\":\"blurslider\",\"zip\":\"blurslider.zip\",\"uid\":\"83bd6e1ccef83f03c944fa05b0a2d879\",\"img\":\"blurslider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/blur-effect-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This slider makes use of the brand new blur effects available with version 5.3.1.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">This is a \\\"Default\\\" content slider. Just edit the slides to change the background images and texts.<br\\/><br\\/><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"133\":{\"id\":\"136\",\"title\":\"Coming Soon Add-On\",\"alias\":\"ComingSoonAddon\",\"zip\":\"ComingSoonAddon.zip\",\"uid\":\"51258492055b940099eb96ba52901fa9\",\"img\":\"ComingSoonAddon\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/coming-soon-add-on\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This slider template makes use of the brand new Coming Soon Add-On.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-maintenance-addon\\\\\\/revslider-maintenance-addon.php\\\",\\\"name\\\":\\\"Coming Soon & Maintenance AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/www.themepunch.com\\\\\\/revslider-doc\\\\\\/add-coming-soon-maintenance\\\\\\/\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"134\":{\"id\":\"137\",\"title\":\"Nice And Clean Package\",\"alias\":\"nice-and-clean-package\",\"zip\":\"\",\"uid\":\"nice-and-clean-package\",\"img\":\"packages\\/templatepack_niceandclean.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/nice-clean-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1024\",\"height\":\"800\",\"description\":\"<span class=\\\"ttm_content\\\">The Nice & Clean Website Template uses groups & rows for a fully responsive website experience. Elegant blur effects available since version 5.3.1 make this template stand out!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Nice & Clean Template\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"6\",\"installed\":false},\"135\":{\"id\":\"138\",\"title\":\"Snow Effect Add-On\",\"alias\":\"snowaddon\",\"zip\":\"snowaddon.zip\",\"uid\":\"7408d8567b8af5716eaabd390422e51b\",\"img\":\"snowaddon\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/snow-effect-add-on\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">Add a pleasant Snow Effect to your website with this slider template. Make sure to install the \\\"Holiday Snow\\\" Add-On.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-snow-addon\\\\\\/revslider-snow-addon.php\\\",\\\"name\\\":\\\"Snow AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"136\":{\"id\":\"139\",\"title\":\"Particle Effect One\",\"alias\":\"particle-effect-one\",\"zip\":\"particle-effect-one.zip\",\"uid\":\"a4611c906e35ca60f617da86217b5299\",\"img\":\"particle-effect-one\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/particle-effect-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A unique particle effect that can serve as a stunning intro for your website.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The blend mode effect is achieved through a layer image with CSS blend mode. If you want to customize this effect, select the \\\"BLEND MODE IMAGE\\\" layer and go to \\\"advanced style -> filters -> blend mode\\\".<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"137\":{\"id\":\"140\",\"title\":\"Particle Effect Two\",\"alias\":\"particle-effect-two\",\"zip\":\"particle-effect-two.zip\",\"uid\":\"b8ecbf43374a69e4ab329ea834862ef8\",\"img\":\"particle-effect-two\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/particle-effect-intro\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A unique particle effect that can serve as a stunning intro for your website.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"138\":{\"id\":\"141\",\"title\":\"Particle Effect Three\",\"alias\":\"particle-effect-three\",\"zip\":\"particle-effect-three.zip\",\"uid\":\"b33bc2c5655d8d51cd17c3740f72e748\",\"img\":\"particle-effect-three\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/particle-background-effect\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">A unique particle effect that can serve as a stunning intro for your website.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The blend mode effect is achieved through a layer image with CSS blend mode. If you want to customize this effect, select the \\\"BLEND IMAGE\\\" layer and go to \\\"advanced style -> filters -> blend mode\\\".<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"139\":{\"id\":\"142\",\"title\":\"Portfolio Viewer\",\"alias\":\"portfolioviewer\",\"zip\":\"portfolioviewer.zip\",\"uid\":\"9ac7230ff5b880fb6c8f28fbbc123b3b\",\"img\":\"portfolioviewer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-portfolio-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This slider template can be used to display your portfolio highlights in a stunning way, utilising out free particles add-on and blend modes.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">The navigation for the slides is found in the <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/second-steps\\/#staticlayers\\\" target=\\\"_blank\\\">Static Layers<\\/a>.<\\/span><span class=\\\"ttm_space\\\"><\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"140\":{\"id\":\"143\",\"title\":\"App Showcase\",\"alias\":\"appshowcase\",\"zip\":\"appshowcase.zip\",\"uid\":\"082aef931b0369080bc30c3a2a0c331f\",\"img\":\"appshowcase\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/app-showcase-wordpress-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.0\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This template shows off our new \\\"Polyfold Effects\\\" Add-On in form of an App product showcase module. Get started now and illustrate your Apps key features in a striking way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-polyfold-addon\\\\\\/revslider-polyfold-addon.php\\\",\\\"name\\\":\\\"Polyfold Scroll Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"141\":{\"id\":\"144\",\"title\":\"Gravity Design Hero\",\"alias\":\"gravitydesign\",\"zip\":\"gravitydesign.zip\",\"uid\":\"7bdbe73a6e5bf290cb4412708ac4134d\",\"img\":\"gravitydesign\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/best-wordpress-slider-plugin-2017\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This hero template shows a striking mouse-parallax scene and cool particle effects. Requires our \\\"Particle Effects\\\" Add-on to be installed.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"142\":{\"id\":\"145\",\"title\":\"404 Error Page\",\"alias\":\"404errorpage\",\"zip\":\"404errorpage.zip\",\"uid\":\"2dc62d802b42b73088651cac17d0c486\",\"img\":\"404errorpage\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/404-error-page-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.3.1.5\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This template can be used as a custom error page on your website with our new 404 page Add-On. Of course you can also use this template as a regular hero header.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-404-addon\\\\\\/revslider-404-addon.php\\\",\\\"name\\\":\\\"404 Page Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"},{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"143\":{\"id\":\"146\",\"title\":\"Carousel Gallery\",\"alias\":\"carouselgallery\",\"zip\":\"carouselgallery.zip\",\"uid\":\"041838fd32923c40e15c998f0ea19526\",\"img\":\"carouselgallery\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-carousel-gallery\\/\",\"version\":\"1.0.0\",\"required\":\"5.4\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This carousel gallery features a subtle parallax effect, html5 video and a fullscreen button.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"144\":{\"id\":\"147\",\"title\":\"Filmstrip Effect\",\"alias\":\"filmstrip\",\"zip\":\"filmstrip.zip\",\"uid\":\"7bd142f272cc15d86998a79520e9e581\",\"img\":\"filmstrip\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/filmstrip-wordpress-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.4\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This template uses our Filmstrip Add-On to display a continuous scrolling set of images per slide.\\r\\n There is lots of customization options like animation speed and direction.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-filmstrip-addon\\\\\\/revslider-filmstrip-addon.php\\\",\\\"name\\\":\\\"Background FilmStrip Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"145\":{\"id\":\"148\",\"title\":\"Space Opera\",\"alias\":\"spaceopera\",\"zip\":\"spaceopera.zip\",\"uid\":\"a2c8bffcb138a86c0f373adebb6de046\",\"img\":\"spaceopera\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/space-opera-presentation\\/\",\"version\":\"1.0.0\",\"required\":\"5.4\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Space Opera template can be enjoyed as a slow paced, full-screen slider experience and is great to show your most prominent photos.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"146\":{\"id\":\"149\",\"title\":\"Website Intro\",\"alias\":\"websiteintro\",\"zip\":\"websiteintro.zip\",\"uid\":\"348df76d999456aa19be58c9df56ae20\",\"img\":\"websiteintro\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/website-intro-wordpress-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.4\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">Making use of our new Slicey Add-On, this slider show a really cool effect you just have to experience yourself! Just change the slides background images and the existing slices will automatically be applied.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-slicey-addon\\\\\\/revslider-slicey-addon.php\\\",\\\"name\\\":\\\"Background Slicey Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"147\":{\"id\":\"150\",\"title\":\"Mask Showcase\",\"alias\":\"maskshowcase\",\"zip\":\"maskshowcase.zip\",\"uid\":\"fc943c31e2da9c63b252aeabf554d128\",\"img\":\"maskshowcase\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/minimal-mask-showcase\\/\",\"version\":\"1.0.0\",\"required\":\"5.4\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Mask Showcase template uses gradients and the amazing block layer animations. A minimal slider treat for any website!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"148\":{\"id\":\"151\",\"title\":\"Parallax Zoom Slices\",\"alias\":\"parallaxzoomslices\",\"zip\":\"parallaxzoomslices.zip\",\"uid\":\"83537cae05709ddb9bcb7375470a5894\",\"img\":\"parallaxzoomslices\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/parallax-zoom-slices-wordpress-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.4\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This template uses the Slicey Add-On to add a unique effect to slide background images. You can simple change the background images and the slice effect will automatically be applied.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-slicey-addon\\\\\\/revslider-slicey-addon.php\\\",\\\"name\\\":\\\"Background Slicey Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"149\":{\"id\":\"152\",\"title\":\"Double Exposure Effect\",\"alias\":\"doubleexposureeffect\",\"zip\":\"doubleexposureeffect.zip\",\"uid\":\"8d9229b5cbcf5bda5fbdc6a8e01a2b8c\",\"img\":\"doubleexposureeffect\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-double-exposure-effect\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">Make any website special with the double exposure effect slider. Use as a header module or anywhere you want!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"150\":{\"id\":\"153\",\"title\":\"Mountain Parallax Header\",\"alias\":\"mountainparallaxheader\",\"zip\":\"mountainparallaxheader.zip\",\"uid\":\"8dc64663f317a2abdf179bbe341d016e\",\"img\":\"mountainparallaxheader\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/mountain-wordpress-parallax-header\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This template is a cool intro for any page, featuring a striking parallax effect. Just change texts and links and you\'re done!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"151\":{\"id\":\"154\",\"title\":\"GoodNews One-Pager Package\",\"alias\":\"goodnews-one-pager-package\",\"zip\":\"\",\"uid\":\"goodnews-one-pager-package\",\"img\":\"packages\\/templatepack_goodnews_one_pager.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"carousel\",\"slider\",\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"152\":{\"id\":\"155\",\"title\":\"GoodNews Menu\",\"alias\":\"goodnewsmenu\",\"zip\":\"packs\\/goodnewsmenu.zip\",\"uid\":\"4cbc82501ff340fcdc0acf7eb3ba2640\",\"img\":\"goodnewsmenu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">You can use this slider as a module on your website or install it as part of a slider pack. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"153\":{\"id\":\"156\",\"title\":\"GoodNews Header\",\"alias\":\"goodnewsheader\",\"zip\":\"packs\\/goodnewsheader.zip\",\"uid\":\"c6660b6bdbf596f38466f569596f5259\",\"img\":\"goodnewsheader\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"154\":{\"id\":\"157\",\"title\":\"GoodNews Whats Hot\",\"alias\":\"goodnewswhatshot\",\"zip\":\"packs\\/goodnewswhatshot.zip\",\"uid\":\"cb841ce64a99a6644adab049cf5405cd\",\"img\":\"goodnewswhatshot\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"155\":{\"id\":\"158\",\"title\":\"GoodNews Featured\",\"alias\":\"goodnewsfeatured\",\"zip\":\"packs\\/goodnewsfeatured.zip\",\"uid\":\"00bde4b09e3700da7183999eaf137ccc\",\"img\":\"goodnewsfeatured\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"156\":{\"id\":\"159\",\"title\":\"GoodNews Spotlight\",\"alias\":\"goodnewsspotlight\",\"zip\":\"packs\\/goodnewsspotlight.zip\",\"uid\":\"138076241a828e1c5764379944755f2b\",\"img\":\"goodnewsspotlight\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"157\":{\"id\":\"160\",\"title\":\"GoodNews Carousel\",\"alias\":\"goodnewscarousel\",\"zip\":\"packs\\/goodnewscarousel.zip\",\"uid\":\"d29d4460a6015e30d08d2714232d3768\",\"img\":\"goodnewscarousel\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"carousel\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"158\":{\"id\":\"161\",\"title\":\"GoodNews Callout\",\"alias\":\"goodnewscallout\",\"zip\":\"packs\\/goodnewscallout.zip\",\"uid\":\"d9568b3193e96577fae630b3a6728785\",\"img\":\"goodnewscallout\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"159\":{\"id\":\"162\",\"title\":\"GoodNews Footer\",\"alias\":\"goodnewsfooter\",\"zip\":\"packs\\/goodnewsfooter.zip\",\"uid\":\"34f43f891cb8d55375149dc4bbc38298\",\"img\":\"goodnewsfooter\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews One-Pager\",\"package_order\":\"8\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"7\",\"installed\":false},\"160\":{\"id\":\"163\",\"title\":\"GoodNews Content Page Package\",\"alias\":\"goodnews-content-page-package\",\"zip\":\"\",\"uid\":\"goodnews-content-page-package\",\"img\":\"packages\\/templatepack_goodnews_content_page_package.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme-content\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\",\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews Content Page\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"8\",\"installed\":false},\"161\":{\"id\":\"164\",\"title\":\"GoodNews Menu Back\",\"alias\":\"goodnewsmenuback\",\"zip\":\"packs\\/goodnewsmenuback.zip\",\"uid\":\"1340d1aeefba497a7d404d12a1fceed4\",\"img\":\"goodnewsmenuback\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme-content\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews Content Page\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"8\",\"installed\":false},\"162\":{\"id\":\"165\",\"title\":\"GoodNews Blog Header\",\"alias\":\"goodnewsblogheader\",\"zip\":\"packs\\/goodnewsblogheader.zip\",\"uid\":\"abc4d7c1e48475c6def05f1f6d8bf564\",\"img\":\"goodnewsblogheader\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme-content\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews Content Page\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"8\",\"installed\":false},\"163\":{\"id\":\"166\",\"title\":\"GoodNews Blog Content\",\"alias\":\"goodnewsblogcontent\",\"zip\":\"packs\\/goodnewsblogcontent.zip\",\"uid\":\"bbf34563da6db2779c29599b503b07e9\",\"img\":\"goodnewsblogcontent\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme-content\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews Content Page\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"8\",\"installed\":false},\"164\":{\"id\":\"167\",\"title\":\"GoodNews Testimonials\",\"alias\":\"goodnewstestimonials\",\"zip\":\"packs\\/goodnewstestimonials.zip\",\"uid\":\"606b7336e86f69c567542d3f43712b56\",\"img\":\"goodnewstestimonials\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme-content\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews Content Page\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"8\",\"installed\":false},\"165\":{\"id\":\"168\",\"title\":\"GoodNews Blog Footer\",\"alias\":\"goodnewsblogfooter\",\"zip\":\"packs\\/goodnewsblogfooter.zip\",\"uid\":\"1fb88aecfb116fde67ce8d52bd3b5f05\",\"img\":\"goodnewsblogfooter\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/goodnews-responsive-wordpress-theme-content\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The GoodNews One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"GoodNews Content Page\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"8\",\"installed\":false},\"166\":{\"id\":\"169\",\"title\":\"Before After Slider\",\"alias\":\"beforeafterslider\",\"zip\":\"beforeafterslider.zip\",\"uid\":\"6e615091a1fc3037c24b985ce5136fb2\",\"img\":\"beforeafterslider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/before-after-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.3.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This is the example slider for our Before \\/ After Add-On that allows you to create unique presentations with a comparing functionality.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">How to install <a href=\\\"https:\\/\\/revolution.themepunch.com\\/direct-customer-benefits\\/#addoninstall\\\" target=\\\"_blank\\\">Add-Ons<\\/a>.<\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-beforeafter-addon\\\\\\/revslider-beforeafter-addon.php\\\",\\\"name\\\":\\\"Before After AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"167\":{\"id\":\"170\",\"title\":\"Product Showcase\",\"alias\":\"productshowcase\",\"zip\":\"productshowcase.zip\",\"uid\":\"a43447670260aaa7e8ff66cedfddb57a\",\"img\":\"productshowcase\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-product-showcase-slider\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">Looking to sell products effectively? Our product showcase slider is just what you are looking for!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"168\":{\"id\":\"171\",\"title\":\"Overexposure Transition\",\"alias\":\"overexposuretransition\",\"zip\":\"overexposuretransition.zip\",\"uid\":\"13f16bbe6c6d646c7d0cb817a0d3d181\",\"img\":\"overexposuretransition\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/overexposure-transition-effect\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">Subtle overexposure transitions and smooth color fade effects make this slider stand out.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">How to install <a href=\\\"https:\\/\\/revolution.themepunch.com\\/direct-customer-benefits\\/#addoninstall\\\" target=\\\"_blank\\\">Add-Ons<\\/a>.<\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"169\":{\"id\":\"172\",\"title\":\"Parallax Scroll\",\"alias\":\"parallaxscroll\",\"zip\":\"parallaxscroll.zip\",\"uid\":\"82546ee2f6af6c6682852f495109b3c3\",\"img\":\"parallaxscroll\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/parallax-scroll\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">A strikingly colourful header for your website with super smooth parallax effects.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"170\":{\"id\":\"173\",\"title\":\"TechCo One-Pager Package\",\"alias\":\"techco-one-pager-package\",\"zip\":\"\",\"uid\":\"techco-one-pager-package\",\"img\":\"packages\\/techco_overview.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\",\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"171\":{\"id\":\"174\",\"title\":\"TechCo Menu\",\"alias\":\"techco-menu\",\"zip\":\"packs\\/techco-menu.zip\",\"uid\":\"55e5efee828cdf1ff7e2d3b90a301ea9\",\"img\":\"techco-menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"172\":{\"id\":\"175\",\"title\":\"TechCo Header\",\"alias\":\"techco-header\",\"zip\":\"packs\\/techco-header.zip\",\"uid\":\"fb574d1376de9b1e408c91f51e6497d7\",\"img\":\"techco-header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"173\":{\"id\":\"176\",\"title\":\"TechCo About\",\"alias\":\"techco-about\",\"zip\":\"packs\\/techco-about.zip\",\"uid\":\"ba216da8231e55118d87e37d2358812c\",\"img\":\"techco-about\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"174\":{\"id\":\"177\",\"title\":\"TechCo Services\",\"alias\":\"techco-services\",\"zip\":\"packs\\/techco-services.zip\",\"uid\":\"ef4a8ddbb5e1136133f7bc1227248e22\",\"img\":\"techco-services\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"175\":{\"id\":\"178\",\"title\":\"TechCo Video\",\"alias\":\"techco-video\",\"zip\":\"packs\\/techco-video.zip\",\"uid\":\"d7bb92281d05f39f9bc9eca71f90e402\",\"img\":\"techco-video\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"176\":{\"id\":\"179\",\"title\":\"TechCo Prices\",\"alias\":\"techco-prices\",\"zip\":\"packs\\/techco-prices.zip\",\"uid\":\"6291f404efbea12bb181352aba71ef11\",\"img\":\"techco-prices\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"177\":{\"id\":\"180\",\"title\":\"TechCo Testimonials\",\"alias\":\"techco-testimonials\",\"zip\":\"packs\\/techco-testimonials.zip\",\"uid\":\"3460bd51f6b80599266fecb7fbb918be\",\"img\":\"techco-testimonials\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"178\":{\"id\":\"181\",\"title\":\"TechCo Footer\",\"alias\":\"techco-footer\",\"zip\":\"packs\\/techco-footer.zip\",\"uid\":\"640abcd549137520461a4a71ff758a3b\",\"img\":\"techco-footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/techco-one-page-wordpress-theme\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The TechCo One-Pager template pack is a fantastic solution if you want to get a simple website going fast. Use all modules at once as seen in the preview or pick individual modules for usage anywhere!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"TechCo One-Pager\",\"package_order\":\"8\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"9\",\"installed\":false},\"179\":{\"id\":\"182\",\"title\":\"Live Weather\",\"alias\":\"weather\",\"zip\":\"weather.zip\",\"uid\":\"aab92e69374e4c7b8c6741fe02e574b9\",\"img\":\"weather\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-live-weather-add-on\\/\",\"version\":\"1.0.1\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">Showcasing our weather add-on, this slider show a different cities temperatures and forecast on each slide.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-weather-addon\\\\\\/revslider-weather-addon.php\\\",\\\"name\\\":\\\"Live Weather Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"180\":{\"id\":\"183\",\"title\":\"360 Panorama Tour\",\"alias\":\"360panorama\",\"zip\":\"360panorama.zip\",\"uid\":\"332720fdacdbb38f65e8327a2a96c52d\",\"img\":\"360panorama\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/360-degree-real-estate-virtual-tour\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.5.2\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">An example template for our Panorama 360 tour add-on, created for the real estate market.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-panorama-addon\\\\\\/revslider-panorama-addon.php\\\",\\\"name\\\":\\\"Panorama AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"181\":{\"id\":\"184\",\"title\":\"Duotone Add-on\",\"alias\":\"duotone-add-on\",\"zip\":\"duotone-add-on.zip\",\"uid\":\"a428c6f363b3146e96d20a6f44958922\",\"img\":\"duotone-add-on\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-duotone-effect-add-on\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">This example template showcases 3 of the 30 Duotone filters available in our brand new Duotone Add-On.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"},{\\\"path\\\":\\\"revslider-duotonefilters-addon\\\\\\/revslider-duotonefilters-addon.php\\\",\\\"name\\\":\\\"Duotone AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"182\":{\"id\":\"185\",\"title\":\"Reveal Add-on\",\"alias\":\"reveal-add-on\",\"zip\":\"reveal-add-on.zip\",\"uid\":\"7fa7525d8ff7fa7365cb98a437e88e32\",\"img\":\"reveal-add-on\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wordpress-content-reveal-effect\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">An example template that uses our Reveal Add-On, which adds 14 new preloaders combined with reveal loading effects for any slider or hero.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"},{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"201\":{\"id\":\"210\",\"title\":\"Cryptocurrency Prices\",\"alias\":\"cryptoslider\",\"zip\":\"cryptoslider.zip\",\"uid\":\"c4b02210387f11946223977e940d9e9e\",\"img\":\"cryptoslider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/cryptocurrency-wordpress-price-api-bitcoin-ethereum-litecoin\\/\",\"version\":\"1.0.2\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"900\",\"description\":\"<span class=\\\"ttm_content\\\">This Template can show live, animated cryptocurrency prices. More than 1300 currencies are supported, getting prices via the cryptocompare API.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Please refer to the \\\"Custom JavaScript\\\" section of this sliders settings, for info on how to modify currencies. Provide a class to the row \\/ column \\/ group where the currency text elements are located in.<\\/span>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"202\":{\"id\":\"211\",\"title\":\"Immersion One Page\",\"alias\":\"immersion-one-page-package\",\"zip\":\"\",\"uid\":\"immersion-one-page-package\",\"img\":\"packages\\/immersion_overview.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\",\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-typewriter-addon\\\\\\/revslider-typewriter-addon.php\\\",\\\"name\\\":\\\"Typewriter AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/www.themepunch.com\\\\\\/revslider-doc\\\\\\/add-on-typewriter\\\\\\/\\\"},{\\\"path\\\":\\\"revslider-slicey-addon\\\\\\/revslider-slicey-addon.php\\\",\\\"name\\\":\\\"Background Slicey Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"203\":{\"id\":\"212\",\"title\":\"Immersion Header\",\"alias\":\"immersion_header\",\"zip\":\"packs\\/immersion_header.zip\",\"uid\":\"853da51256308b341ecd030bd4883229\",\"img\":\"immersion_header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-typewriter-addon\\\\\\/revslider-typewriter-addon.php\\\",\\\"name\\\":\\\"Typewriter AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/www.themepunch.com\\\\\\/revslider-doc\\\\\\/add-on-typewriter\\\\\\/\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"204\":{\"id\":\"213\",\"title\":\"Immersion Mountains\",\"alias\":\"immersion-mountains\",\"zip\":\"packs\\/immersion-mountains.zip\",\"uid\":\"817167eb3fe22b7e065ba210cbe6d53c\",\"img\":\"immersion-mountains\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-slicey-addon\\\\\\/revslider-slicey-addon.php\\\",\\\"name\\\":\\\"Background Slicey Add-On\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"205\":{\"id\":\"214\",\"title\":\"Immersion Product\",\"alias\":\"immersion-product\",\"zip\":\"packs\\/immersion-product.zip\",\"uid\":\"64134f263484d2bbcd7ef088ffbbfb4b\",\"img\":\"immersion-product\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"206\":{\"id\":\"215\",\"title\":\"Immersion Design\",\"alias\":\"immersion-design\",\"zip\":\"packs\\/immersion-design.zip\",\"uid\":\"91e1d77c1a2826438763804f4d02bc26\",\"img\":\"immersion-design\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"207\":{\"id\":\"216\",\"title\":\"Immersion Phototgraphy\",\"alias\":\"immersion-photography\",\"zip\":\"packs\\/immersion-photography.zip\",\"uid\":\"e3ddf0c577b09740f5cbf2e38ffd684d\",\"img\":\"immersion-photography\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"208\":{\"id\":\"217\",\"title\":\"Immersion Grid\",\"alias\":\"immersion-grid\",\"zip\":\"packs\\/immersion-grid.zip\",\"uid\":\"b6903868189bb83b2c7a852fde3a7dc3\",\"img\":\"immersion-grid\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/immersion-wordpress-one-page-theme-2018\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Immersion WordPress one page theme is a great fit if you quickly want to setup a visually oriented one page website. Using the power of Slider Revolution, we added some interesting effects like slicing and smooth parallax.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Immersion One Page\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"10\",\"installed\":false},\"209\":{\"id\":\"218\",\"title\":\"Funky Intro Slider\",\"alias\":\"funkyslider\",\"zip\":\"funkyslider.zip\",\"uid\":\"2d4187e3fdad19b976be335253c8925d\",\"img\":\"funkyslider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/funky-intro-slider-wordpress-plugin\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This funky intro slider fits just right if you need a striking introduction to your website! Sleek, beautiful and easily customizable!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-typewriter-addon\\\\\\/revslider-typewriter-addon.php\\\",\\\"name\\\":\\\"Typewriter AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/www.themepunch.com\\\\\\/revslider-doc\\\\\\/add-on-typewriter\\\\\\/\\\"},{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"210\":{\"id\":\"219\",\"title\":\"Clear View Magazine\",\"alias\":\"clear-view-magazine-package\",\"zip\":\"\",\"uid\":\"clear-view-magazine-package\",\"img\":\"packages\\/clear_view_magazine_overview.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"211\":{\"id\":\"220\",\"title\":\"Clear View Menu\",\"alias\":\"clearview_menu\",\"zip\":\"packs\\/clearview_menu.zip\",\"uid\":\"eaecee5fa5f3c3a7f4d2a96c4616a353\",\"img\":\"clearview_menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"212\":{\"id\":\"221\",\"title\":\"Clear View Header\",\"alias\":\"clearview_header\",\"zip\":\"packs\\/clearview_header.zip\",\"uid\":\"25e3dd80ad130875d7438a07952cb0cd\",\"img\":\"clearview_header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"213\":{\"id\":\"222\",\"title\":\"Clear View Mission\",\"alias\":\"clearview_mission\",\"zip\":\"packs\\/clearview_mission.zip\",\"uid\":\"35b2092a49fd2beb549342e69097eb5b\",\"img\":\"clearview_mission\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"214\":{\"id\":\"223\",\"title\":\"Clear View Slider\",\"alias\":\"clear-view-slider\",\"zip\":\"packs\\/clear-view-slider.zip\",\"uid\":\"d2e17edffce16ed78c54b0ef23fd7e05\",\"img\":\"clear-view-slider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"215\":{\"id\":\"224\",\"title\":\"Clear View News\",\"alias\":\"clear-view-news\",\"zip\":\"packs\\/clear-view-news.zip\",\"uid\":\"5698d3131ba141e9afcfd2906739dd00\",\"img\":\"clear-view-news\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"216\":{\"id\":\"225\",\"title\":\"Clear View Clients\",\"alias\":\"clear-view-clients\",\"zip\":\"packs\\/clear-view-clients.zip\",\"uid\":\"b95616a94832e22bdfac5ce60232be1b\",\"img\":\"clear-view-clients\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"217\":{\"id\":\"226\",\"title\":\"Clear View Contact\",\"alias\":\"clear-view-contact\",\"zip\":\"packs\\/clear-view-contact.zip\",\"uid\":\"0e0cc1d8f6f6500e5f8a2b091fa3b4cb\",\"img\":\"clear-view-contact\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-minimal-magazine-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Magazine\",\"package_order\":\"7\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"11\",\"installed\":false},\"218\":{\"id\":\"227\",\"title\":\"Clear View Post Page\",\"alias\":\"clear-view-post-page-package\",\"zip\":\"\",\"uid\":\"clear-view-post-page-package\",\"img\":\"packages\\/clear_view_post_page_overview.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-single-post-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Post Page\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"12\",\"installed\":false},\"219\":{\"id\":\"228\",\"title\":\"Clear View Single Menu\",\"alias\":\"clear-view-single-menu\",\"zip\":\"packs\\/clear-view-single-menu.zip\",\"uid\":\"1e80f81982f8a4ea763482d4fa99d321\",\"img\":\"clear-view-single-menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-single-post-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Post Page\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"12\",\"installed\":false},\"220\":{\"id\":\"229\",\"title\":\"Clear View Single Header\",\"alias\":\"clear-view-single-header\",\"zip\":\"packs\\/clear-view-single-header.zip\",\"uid\":\"c8d717627be6cd5e70922ab609694dbf\",\"img\":\"clear-view-single-header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-single-post-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Post Page\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"12\",\"installed\":false},\"221\":{\"id\":\"230\",\"title\":\"Clear View Single Media\",\"alias\":\"clear-view-single-media\",\"zip\":\"packs\\/clear-view-single-media.zip\",\"uid\":\"c480368ded2a64f0cdd44f1674213814\",\"img\":\"clear-view-single-media\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-single-post-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Post Page\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"12\",\"installed\":false},\"222\":{\"id\":\"231\",\"title\":\"Clear View Single More\",\"alias\":\"clear-view-single-more\",\"zip\":\"packs\\/clear-view-single-more.zip\",\"uid\":\"9c693190df26218366d1f77e10cf550a\",\"img\":\"clear-view-single-more\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-single-post-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Post Page\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"12\",\"installed\":false},\"223\":{\"id\":\"232\",\"title\":\"Clear View Single Contact\",\"alias\":\"clear-view-single-contact\",\"zip\":\"packs\\/clear-view-single-contact.zip\",\"uid\":\"73c0a889f2b654a87b5aba1ff76fbc5c\",\"img\":\"clear-view-single-contact\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-view-single-post-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.6\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear View Minimal Magazine WordPress Template is a full Slider Revolution module arrangement with fully customizable content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Clear View Post Page\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"12\",\"installed\":false},\"224\":{\"id\":\"233\",\"title\":\"Clean Landing Page\",\"alias\":\"cleanlandingpage\",\"zip\":\"cleanlandingpage.zip\",\"uid\":\"727a3680d312f22c6a5998ebdb9afe52\",\"img\":\"cleanlandingpage\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clean-landing-page-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clean Landing Page is a fullscreen Intro Module that is a striking introduction to any minimal website, with no unnecessary elements.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-typewriter-addon\\\\\\/revslider-typewriter-addon.php\\\",\\\"name\\\":\\\"Typewriter AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/www.themepunch.com\\\\\\/revslider-doc\\\\\\/add-on-typewriter\\\\\\/\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"225\":{\"id\":\"234\",\"title\":\"Clear Cut\",\"alias\":\"clearcut\",\"zip\":\"clearcut.zip\",\"uid\":\"07f43c00e9b4d3057a03cdb3385ad2b7\",\"img\":\"clearcut\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/clear-cut-portfolio-website-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Clear Cut Template is an All-In-One Portfolio Website solution that works best for smaller portfolios. Highlight your best work in a striking and mobile friendly fashion!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"226\":{\"id\":\"235\",\"title\":\"Wonderstruck One-Pager\",\"alias\":\"wonderstruck-one-pager-package\",\"zip\":\"\",\"uid\":\"wonderstruck-one-pager-package\",\"img\":\"packages\\/wonderstruck-one-pager-overview.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wonderstruck-one-page-website-for-wordpress\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Wonderstruck One-Pager Template is a clean, colorful way to showcase your portfolio online. Just install, change the content and you have a mobile ready online experience!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Wonderstruck One-Pager\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"13\",\"installed\":false},\"227\":{\"id\":\"236\",\"title\":\"Wonderstruck Menu\",\"alias\":\"wonderstruck_menu\",\"zip\":\"packs\\/wonderstruck_menu.zip\",\"uid\":\"0a976e9aaae59c4f795b38f59f5a08d8\",\"img\":\"wonderstruck_menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wonderstruck-one-page-website-for-wordpress\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Wonderstruck One-Pager Template is a clean, colorful way to showcase your portfolio online. Just install, change the content and you have a mobile ready online experience!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Wonderstruck One-Pager\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"13\",\"installed\":false},\"228\":{\"id\":\"237\",\"title\":\"Wonderstruck Header\",\"alias\":\"wonderstruck_header\",\"zip\":\"packs\\/wonderstruck_header.zip\",\"uid\":\"e1379f77a902960a0ce12d44d85a9e0a\",\"img\":\"wonderstruck_header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wonderstruck-one-page-website-for-wordpress\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Wonderstruck One-Pager Template is a clean, colorful way to showcase your portfolio online. Just install, change the content and you have a mobile ready online experience!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Wonderstruck One-Pager\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"13\",\"installed\":false},\"229\":{\"id\":\"238\",\"title\":\"Wonderstruck About\",\"alias\":\"wonderstruck_about\",\"zip\":\"packs\\/wonderstruck_about.zip\",\"uid\":\"d207fb140fd328acc3038300ea52082a\",\"img\":\"wonderstruck_about\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wonderstruck-one-page-website-for-wordpress\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Wonderstruck One-Pager Template is a clean, colorful way to showcase your portfolio online. Just install, change the content and you have a mobile ready online experience!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Wonderstruck One-Pager\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"13\",\"installed\":false},\"230\":{\"id\":\"239\",\"title\":\"Wonderstruck Works\",\"alias\":\"wonderstruck-works\",\"zip\":\"packs\\/wonderstruck-works.zip\",\"uid\":\"4476935097e27d92454b0011b7700c1d\",\"img\":\"wonderstruck-works\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wonderstruck-one-page-website-for-wordpress\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Wonderstruck One-Pager Template is a clean, colorful way to showcase your portfolio online. Just install, change the content and you have a mobile ready online experience!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Wonderstruck One-Pager\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"13\",\"installed\":false},\"231\":{\"id\":\"240\",\"title\":\"Wonderstruck Contact\",\"alias\":\"wonderstruck-contact\",\"zip\":\"packs\\/wonderstruck-contact.zip\",\"uid\":\"9e4911521f77bce2b8efa40f4c2adc22\",\"img\":\"wonderstruck-contact\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/wonderstruck-one-page-website-for-wordpress\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Wonderstruck One-Pager Template is a clean, colorful way to showcase your portfolio online. Just install, change the content and you have a mobile ready online experience!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Wonderstruck One-Pager\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"13\",\"installed\":false},\"232\":{\"id\":\"241\",\"title\":\"Bubble Morph\",\"alias\":\"bubblemorph\",\"zip\":\"bubblemorph.zip\",\"uid\":\"1102d6f5460ab82cb612cbe9f1d9514b\",\"img\":\"bubblemorph\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/bubble-morph-effect-add-on-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Bubble Morph Add-On allows you to create interesting Lava-Lamp style effects on any of your Slider Revolution content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-bubblemorph-addon\\\\\\/revslider-bubblemorph-addon.php\\\",\\\"name\\\":\\\"BubbleMorph AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"233\":{\"id\":\"242\",\"title\":\"Distortion AddOn\",\"alias\":\"distortionaddon\",\"zip\":\"distortionaddon.zip\",\"uid\":\"0ad46c9929bb2fa6316f1e6ced301aaf\",\"img\":\"distortionaddon\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/distortion-effect-add-on-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.7\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Distortion Add-On allows you to create interesting Distortion style effects on any of your Slider Revolution content.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-liquideffect-addon\\\\\\/revslider-liquideffect-addon.php\\\",\\\"name\\\":\\\"Distortion Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"234\":{\"id\":\"243\",\"title\":\"Club Flyer\",\"alias\":\"clubflyer\",\"zip\":\"clubflyer.zip\",\"uid\":\"dab73b9904d0e643a35b0475980998bd\",\"img\":\"clubflyer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/club-flyer-landing-page-template-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">This Club Fyler Template for Slider Revolution uses the brand new Exploding Layers Add-On to create a captivating effect. Tap into unlimited creative potential. <\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-explodinglayers-addon\\\\\\/revslider-explodinglayers-addon.php\\\",\\\"name\\\":\\\"Exploding Layers AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"235\":{\"id\":\"244\",\"title\":\"Paintbrush AddOn\",\"alias\":\"paintbrushaddon\",\"zip\":\"paintbrushaddon.zip\",\"uid\":\"c85c48a6742b4bf72a2590cc1b50f6a0\",\"img\":\"paintbrushaddon\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/paintbrush-effect-add-on-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Paintbrush Add-On for WordPress allows you to add cool mouse interaction effects to your Slider Revolution content. Examples: Blur Reveal, Color Reveal, Scratch Me Free.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-paintbrush-addon\\\\\\/revslider-paintbrush-addon.php\\\",\\\"name\\\":\\\"Paintbrush AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"236\":{\"id\":\"245\",\"title\":\"Parallax One-Pager\",\"alias\":\"parallax-one-pager-package\",\"zip\":\"\",\"uid\":\"parallax-one-pager-package\",\"img\":\"packages\\/parallax-one-pager-package.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/parallax-one-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Parallax One-Page WordPress Template is consisting of 3 hero blocks and contains a beautiful parallax mountain scene. An impactful opening to your web presence!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Parallax One-Pager\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"14\",\"installed\":false},\"237\":{\"id\":\"246\",\"title\":\"Parallax Header\",\"alias\":\"parallax_header\",\"zip\":\"packs\\/parallax_header.zip\",\"uid\":\"32bf4a0f5136853bd6bd366275a7a60b\",\"img\":\"parallax_header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/parallax-one-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Parallax One-Page WordPress Template is consisting of 3 hero blocks and contains a beautiful parallax mountain scene. An impactful opening to your web presence!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Parallax One-Pager\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"14\",\"installed\":false},\"238\":{\"id\":\"247\",\"title\":\"Parallax Content\",\"alias\":\"parallax_content\",\"zip\":\"packs\\/parallax_content.zip\",\"uid\":\"a25c5fb8f74757476ab4dc038e56f90d\",\"img\":\"parallax_content\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/parallax-one-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Parallax One-Page WordPress Template is consisting of 3 hero blocks and contains a beautiful parallax mountain scene. An impactful opening to your web presence!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Parallax One-Pager\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"14\",\"installed\":false},\"239\":{\"id\":\"248\",\"title\":\"Parallax Footer\",\"alias\":\"parallax_footer\",\"zip\":\"packs\\/parallax_footer.zip\",\"uid\":\"665dff9ea6eaf162a2a1f160f51a9ddb\",\"img\":\"parallax_footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/parallax-one-page-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Parallax One-Page WordPress Template is consisting of 3 hero blocks and contains a beautiful parallax mountain scene. An impactful opening to your web presence!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Parallax One-Pager\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"14\",\"installed\":false},\"240\":{\"id\":\"249\",\"title\":\"Le Chef Restaurant\",\"alias\":\"le-chef-restaurant\",\"zip\":\"\",\"uid\":\"le-chef-restaurant-package\",\"img\":\"packages\\/le-chef-restaurant-package.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1024\",\"height\":\"768\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[null],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-particles-addon\\\\\\/revslider-particles-addon.php\\\",\\\"name\\\":\\\"Particles Effect AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"0\",\"package_parent\":\"true\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"241\":{\"id\":\"250\",\"title\":\"Le Chef Menu\",\"alias\":\"le-chef-menu\",\"zip\":\"packs\\/le-chef-menu.zip\",\"uid\":\"2f2a6a333431fefe4a7b3b6a982b2ff5\",\"img\":\"le-chef-menu\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"1\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"242\":{\"id\":\"251\",\"title\":\"Le Chef Header\",\"alias\":\"le-chef-header\",\"zip\":\"packs\\/le-chef-header.zip\",\"uid\":\"06450ca3be5a4a8959f3752ab974e574\",\"img\":\"le-chef-header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"2\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"243\":{\"id\":\"252\",\"title\":\"Le Chef Philosophy\",\"alias\":\"le-chef-philosophy\",\"zip\":\"packs\\/le-chef-philosophy.zip\",\"uid\":\"a532fd029addeb18106e751409b8e20a\",\"img\":\"le-chef-philosophy\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"3\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"244\":{\"id\":\"253\",\"title\":\"Le Chef Food\",\"alias\":\"le-chef-food\",\"zip\":\"packs\\/le-chef-food.zip\",\"uid\":\"cd4b6a2cb5de2dd20a0e8ba0c35451df\",\"img\":\"le-chef-food\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"4\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"245\":{\"id\":\"254\",\"title\":\"Le Chef La Carte\",\"alias\":\"le-chef-la-carte\",\"zip\":\"packs\\/le-chef-la-carte.zip\",\"uid\":\"6f312749679c3ba52c41189a6b8bf729\",\"img\":\"le-chef-la-carte\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"5\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"246\":{\"id\":\"255\",\"title\":\"Le Chef Footer\",\"alias\":\"le-chef-footer\",\"zip\":\"packs\\/le-chef-footer.zip\",\"uid\":\"12fc241e8e5b9e5df9758c43448e2907\",\"img\":\"le-chef-footer\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/le-chef-restaurant-website-wordpress-template\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Le Chef Restaurant Template is a One-Page Website and a full solution for your clients Restaurant. Showcase images in the header slideshow and present the Restaurants offering in a clear way!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">If you want to setup the full website example as seen in the preview, please follow the <a href=\\\"https:\\/\\/www.themepunch.com\\/faq\\/setup-content-blocks-template\\/\\\" target=\\\"_blank\\\">Content Blocks instructions here<\\/a>.<br><br>\\r\\n<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"hero\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package\":\"Le Chef Restaurant\",\"package_order\":\"6\",\"package_parent\":\"\",\"active\":\"1\",\"package_id\":\"15\",\"installed\":false},\"247\":{\"id\":\"256\",\"title\":\"News Magazine Slider\",\"alias\":\"news-magazine-slider\",\"zip\":\"news-magazine-slider.zip\",\"uid\":\"31f2c1506babb1fef459401f051d2d52\",\"img\":\"news-magazine-slider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/news-magazine-slider-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Magazine Slider offers various layouts that support static content images, HTML5 and Youtube video. Great for any blog or news website!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-revealer-addon\\\\\\/revslider-revealer-addon.php\\\",\\\"name\\\":\\\"Reveal AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"248\":{\"id\":\"257\",\"title\":\"Real Estate Slider\",\"alias\":\"real-estate-slider\",\"zip\":\"real-estate-slider.zip\",\"uid\":\"739ce6336e46815094387c9448e6c804\",\"img\":\"real-estate-slider\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/real-estate-slider-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Real Estate Slider allows to display real estate offers with a large ken burns background image and additional thumbnail hover images.<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":null,\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false},\"249\":{\"id\":\"258\",\"title\":\"Fashion Header\",\"alias\":\"fashion-header\",\"zip\":\"fashion-header.zip\",\"uid\":\"24ece4e7f4d31cd90377c62abbd9e25a\",\"img\":\"fashion-header\\/slide1.jpg\",\"preview\":\"https:\\/\\/revolution.themepunch.com\\/fashion-shop-header-slider-for-wordpress\\/\",\"version\":\"1.0.0\",\"required\":\"5.4.8\",\"width\":\"1240\",\"height\":\"868\",\"description\":\"<span class=\\\"ttm_content\\\">The Fashion Shop Header ist the ideal Slider to display as an intro to your shop website or as a separator in between content. Our unique \\\"Bubble Morph\\\" effect makes this module stand out!<\\/span>\\r\\n<span class=\\\"ttm_space\\\"><\\/span>\",\"setup_notes\":\"<span class=\\\"ttm_content\\\">Checkout our <a href=\\\"https:\\/\\/www.themepunch.com\\/revslider-doc\\/slider-revolution-documentation\\/\\\" target=\\\"_blank\\\">Documentation<\\/a> for basic Slider Revolution help.<\\/span>\",\"cat\":\"Premium\",\"filter\":[\"slider\"],\"plugin_require\":\"[{\\\"path\\\":\\\"revslider-bubblemorph-addon\\\\\\/revslider-bubblemorph-addon.php\\\",\\\"name\\\":\\\"BubbleMorph AddOn\\\",\\\"url\\\":\\\"https:\\\\\\/\\\\\\/revolution.themepunch.com\\\\\\/direct-customer-benefits\\\\\\/#addoninstall\\\"}]\",\"date\":\"0000-00-00 00:00:00\",\"package_order\":\"0\",\"active\":\"1\",\"installed\":false}},\"slides\":{\"classic-carousel\":[{\"title\":\"Slide 1\",\"img\":\"classic-carousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"classic-carousel\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"classic-carousel\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"classic-carousel\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"classic-carousel\\/slide5.jpg\"}],\"classicslider\":[{\"title\":\"Slide 1\",\"img\":\"classicslider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"classicslider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"classicslider\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"classicslider\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"classicslider\\/slide5.jpg\"}],\"contenttabs\":[{\"title\":\"Slide 1\",\"img\":\"contenttabs\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"contenttabs\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"contenttabs\\/slide3.jpg\"}],\"facebook-feed\":[{\"title\":\"Slide 1\",\"img\":\"facebook-feed\\/slide1.jpg\"}],\"fashion\":[{\"title\":\"Slide 1\",\"img\":\"fashion\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fashion\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"fashion\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"fashion\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"fashion\\/slide5.jpg\"}],\"flickr-gallery\":[{\"title\":\"Slide 1\",\"img\":\"flickr-gallery\\/slide1.jpg\"}],\"gym\":[{\"title\":\"Slide 1\",\"img\":\"gym\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"gym\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"gym\\/slide3.jpg\"}],\"highlight-carousel\":[{\"title\":\"Slide 1\",\"img\":\"highlight-carousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"highlight-carousel\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"highlight-carousel\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"highlight-carousel\\/slide4.jpg\"}],\"highlight-showcase\":[{\"title\":\"Slide 1\",\"img\":\"highlight-showcase\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"highlight-showcase\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"highlight-showcase\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"highlight-showcase\\/slide4.jpg\"}],\"image-hero\":[{\"title\":\"Slide 1\",\"img\":\"imagehero\\/slide1.jpg\"}],\"insta-gallery\":[{\"title\":\"Slide 1\",\"img\":\"insta-gallery\\/slide1.jpg\"}],\"levanorestaurantbar\":[{\"title\":\"Slide 1\",\"img\":\"levanorestaurantbar\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"levanorestaurantbar\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"levanorestaurantbar\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"levanorestaurantbar\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"levanorestaurantbar\\/slide5.jpg\"}],\"mainfeature\":[{\"title\":\"Slide 1\",\"img\":\"mainfeature\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"mainfeature\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"mainfeature\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"mainfeature\\/slide4.jpg\"},{\"title\":\"Slide 7\",\"img\":\"mainfeature\\/slide7.jpg\"}],\"media-gallery-two\":[{\"title\":\"Slide 1\",\"img\":\"media-gallery-two\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"media-gallery-two\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"media-gallery-two\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"media-gallery-two\\/slide4.jpg\"},{\"title\":\"Slide 6\",\"img\":\"media-gallery-two\\/slide6.jpg\"}],\"media-carousel-autoplay\":[{\"title\":\"Slide 1\",\"img\":\"media-carousel-autoplay\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"media-carousel-autoplay\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"media-carousel-autoplay\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"media-carousel-autoplay\\/slide4.jpg\"},{\"title\":\"Slide 6\",\"img\":\"media-carousel-autoplay\\/slide6.jpg\"}],\"news-background-video\":[{\"title\":\"Slide 1\",\"img\":\"news-bg-video\\/slide1.jpg\"}],\"news-gallery\":[{\"title\":\"Slide 1\",\"img\":\"news-gallery\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"news-gallery\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"news-gallery\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"news-gallery\\/slide4.jpg\"}],\"news-gallery-post-based\":[{\"title\":\"Slide 1\",\"img\":\"news-gallery-post-based\\/slide1.jpg\"}],\"news-hero\":[{\"title\":\"Slide 1\",\"img\":\"news-hero\\/slide1.jpg\"}],\"news-video\":[{\"title\":\"Slide 1\",\"img\":\"news-video\\/slide1.jpg\"}],\"newsletter-hero\":[{\"title\":\"Slide 1\",\"img\":\"newsletter-hero\\/slide1.jpg\"}],\"notgeneric\":[{\"title\":\"Slide 1\",\"img\":\"notgeneric\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"notgeneric\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"notgeneric\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"notgeneric\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"notgeneric\\/slide5.jpg\"}],\"photography\":[{\"title\":\"Slide 1\",\"img\":\"photography\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"photography\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"photography\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"photography\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"photography\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"photography\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"photography\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"photography\\/slide8.jpg\"},{\"title\":\"Slide 9\",\"img\":\"photography\\/slide9.jpg\"},{\"title\":\"Slide 10\",\"img\":\"photography\\/slide10.jpg\"}],\"photography-carousel\":[{\"title\":\"Slide 1\",\"img\":\"photography-carousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"photography-carousel\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"photography-carousel\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"photography-carousel\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"photography-carousel\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"photography-carousel\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"photography-carousel\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"photography-carousel\\/slide8.jpg\"},{\"title\":\"Slide 9\",\"img\":\"photography-carousel\\/slide9.jpg\"},{\"title\":\"Slide 10\",\"img\":\"photography-carousel\\/slide10.jpg\"}],\"search-form-hero\":[{\"title\":\"Slide 1\",\"img\":\"search-form-hero\\/slide1.jpg\"}],\"showcasecarousel\":[{\"title\":\"Slide 1\",\"img\":\"showcasecarousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"showcasecarousel\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"showcasecarousel\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"showcasecarousel\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"showcasecarousel\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"showcasecarousel\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"showcasecarousel\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"showcasecarousel\\/slide8.jpg\"},{\"title\":\"Slide 9\",\"img\":\"showcasecarousel\\/slide9.jpg\"}],\"sports-hero\":[{\"title\":\"Slide 1\",\"img\":\"sportshero\\/slide1.jpg\"}],\"twitter-feed\":[{\"title\":\"Slide 1\",\"img\":\"twitter-feed\\/slide1.jpg\"}],\"vimeo-gallery\":[{\"title\":\"Slide 1\",\"img\":\"vimeo-gallery\\/slide1.jpg\"}],\"vimeohero\":[{\"title\":\"Slide 1\",\"img\":\"vimeohero\\/slide1.jpg\"}],\"web-product-dark\":[{\"title\":\"Slide 1\",\"img\":\"web-product-dark\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"web-product-dark\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"web-product-dark\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"web-product-dark\\/slide4.jpg\"}],\"web-product-dark-hero\":[{\"title\":\"Slide 1\",\"img\":\"web-product-dark-hero\\/slide1.jpg\"}],\"web-product-light-hero\":[{\"title\":\"Slide 1\",\"img\":\"web-product-light-hero\\/slide1.jpg\"}],\"webproductlight\":[{\"title\":\"Slide 1\",\"img\":\"webproductlight\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"webproductlight\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"webproductlight\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"webproductlight\\/slide4.jpg\"}],\"youtube-gallery\":[{\"title\":\"Slide 1\",\"img\":\"youtube-gallery\\/slide1.jpg\"}],\"youtubehero\":[{\"title\":\"Slide 1\",\"img\":\"youtubehero\\/slide1.jpg\"}],\"scroll-effect\":[{\"title\":\"Slide 1\",\"img\":\"scrolleffect\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"scrolleffect\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"scrolleffect\\/slide3.jpg\"}],\"content-zoom\":[{\"title\":\"Slide 1\",\"img\":\"contentzoom\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"contentzoom\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"contentzoom\\/slide3.jpg\"}],\"food-carousel\":[{\"title\":\"Slide 1\",\"img\":\"foodcarousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"foodcarousel\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"foodcarousel\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"foodcarousel\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"foodcarousel\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"foodcarousel\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"foodcarousel\\/slide7.jpg\"}],\"rotating-words\":[{\"title\":\"Slide 1\",\"img\":\"rotatingwords\\/slide1.jpg\"}],\"travel-static-captions\":[{\"title\":\"Slide 1\",\"img\":\"travel-static-captions\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"travel-static-captions\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"travel-static-captions\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"travel-static-captions\\/slide4.jpg\"}],\"concept\":[{\"title\":\"Slide 1\",\"img\":\"concept\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"concept\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"concept\\/slide3.jpg\"}],\"fullscreen-button\":[{\"title\":\"Slide 1\",\"img\":\"fullscreen-button\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fullscreen-button\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"fullscreen-button\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"fullscreen-button\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"fullscreen-button\\/slide5.jpg\"}],\"creativefreedom\":[{\"title\":\"Slide 1\",\"img\":\"creativefreedom\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"creativefreedom\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"creativefreedom\\/slide3.jpg\"}],\"parallaxscene\":[{\"title\":\"Slide 1\",\"img\":\"parallaxscene\\/slide1.jpg\"}],\"slidingoverlays\":[{\"title\":\"Slide 1\",\"img\":\"slidingoverlays\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"slidingoverlays\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"slidingoverlays\\/slide3.jpg\"}],\"web-product-light-hero-3d\":[{\"title\":\"Slide 1\",\"img\":\"web-product-light-hero-3d\\/slide1.jpg\"}],\"woobig\":[{\"title\":\"Slide 1\",\"img\":\"woobig\\/slide1.jpg\"}],\"woocommercesmall\":[{\"title\":\"Slide 1\",\"img\":\"woocommercesmall\\/slide1.jpg\"}],\"finedining\":[{\"title\":\"Slide 1\",\"img\":\"finedining\\/slide1.jpg\"}],\"agency-intro\":[{\"title\":\"Slide 1\",\"img\":\"agency-intro\\/slide1.jpg\"}],\"ourteam\":[{\"title\":\"Slide 1\",\"img\":\"ourteam\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"ourteam\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"ourteam\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"ourteam\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"ourteam\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"ourteam\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"ourteam\\/slide7.jpg\"}],\"our-team-carousel\":[{\"title\":\"Slide 1\",\"img\":\"ourteamcarousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"ourteamcarousel\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"ourteamcarousel\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"ourteamcarousel\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"ourteamcarousel\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"ourteamcarousel\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"ourteamcarousel\\/slide7.jpg\"}],\"betteryoutube\":[{\"title\":\"Slide 1\",\"img\":\"betteryoutube\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"betteryoutube\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"betteryoutube\\/slide3.jpg\"}],\"agencywebsiteheader\":[{\"title\":\"Slide 1\",\"img\":\"agencywebsiteheader\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"agencywebsiteheader\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"agencywebsiteheader\\/slide3.jpg\"}],\"comingsoon\":[{\"title\":\"Slide 1\",\"img\":\"comingsoon\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"comingsoon\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"comingsoon\\/slide3.jpg\"}],\"snowscene\":[{\"title\":\"Slide 1\",\"img\":\"snowscene\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"snowscene\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"snowscene\\/slide3.jpg\"}],\"rockband\":[{\"title\":\"Slide 1\",\"img\":\"rockband\\/slide1.jpg\"}],\"sleeklandingpage\":[{\"title\":\"Slide 1\",\"img\":\"sleeklandingpage\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"sleeklandingpage\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"sleeklandingpage\\/slide3.jpg\"}],\"applandingpage\":[{\"title\":\"Slide 1\",\"img\":\"applandingpage\\/slide1.jpg\"}],\"deskscene\":[{\"title\":\"Slide 1\",\"img\":\"deskscene\\/slide1.jpg\"}],\"cleannewsslider\":[{\"title\":\"Slide 1\",\"img\":\"cleannewsslider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"cleannewsslider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"cleannewsslider\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"cleannewsslider\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"cleannewsslider\\/slide5.jpg\"}],\"imagegallery\":[{\"title\":\"Slide 1\",\"img\":\"imagegallery\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"imagegallery\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"imagegallery\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"imagegallery\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"imagegallery\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"imagegallery\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"imagegallery\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"imagegallery\\/slide8.jpg\"}],\"standard-wp-gallery\":[{\"title\":\"Slide 1\",\"img\":\"standard-wp-gallery\\/slide1.jpg\"}],\"clean-news-post-based\":[{\"title\":\"Slide 1\",\"img\":\"clean-news-post-based\\/slide1.jpg\"}],\"interactivewhiteboard\":[{\"title\":\"Slide 1\",\"img\":\"interactivewhiteboard\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"interactivewhiteboard\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"interactivewhiteboard\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"interactivewhiteboard\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"interactivewhiteboard\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"interactivewhiteboard\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"interactivewhiteboard\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"interactivewhiteboard\\/slide8.jpg\"}],\"innovation\":[{\"title\":\"Slide 1\",\"img\":\"innovation\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"innovation\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"innovation\\/slide3.jpg\"}],\"dark-fullsite-block-menu\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-menu\\/slide1.jpg\"}],\"dark-fullsite-block-1\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-1\\/slide1.jpg\"}],\"dark-fullsite-block-2-about\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-2-about\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"dark-fullsite-block-2-about\\/slide2.jpg\"}],\"dark-fullsite-block-3-services\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-3-services\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"dark-fullsite-block-3-services\\/slide2.jpg\"}],\"dark-fullsite-block-4-products\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-4-products\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"dark-fullsite-block-4-products\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"dark-fullsite-block-4-products\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"dark-fullsite-block-4-products\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"dark-fullsite-block-4-products\\/slide5.jpg\"}],\"dark-fullsite-block-5-testimonials\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-5-testimonials\\/slide1.jpg\"}],\"dark-fullsite-block-6-callout\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-6-callout\\/slide1.jpg\"}],\"dark-fullsite-block-7-footer\":[{\"title\":\"Slide 1\",\"img\":\"dark-fullsite-block-7-footer\\/slide1.jpg\"}],\"fullsite-block-menu\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-menu\\/slide1.jpg\"}],\"fullsite-block1\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block1\\/slide1.jpg\"}],\"fullsite-block-2-about\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-2-about\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fullsite-block-2-about\\/slide2.jpg\"}],\"fullsite-block-3-services\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-3-services\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fullsite-block-3-services\\/slide2.jpg\"}],\"fullsite-block-4-products\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-4-products\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fullsite-block-4-products\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"fullsite-block-4-products\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"fullsite-block-4-products\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"fullsite-block-4-products\\/slide5.jpg\"}],\"fullsite-block-5-testimonials\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-5-testimonials\\/slide1.jpg\"}],\"fullsite-block-6-callout\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-6-callout\\/slide1.jpg\"}],\"fullsite-block-7-footer\":[{\"title\":\"Slide 1\",\"img\":\"fullsite-block-7-footer\\/slide1.jpg\"}],\"techjournal\":[{\"title\":\"Slide 1\",\"img\":\"techjournal\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"techjournal\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"techjournal\\/slide3.jpg\"}],\"cardealership\":[{\"title\":\"Slide 1\",\"img\":\"cardealership\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"cardealership\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"cardealership\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"cardealership\\/slide4.jpg\"}],\"fullscreenmenu\":[{\"title\":\"Slide 1\",\"img\":\"fullscreenmenu\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fullscreenmenu\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"fullscreenmenu\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"fullscreenmenu\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"fullscreenmenu\\/slide5.jpg\"}],\"creativefrontpage\":[{\"title\":\"Slide 1\",\"img\":\"creativefrontpage\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"creativefrontpage\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"creativefrontpage\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"creativefrontpage\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"creativefrontpage\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"creativefrontpage\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"creativefrontpage\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"creativefrontpage\\/slide8.jpg\"}],\"websitebuilder-menu\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-menu\\/slide1.jpg\"}],\"websitebuilder-hero\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-hero\\/slide1.jpg\"}],\"websitebuilder-clients\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-clients\\/slide1.jpg\"}],\"websitebuilder-services\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-services\\/slide1.jpg\"}],\"websitebuilder-discover\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-discover\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"websitebuilder-discover\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"websitebuilder-discover\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"websitebuilder-discover\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"websitebuilder-discover\\/slide5.jpg\"}],\"websitebuilder-slider\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-slider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"websitebuilder-slider\\/slide2.jpg\"}],\"websitebuilder-calltoaction\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-calltoaction\\/slide1.jpg\"}],\"websitebuilder-footer\":[{\"title\":\"Slide 1\",\"img\":\"websitebuilder-footer\\/slide1.jpg\"}],\"focusparallax\":[{\"title\":\"Slide 1\",\"img\":\"focusparallax\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"focusparallax\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"focusparallax\\/slide3.jpg\"}],\"duotone\":[{\"title\":\"Slide 1\",\"img\":\"duotone\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"duotone\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"duotone\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"duotone\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"duotone\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"duotone\\/slide6.jpg\"}],\"r_menu\":[{\"title\":\"Slide 1\",\"img\":\"r_menu\\/slide1.jpg\"}],\"rhero\":[{\"title\":\"Slide 1\",\"img\":\"r_hero\\/slide1.jpg\"}],\"r_about\":[{\"title\":\"Slide 1\",\"img\":\"r_about\\/slide1.jpg\"}],\"r_products\":[{\"title\":\"Slide 1\",\"img\":\"r_products\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"r_products\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"r_products\\/slide3.jpg\"}],\"r_info\":[{\"title\":\"Slide 1\",\"img\":\"r_info\\/slide1.jpg\"}],\"inspirationheader\":[{\"title\":\"Slide 1\",\"img\":\"inspirationheader\\/slide1.jpg\"}],\"magazineposts\":[{\"title\":\"Slide 1\",\"img\":\"magazineposts\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"magazineposts\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"magazineposts\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"magazineposts\\/slide4.jpg\"}],\"explorationheader\":[{\"title\":\"Slide 1\",\"img\":\"explorationheader\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"explorationheader\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"explorationheader\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"explorationheader\\/slide4.jpg\"}],\"typewritereffect\":[{\"title\":\"Slide 1\",\"img\":\"typewritereffect\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"typewritereffect\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"typewritereffect\\/slide3.jpg\"}],\"blendmodeheader\":[{\"title\":\"Slide 1\",\"img\":\"blendmodeheader\\/slide1.jpg\"}],\"themeplicity_menu\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_menu\\/slide1.jpg\"}],\"themeplicity_header\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_header\\/slide1.jpg\"}],\"themeplicity_offer\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_offer\\/slide1.jpg\"}],\"themeplicity_whatwedo\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_whatwedo\\/slide1.jpg\"}],\"themeplicity_projects\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_projects\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"themeplicity_projects\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"themeplicity_projects\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"themeplicity_projects\\/slide4.jpg\"},{\"title\":\"themeplicity_projects\\/slide5.jpg\",\"img\":\"Slide 5\"}],\"themeplicity_whatsgreat\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_whatsgreat\\/slide1.jpg\"}],\"themeplicity_tables\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_tables\\/slide1.jpg\"}],\"themeplicity_contactform\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_contactform\\/slide1.jpg\"}],\"themeplicity_footer\":[{\"title\":\"Slide 1\",\"img\":\"themeplicity_footer\\/slide1.jpg\"}],\"NiceAndClean_Menu\":[{\"title\":\"Slide 1\",\"img\":\"NiceAndClean_Menu\\/slide1.jpg\"}],\"NiceAndClean_Header\":[{\"title\":\"Slide 1\",\"img\":\"NiceAndClean_Header\\/slide1.jpg\"}],\"NiceAndClean_Services\":[{\"title\":\"Slide 1\",\"img\":\"NiceAndClean_Services\\/slide1.jpg\"}],\"NiceAndClean_About\":[{\"title\":\"Slide 1\",\"img\":\"NiceAndClean_About\\/slide1.jpg\"}],\"niceandclean_video\":[{\"title\":\"Slide 1\",\"img\":\"niceandclean_video\\/slide1.jpg\"}],\"niceandclean_highlights\":[{\"title\":\"Slide 1\",\"img\":\"niceandclean_highlights\\/slide1.jpg\"}],\"NiceAndClean_Projects\":[{\"title\":\"Slide 1\",\"img\":\"NiceAndClean_Projects\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"NiceAndClean_Projects\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"NiceAndClean_Projects\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"NiceAndClean_Projects\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"NiceAndClean_Projects\\/slide5.jpg\"}],\"niceandclean_textblocks\":[{\"title\":\"Slide 1\",\"img\":\"niceandclean_textblocks\\/slide1.jpg\"}],\"niceandclean_callout\":[{\"title\":\"Slide 1\",\"img\":\"niceandclean_callout\\/slide1.jpg\"}],\"niceandclean_footer\":[{\"title\":\"Slide 1\",\"img\":\"niceandclean_footer\\/slide1.jpg\"}],\"80s\":[{\"title\":\"Slide 1\",\"img\":\"80s\\/slide1.jpg\"}],\"blurslider\":[{\"title\":\"Slide 1\",\"img\":\"blurslider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"blurslider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"blurslider\\/slide3.jpg\"}],\"ComingSoonAddon\":[{\"title\":\"Slide 1\",\"img\":\"ComingSoonAddon\\/slide1.jpg\"}],\"snowaddon\":[{\"title\":\"Slide 1\",\"img\":\"snowaddon\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"snowaddon\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"snowaddon\\/slide3.jpg\"}],\"particle-effect-one\":[{\"title\":\"Slide 1\",\"img\":\"particle-effect-one\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"particle-effect-one\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"particle-effect-one\\/slide3.jpg\"}],\"particle-effect-two\":[{\"title\":\"Slide 1\",\"img\":\"particle-effect-two\\/slide1.jpg\"}],\"particle-effect-three\":[{\"title\":\"Slide 1\",\"img\":\"particle-effect-three\\/slide1.jpg\"}],\"portfolioviewer\":[{\"title\":\"Slide 1\",\"img\":\"portfolioviewer\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"portfolioviewer\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"portfolioviewer\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"portfolioviewer\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"portfolioviewer\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"portfolioviewer\\/slide6.jpg\"}],\"appshowcase\":[{\"title\":\"Slide 1\",\"img\":\"appshowcase\\/slide1.jpg\"}],\"gravitydesign\":[{\"title\":\"Slide 1\",\"img\":\"gravitydesign\\/slide1.jpg\"}],\"404errorpage\":[{\"title\":\"Slide 1\",\"img\":\"404errorpage\\/slide1.jpg\"}],\"carouselgallery\":[{\"title\":\"Slide 1\",\"img\":\"carouselgallery\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"carouselgallery\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"carouselgallery\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"carouselgallery\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"carouselgallery\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"carouselgallery\\/slide6.jpg\"}],\"filmstrip\":[{\"title\":\"Slide 1\",\"img\":\"filmstrip\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"filmstrip\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"filmstrip\\/slide3.jpg\"}],\"spaceopera\":[{\"title\":\"Slide 1\",\"img\":\"spaceopera\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"spaceopera\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"spaceopera\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"spaceopera\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"spaceopera\\/slide5.jpg\"}],\"websiteintro\":[{\"title\":\"Slide 1\",\"img\":\"websiteintro\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"websiteintro\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"websiteintro\\/slide3.jpg\"}],\"maskshowcase\":[{\"title\":\"Slide 1\",\"img\":\"maskshowcase\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"maskshowcase\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"maskshowcase\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"maskshowcase\\/slide4.jpg\"}],\"parallaxzoomslices\":[{\"title\":\"Slide 1\",\"img\":\"parallaxzoomslices\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"parallaxzoomslices\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"parallaxzoomslices\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"parallaxzoomslices\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"parallaxzoomslices\\/slide5.jpg\"}],\"doubleexposureeffect\":[{\"title\":\"Slide 1\",\"img\":\"doubleexposureeffect\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"doubleexposureeffect\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"doubleexposureeffect\\/slide3.jpg\"}],\"mountainparallaxheader\":[{\"title\":\"Slide 1\",\"img\":\"mountainparallaxheader\\/slide1.jpg\"}],\"goodnewsmenu\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsmenu\\/slide1.jpg\"}],\"goodnewsheader\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsheader\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"goodnewsheader\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"goodnewsheader\\/slide3.jpg\"}],\"goodnewswhatshot\":[{\"title\":\"Slide 1\",\"img\":\"goodnewswhatshot\\/slide1.jpg\"}],\"goodnewsfeatured\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsfeatured\\/slide1.jpg\"}],\"goodnewsspotlight\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsspotlight\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"goodnewsspotlight\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"goodnewsspotlight\\/slide3.jpg\"}],\"goodnewscarousel\":[{\"title\":\"Slide 1\",\"img\":\"goodnewscarousel\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"goodnewscarousel\\/slide2.jpg\"}],\"goodnewscallout\":[{\"title\":\"Slide 1\",\"img\":\"goodnewscallout\\/slide1.jpg\"}],\"goodnewsfooter\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsfooter\\/slide1.jpg\"}],\"goodnewsmenuback\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsmenuback\\/slide1.jpg\"}],\"goodnewsblogheader\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsblogheader\\/slide1.jpg\"}],\"goodnewsblogcontent\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsblogcontent\\/slide1.jpg\"}],\"goodnewstestimonials\":[{\"title\":\"Slide 1\",\"img\":\"goodnewstestimonials\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"goodnewstestimonials\\/slide2.jpg\"}],\"goodnewsblogfooter\":[{\"title\":\"Slide 1\",\"img\":\"goodnewsblogfooter\\/slide1.jpg\"}],\"beforeafterslider\":[{\"title\":\"Slide 1\",\"img\":\"beforeafterslider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"beforeafterslider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"beforeafterslider\\/slide3.jpg\"}],\"productshowcase\":[{\"title\":\"Slide 1\",\"img\":\"productshowcase\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"productshowcase\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"productshowcase\\/slide3.jpg\"}],\"overexposuretransition\":[{\"title\":\"Slide 1\",\"img\":\"overexposuretransition\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"overexposuretransition\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"overexposuretransition\\/slide3.jpg\"}],\"parallaxscroll\":[{\"title\":\"Slide 1\",\"img\":\"parallaxscroll\\/slide1.jpg\"}],\"techco-menu\":[{\"title\":\"Slide 1\",\"img\":\"techco-menu\\/slide1.jpg\"}],\"techco-header\":[{\"title\":\"Slide 1\",\"img\":\"techco-header\\/slide1.jpg\"}],\"techco-about\":[{\"title\":\"Slide 1\",\"img\":\"techco-about\\/slide1.jpg\"}],\"techco-services\":[{\"title\":\"Slide 1\",\"img\":\"techco-services\\/slide1.jpg\"}],\"techco-video\":[{\"title\":\"Slide 1\",\"img\":\"techco-video\\/slide1.jpg\"}],\"techco-prices\":[{\"title\":\"Slide 1\",\"img\":\"techco-prices\\/slide1.jpg\"}],\"techco-testimonials\":[{\"title\":\"Slide 1\",\"img\":\"techco-testimonials\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"techco-testimonials\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"techco-testimonials\\/slide3.jpg\"}],\"techco-footer\":[{\"title\":\"Slide 1\",\"img\":\"techco-footer\\/slide1.jpg\"}],\"weather\":[{\"title\":\"Slide 1\",\"img\":\"weather\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"weather\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"weather\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"weather\\/slide4.jpg\"}],\"360panorama\":[{\"title\":\"Slide 1\",\"img\":\"360panorama\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"360panorama\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"360panorama\\/slide3.jpg\"}],\"duotone-add-on\":[{\"title\":\"Slide 1\",\"img\":\"duotone-add-on\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"duotone-add-on\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"duotone-add-on\\/slide3.jpg\"}],\"reveal-add-on\":[{\"title\":\"Slide 1\",\"img\":\"reveal-add-on\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"reveal-add-on\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"reveal-add-on\\/slide3.jpg\"}],\"band-tour-poster\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/band-tour-poster\\/slide1.png\"}],\"brewery-slider\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/brewery-slider\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/brewery-slider\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/brewery-slider\\/slide3.png\"}],\"burgerbar\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/burgerbar\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/burgerbar\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/burgerbar\\/slide3.png\"}],\"burger-bar-portrait\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/burger-bar-portrait\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/burger-bar-portrait\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/burger-bar-portrait\\/slide3.png\"}],\"car-rent\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/car-rent\\/slide1.png\"}],\"coffee\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/coffee\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/coffee\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/coffee\\/slide3.png\"}],\"holiday-advert\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/holiday-advert\\/slide1.png\"}],\"iphone-cases-light\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/iphone-cases-light\\/slide1.png\"}],\"medical\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/medical\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/medical\\/slide2.png\"}],\"mexican-grill\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/mexican-grill\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/mexican-grill\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/mexican-grill\\/slide3.png\"}],\"mobile-retail-slider\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/mobile-retail-slider\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/mobile-retail-slider\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/mobile-retail-slider\\/slide3.png\"}],\"money-exchange\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/money-exchange\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/money-exchange\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/money-exchange\\/slide3.png\"}],\"restaurant-menu-first-screen\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/restaurant-menu-first-screen\\/slide1.png\"}],\"restaurant-menu-second-screen\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/restaurant-menu-second-screen\\/slide1.png\"}],\"restaurant-menu-third-screen\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/restaurant-menu-third-screen\\/slide1.png\"}],\"shoes-store\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/shoes-store\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/shoes-store\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/shoes-store\\/slide3.png\"}],\"supermarket-products\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/supermarket-products\\/slide1.png\"},{\"title\":\"Slide 2\",\"img\":\"smartcontent\\/supermarket-products\\/slide2.png\"},{\"title\":\"Slide 3\",\"img\":\"smartcontent\\/supermarket-products\\/slide3.png\"},{\"title\":\"Slide 4\",\"img\":\"smartcontent\\/supermarket-products\\/slide4.png\"},{\"title\":\"Slide 5\",\"img\":\"smartcontent\\/supermarket-products\\/slide5.png\"}],\"travel-insurance\":[{\"title\":\"Slide 1\",\"img\":\"smartcontent\\/travel-insurance\\/slide1.png\"}],\"cryptoslider\":[{\"title\":\"Slide 1\",\"img\":\"cryptoslider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"cryptoslider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"cryptoslider\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"cryptoslider\\/slide4.jpg\"}],\"immersion_header\":[{\"title\":\"Slide 1\",\"img\":\"immersion_header\\/slide1.jpg\"}],\"immersion-mountains\":[{\"title\":\"Slide 1\",\"img\":\"immersion-mountains\\/slide1.jpg\"}],\"immersion-product\":[{\"title\":\"Slide 1\",\"img\":\"immersion-product\\/slide1.jpg\"}],\"immersion-design\":[{\"title\":\"Slide 1\",\"img\":\"immersion-design\\/slide1.jpg\"}],\"immersion-photography\":[{\"title\":\"Slide 1\",\"img\":\"immersion-photography\\/slide1.jpg\"}],\"immersion-grid\":[{\"title\":\"Slide 1\",\"img\":\"immersion-grid\\/slide1.jpg\"}],\"funkyslider\":[{\"title\":\"Slide 1\",\"img\":\"funkyslider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"funkyslider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"funkyslider\\/slide3.jpg\"}],\"clearview_menu\":[{\"title\":\"Slide 1\",\"img\":\"clearview_menu\\/slide1.jpg\"}],\"clearview_header\":[{\"title\":\"Slide 1\",\"img\":\"clearview_header\\/slide1.jpg\"}],\"clearview_mission\":[{\"title\":\"Slide 1\",\"img\":\"clearview_mission\\/slide1.jpg\"}],\"clear-view-slider\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-slider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"clear-view-slider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"clear-view-slider\\/slide3.jpg\"}],\"clear-view-news\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-news\\/slide1.jpg\"}],\"clear-view-clients\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-clients\\/slide1.jpg\"}],\"clear-view-contact\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-contact\\/slide1.jpg\"}],\"clear-view-single-menu\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-single-menu\\/slide1.jpg\"}],\"clear-view-single-header\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-single-header\\/slide1.jpg\"}],\"clear-view-single-media\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-single-media\\/slide1.jpg\"}],\"clear-view-single-more\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-single-more\\/slide1.jpg\"}],\"clear-view-single-contact\":[{\"title\":\"Slide 1\",\"img\":\"clear-view-single-contact\\/slide1.jpg\"}],\"cleanlandingpage\":[{\"title\":\"Slide 1\",\"img\":\"cleanlandingpage\\/slide1.jpg\"}],\"clearcut\":[{\"title\":\"Slide 1\",\"img\":\"clearcut\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"clearcut\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"clearcut\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"clearcut\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"clearcut\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"clearcut\\/slide6.jpg\"},{\"title\":\"Slide 7\",\"img\":\"clearcut\\/slide7.jpg\"},{\"title\":\"Slide 8\",\"img\":\"clearcut\\/slide8.jpg\"},{\"title\":\"Slide 9\",\"img\":\"clearcut\\/slide9.jpg\"},{\"title\":\"Slide 10\",\"img\":\"clearcut\\/slide10.jpg\"}],\"wonderstruck_menu\":[{\"title\":\"Slide 1\",\"img\":\"wonderstruck_menu\\/slide1.jpg\"}],\"wonderstruck_header\":[{\"title\":\"Slide 1\",\"img\":\"wonderstruck_header\\/slide1.jpg\"}],\"wonderstruck_about\":[{\"title\":\"Slide 1\",\"img\":\"wonderstruck_about\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"wonderstruck_about\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"wonderstruck_about\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"wonderstruck_about\\/slide4.jpg\"}],\"wonderstruck-works\":[{\"title\":\"Slide 1\",\"img\":\"wonderstruck-works\\/slide1.jpg\"}],\"wonderstruck-contact\":[{\"title\":\"Slide 1\",\"img\":\"wonderstruck-contact\\/slide1.jpg\"}],\"bubblemorph\":[{\"title\":\"Slide 1\",\"img\":\"bubblemorph\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"bubblemorph\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"bubblemorph\\/slide3.jpg\"}],\"distortionaddon\":[{\"title\":\"Slide 1\",\"img\":\"distortionaddon\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"distortionaddon\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"distortionaddon\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"distortionaddon\\/slide4.jpg\"},{\"title\":\"Slide 5\",\"img\":\"distortionaddon\\/slide5.jpg\"},{\"title\":\"Slide 6\",\"img\":\"distortionaddon\\/slide6.jpg\"}],\"clubflyer\":[{\"title\":\"Slide 1\",\"img\":\"clubflyer\\/slide1.jpg\"}],\"paintbrushaddon\":[{\"title\":\"Slide 1\",\"img\":\"paintbrushaddon\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"paintbrushaddon\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"paintbrushaddon\\/slide3.jpg\"}],\"parallax_header\":[{\"title\":\"Slide 1\",\"img\":\"parallax_header\\/slide1.jpg\"}],\"parallax_content\":[{\"title\":\"Slide 1\",\"img\":\"parallax_content\\/slide1.jpg\"}],\"parallax_footer\":[{\"title\":\"Slide 1\",\"img\":\"parallax_footer\\/slide1.jpg\"}],\"le-chef-menu\":[{\"title\":\"Slide 1\",\"img\":\"le-chef-menu\\/slide1.jpg\"}],\"le-chef-header\":[{\"title\":\"Slide 1\",\"img\":\"le-chef-header\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"le-chef-header\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"le-chef-header\\/slide3.jpg\"}],\"le-chef-philosophy\":[{\"title\":\"Slide 1\",\"img\":\"le-chef-philosophy\\/slide1.jpg\"}],\"le-chef-food\":[{\"title\":\"Slide 1\",\"img\":\"le-chef-food\\/slide1.jpg\"}],\"le-chef-la-carte\":[{\"title\":\"Slide 1\",\"img\":\"le-chef-la-carte\\/slide1.jpg\"}],\"le-chef-footer\":[{\"title\":\"Slide 1\",\"img\":\"le-chef-footer\\/slide1.jpg\"}],\"news-magazine-slider\":[{\"title\":\"Slide 1\",\"img\":\"news-magazine-slider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"news-magazine-slider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"news-magazine-slider\\/slide3.jpg\"},{\"title\":\"Slide 4\",\"img\":\"news-magazine-slider\\/slide4.jpg\"}],\"real-estate-slider\":[{\"title\":\"Slide 1\",\"img\":\"real-estate-slider\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"real-estate-slider\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"real-estate-slider\\/slide3.jpg\"}],\"fashion-header\":[{\"title\":\"Slide 1\",\"img\":\"fashion-header\\/slide1.jpg\"},{\"title\":\"Slide 2\",\"img\":\"fashion-header\\/slide2.jpg\"},{\"title\":\"Slide 3\",\"img\":\"fashion-header\\/slide3.jpg\"}]}}');

-- --------------------------------------------------------

--
-- Table structure for table `revslider_sliders`
--

CREATE TABLE `revslider_sliders` (
  `id` int(11) NOT NULL,
  `title` tinytext NOT NULL,
  `alias` tinytext,
  `params` longtext NOT NULL,
  `settings` text,
  `type` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revslider_slides`
--

CREATE TABLE `revslider_slides` (
  `id` int(11) NOT NULL,
  `slider_id` int(9) NOT NULL,
  `slide_order` int(11) NOT NULL,
  `params` longtext NOT NULL,
  `layers` longtext NOT NULL,
  `settings` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revslider_static_slides`
--

CREATE TABLE `revslider_static_slides` (
  `id` int(11) NOT NULL,
  `slider_id` int(9) NOT NULL,
  `params` longtext NOT NULL,
  `layers` longtext NOT NULL,
  `settings` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_url`
--

CREATE TABLE `seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo_url`
--

INSERT INTO `seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(824, 0, 1, 'product_id=48', 'ipod-classic'),
(836, 0, 1, 'category_id=20', 'desktops'),
(834, 0, 1, 'category_id=26', 'pc'),
(835, 0, 1, 'category_id=27', 'mac'),
(730, 0, 1, 'manufacturer_id=8', 'apple'),
(772, 0, 1, 'information_id=4', 'about_us'),
(768, 0, 1, 'product_id=42', 'test'),
(789, 0, 1, 'category_id=34', 'mp3-players'),
(781, 0, 1, 'category_id=36', 'test2'),
(774, 0, 1, 'category_id=18', 'laptop-notebook'),
(775, 0, 1, 'category_id=46', 'macs'),
(776, 0, 1, 'category_id=45', 'windows'),
(777, 0, 1, 'category_id=25', 'component'),
(778, 0, 1, 'category_id=29', 'mouse'),
(779, 0, 1, 'category_id=28', 'monitor'),
(780, 0, 1, 'category_id=35', 'test1'),
(782, 0, 1, 'category_id=30', 'printer'),
(783, 0, 1, 'category_id=31', 'scanner'),
(784, 0, 1, 'category_id=32', 'web-camera'),
(785, 0, 1, 'category_id=57', 'tablet'),
(786, 0, 1, 'category_id=17', 'software'),
(787, 0, 1, 'category_id=24', 'smartphone'),
(788, 0, 1, 'category_id=33', 'camera'),
(790, 0, 1, 'category_id=43', 'test11'),
(791, 0, 1, 'category_id=44', 'test12'),
(792, 0, 1, 'category_id=47', 'test15'),
(793, 0, 1, 'category_id=48', 'test16'),
(794, 0, 1, 'category_id=49', 'test17'),
(795, 0, 1, 'category_id=50', 'test18'),
(796, 0, 1, 'category_id=51', 'test19'),
(797, 0, 1, 'category_id=52', 'test20'),
(798, 0, 1, 'category_id=58', 'test25'),
(799, 0, 1, 'category_id=53', 'test21'),
(800, 0, 1, 'category_id=54', 'test22'),
(801, 0, 1, 'category_id=55', 'test23'),
(802, 0, 1, 'category_id=56', 'test24'),
(803, 0, 1, 'category_id=38', 'test4'),
(804, 0, 1, 'category_id=37', 'test5'),
(805, 0, 1, 'category_id=39', 'test6'),
(806, 0, 1, 'category_id=40', 'test7'),
(807, 0, 1, 'category_id=41', 'test8'),
(808, 0, 1, 'category_id=42', 'test9'),
(809, 0, 1, 'product_id=30', 'canon-eos-5d'),
(840, 0, 1, 'product_id=47', 'hp-lp3065'),
(811, 0, 1, 'product_id=28', 'htc-touch-hd'),
(812, 0, 1, 'product_id=43', 'macbook'),
(813, 0, 1, 'product_id=44', 'macbook-air'),
(814, 0, 1, 'product_id=45', 'macbook-pro'),
(816, 0, 1, 'product_id=31', 'nikon-d300'),
(817, 0, 1, 'product_id=29', 'palm-treo-pro'),
(818, 0, 1, 'product_id=35', 'product-8'),
(819, 0, 1, 'product_id=49', 'samsung-galaxy-tab-10-1'),
(820, 0, 1, 'product_id=33', 'samsung-syncmaster-941bw'),
(821, 0, 1, 'product_id=46', 'sony-vaio'),
(837, 0, 1, 'product_id=41', 'imac'),
(823, 0, 1, 'product_id=40', 'iphone'),
(825, 0, 1, 'product_id=36', 'ipod-nano'),
(826, 0, 1, 'product_id=34', 'ipod-shuffle'),
(827, 0, 1, 'product_id=32', 'ipod-touch'),
(828, 0, 1, 'manufacturer_id=9', 'canon'),
(829, 0, 1, 'manufacturer_id=5', 'htc'),
(830, 0, 1, 'manufacturer_id=7', 'hewlett-packard'),
(831, 0, 1, 'manufacturer_id=6', 'palm'),
(832, 0, 1, 'manufacturer_id=10', 'sony'),
(841, 0, 1, 'information_id=6', 'delivery'),
(842, 0, 1, 'information_id=3', 'privacy'),
(843, 0, 1, 'information_id=5', 'terms');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `data`, `expire`) VALUES
('67c575170d797d92a9d848d583', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"X4V0z0wpXK4VnidJ0E6hCcxdeMY8swfJ\"}', '2018-11-07 17:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(2, 0, 'config', 'config_shared', '0', 0),
(3, 0, 'config', 'config_secure', '0', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(6, 0, 'config', 'config_fraud_detection', '0', 0),
(7, 0, 'config', 'config_ftp_status', '0', 0),
(8, 0, 'config', 'config_ftp_root', '', 0),
(9, 0, 'config', 'config_ftp_password', '', 0),
(10, 0, 'config', 'config_ftp_username', '', 0),
(11, 0, 'config', 'config_ftp_port', '21', 0),
(12, 0, 'config', 'config_ftp_hostname', '', 0),
(13, 0, 'config', 'config_meta_title', 'Your Store', 0),
(14, 0, 'config', 'config_meta_description', 'My Store', 0),
(15, 0, 'config', 'config_meta_keyword', '', 0),
(16, 0, 'config', 'config_theme', 'default', 0),
(17, 0, 'config', 'config_layout_id', '4', 0),
(18, 0, 'config', 'config_country_id', '222', 0),
(19, 0, 'config', 'config_zone_id', '3563', 0),
(20, 0, 'config', 'config_language', 'en-gb', 0),
(21, 0, 'config', 'config_admin_language', 'en-gb', 0),
(22, 0, 'config', 'config_currency', 'USD', 0),
(23, 0, 'config', 'config_currency_auto', '1', 0),
(24, 0, 'config', 'config_length_class_id', '1', 0),
(25, 0, 'config', 'config_weight_class_id', '1', 0),
(26, 0, 'config', 'config_product_count', '1', 0),
(27, 0, 'config', 'config_limit_admin', '20', 0),
(28, 0, 'config', 'config_review_status', '1', 0),
(29, 0, 'config', 'config_review_guest', '1', 0),
(30, 0, 'config', 'config_voucher_min', '1', 0),
(31, 0, 'config', 'config_voucher_max', '1000', 0),
(32, 0, 'config', 'config_tax', '1', 0),
(33, 0, 'config', 'config_tax_default', 'shipping', 0),
(34, 0, 'config', 'config_tax_customer', 'shipping', 0),
(35, 0, 'config', 'config_customer_online', '0', 0),
(36, 0, 'config', 'config_customer_activity', '0', 0),
(37, 0, 'config', 'config_customer_search', '0', 0),
(38, 0, 'config', 'config_customer_group_id', '1', 0),
(39, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(40, 0, 'config', 'config_customer_price', '0', 0),
(41, 0, 'config', 'config_account_id', '3', 0),
(42, 0, 'config', 'config_invoice_prefix', 'INV-2018-00', 0),
(203, 0, 'config', 'config_api_id', '1', 0),
(44, 0, 'config', 'config_cart_weight', '1', 0),
(45, 0, 'config', 'config_checkout_guest', '1', 0),
(46, 0, 'config', 'config_checkout_id', '5', 0),
(47, 0, 'config', 'config_order_status_id', '1', 0),
(48, 0, 'config', 'config_processing_status', '[\"5\",\"1\",\"2\",\"12\",\"3\"]', 1),
(49, 0, 'config', 'config_complete_status', '[\"5\",\"3\"]', 1),
(50, 0, 'config', 'config_stock_display', '0', 0),
(51, 0, 'config', 'config_stock_warning', '0', 0),
(52, 0, 'config', 'config_stock_checkout', '0', 0),
(53, 0, 'config', 'config_affiliate_approval', '0', 0),
(54, 0, 'config', 'config_affiliate_auto', '0', 0),
(55, 0, 'config', 'config_affiliate_commission', '5', 0),
(56, 0, 'config', 'config_affiliate_id', '4', 0),
(57, 0, 'config', 'config_return_id', '0', 0),
(58, 0, 'config', 'config_return_status_id', '2', 0),
(59, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(60, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(61, 0, 'config', 'config_comment', '', 0),
(62, 0, 'config', 'config_open', '', 0),
(63, 0, 'config', 'config_image', '', 0),
(64, 0, 'config', 'config_fax', '', 0),
(65, 0, 'config', 'config_telephone', '123456789', 0),
(201, 0, 'config', 'config_email', 'admin@gmail.com', 0),
(67, 0, 'config', 'config_geocode', '', 0),
(68, 0, 'config', 'config_owner', 'Your Name', 0),
(69, 0, 'config', 'config_address', 'Address 1', 0),
(70, 0, 'config', 'config_name', 'Your Store', 0),
(71, 0, 'config', 'config_seo_url', '0', 0),
(72, 0, 'config', 'config_file_max_size', '300000', 0),
(73, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(74, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(75, 0, 'config', 'config_maintenance', '0', 0),
(76, 0, 'config', 'config_password', '1', 0),
(202, 0, 'config', 'config_encryption', 'nD6tlFlMyAI2pshtlSJYIYdkIdZt5jM384jRmFC21V47pXbMsL6jfTzRzhAmdnlK9idxdRUBbSUqxopZdjClpBMHkORrMCJebzmUcZmztgWLB6dJ84RUKG8HA8gXKgFautgHaIczqoOHkt5oc7efhzIvuLAkW8BqfNmQzQxDqDcrqmNcNmXwW8AsxmHrDQ1O0S2vKyLau8kxXj2YDnmHTL6Klg1g7TdQYOD5S5iLiadj9onnIAGFOtsPh4GlL4JUe7eHlnmvCyLJacZwAb3KZW5oYKRk37k8fkC8CqSVFeKDTRSikAdPhlgfOu8JQC6oQjYFgsAogVLjyGVkzriOkZRc82N5qN3I6u0fDmdmqjFruPKzTKTrxwqgyNeiIr5uLo68On7ctMddVWatQShSMTA9n3Uo7h2C21zzXDPJdDXe0dx3eZjX3ZKEzUjSqBbhvkgw6wIWuuBwGb5oifuIBKShcDttCYj2zEgZQ1YHt9X6IRlSZhGUF8Fz6FGa4HhNlDijuOXbCsxZJyUG8DErHMsGrTtx8usWpW5EkpFFK6y8PSFsm7YMWpQQgMFSW56iwHGrO6TBsMk0uG4eOuwUI645lMmt7KlIJSoUP4UdAW5PQCygSx4M3UHEqRREQqOAzooBX4XITL8NSjTHJyr5w7brYAdj853LSJfiJ29A9dukk6wOrrqFtvb7VfcgF4g9TUHu0iYmsSxPMFXf0Rt0m2Q2ypDAJBhJN6OlrzAvXZbljpT9GNWJqemE6YvEiRx42WiEUuxQBW8cYr6BJkI67hIGSIzLQuq762khZGP6BwBJzuGPUNK9oIO7cuTrqj7KQ74x7WGgkRlKsKA2nHiBu8ZXiWh7TiWeZoMBRnR7lfCd2yy3pTi8WuZnCdVADF6RQaWsx7GtiuQq8SnMTEJP6cULwmvTWaJxOM8QFPx59W8mPMce2WQRbYnj1EAEYPZJEMl9VjajyeekSGeCObdLBXTS28sMtvlQiiC5mhCFu70twhfmmUeZHERU9tToScQw', 0),
(78, 0, 'config', 'config_compression', '0', 0),
(79, 0, 'config', 'config_error_display', '1', 0),
(80, 0, 'config', 'config_error_log', '1', 0),
(81, 0, 'config', 'config_error_filename', 'error.log', 0),
(82, 0, 'config', 'config_google_analytics', '', 0),
(83, 0, 'config', 'config_mail_engine', 'mail', 0),
(84, 0, 'config', 'config_mail_parameter', '', 0),
(85, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(86, 0, 'config', 'config_mail_smtp_username', '', 0),
(87, 0, 'config', 'config_mail_smtp_password', '', 0),
(88, 0, 'config', 'config_mail_smtp_port', '25', 0),
(89, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(90, 0, 'config', 'config_mail_alert_email', '', 0),
(91, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(92, 0, 'config', 'config_captcha', 'basic', 0),
(93, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(94, 0, 'config', 'config_login_attempts', '5', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(103, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(104, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(105, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(106, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(107, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(123, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(134, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(138, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(146, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(199, 0, 'developer', 'developer_theme', '1', 0),
(200, 0, 'developer', 'developer_sass', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_courier`
--

CREATE TABLE `shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping_courier`
--

INSERT INTO `shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '0.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `stock_status`
--

CREATE TABLE `stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_status`
--

INSERT INTO `stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tax_class`
--

CREATE TABLE `tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_class`
--

INSERT INTO `tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate`
--

CREATE TABLE `tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_rate`
--

INSERT INTO `tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate_to_customer_group`
--

CREATE TABLE `tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_rate_to_customer_group`
--

INSERT INTO `tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_rule`
--

CREATE TABLE `tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_rule`
--

INSERT INTO `tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE `theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE `translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '43edb86b7a042f1fe0e86052cb8960e248eeb9cf', 'qoVX2KJBt', 'John', 'Doe', 'admin@gmail.com', '', '', '::1', 1, '2018-11-07 22:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/theme\",\"design\\/translation\",\"design\\/seo_url\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/marketing\\/remarketing\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/worldpay\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/payment\\/pp_braintree\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/module\\/advanced_grid\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/blog_popular\",\"extension\\/module\\/blog_tags\",\"extension\\/module\\/blog_related_post\",\"extension\\/module\\/blog_search\",\"extension\\/module\\/breadcrumb_background_image\",\"extension\\/module\\/camera_slider\",\"extension\\/module\\/carousel_item\",\"extension\\/module\\/category_wall\",\"extension\\/module\\/cookie\",\"extension\\/module\\/custom_module\",\"extension\\/module\\/faq\",\"extension\\/module\\/fastor\",\"extension\\/module\\/filter_product\",\"extension\\/module\\/full_screen_background_slider\",\"extension\\/module\\/header_notice\",\"extension\\/module\\/megamenu_manager_links\",\"extension\\/module\\/megamenu\",\"extension\\/module\\/newsletter\",\"extension\\/module\\/popup\",\"extension\\/module\\/product_blocks\",\"extension\\/module\\/product_questions\",\"extension\\/module\\/product_tabs\",\"extension\\/module\\/revslideropencart\",\"extension\\/module\\/revolution_slider\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/amazon_login\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/theme\",\"design\\/translation\",\"design\\/seo_url\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/marketing\\/remarketing\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/worldpay\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/payment\\/pp_braintree\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/event\",\"marketplace\\/api\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/module\\/advanced_grid\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/blog_popular\",\"extension\\/module\\/blog_tags\",\"extension\\/module\\/blog_related_post\",\"extension\\/module\\/blog_search\",\"extension\\/module\\/breadcrumb_background_image\",\"extension\\/module\\/camera_slider\",\"extension\\/module\\/carousel_item\",\"extension\\/module\\/category_wall\",\"extension\\/module\\/cookie\",\"extension\\/module\\/custom_module\",\"extension\\/module\\/faq\",\"extension\\/module\\/fastor\",\"extension\\/module\\/filter_product\",\"extension\\/module\\/full_screen_background_slider\",\"extension\\/module\\/header_notice\",\"extension\\/module\\/megamenu_manager_links\",\"extension\\/module\\/megamenu\",\"extension\\/module\\/newsletter\",\"extension\\/module\\/popup\",\"extension\\/module\\/product_blocks\",\"extension\\/module\\/product_questions\",\"extension\\/module\\/product_tabs\",\"extension\\/module\\/revslideropencart\",\"extension\\/module\\/revolution_slider\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/amazon_login\"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_history`
--

CREATE TABLE `voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_theme`
--

CREATE TABLE `voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher_theme`
--

INSERT INTO `voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_theme_description`
--

CREATE TABLE `voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher_theme_description`
--

INSERT INTO `voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `weight_class`
--

CREATE TABLE `weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `weight_class`
--

INSERT INTO `weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `weight_class_description`
--

CREATE TABLE `weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `weight_class_description`
--

INSERT INTO `weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Niederösterreich', 'NOS', 1),
(202, 14, 'Oberösterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-Württemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Thüringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'Kalimantan Utara', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1);
INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord-Brabant', 'NB', 1),
(2336, 150, 'Noord-Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid-Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'Gävleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'Jämtland', 'Z', 1),
(3080, 203, 'Jönköping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, 'Örebro', 'T', 1),
(3085, 203, 'Östergötland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'Södermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'Värmland', 'S', 1),
(3091, 203, 'Västerbotten', 'AC', 1),
(3092, 203, 'Västernorrland', 'Y', 1),
(3093, 203, 'Västmanland', 'U', 1),
(3094, 203, 'Västra Götaland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graubünden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Zürich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1);
INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(3506, 221, 'Abu Dhabi', 'ADH', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1),
(4232, 44, 'Qinghai', 'QH', 1),
(4233, 100, 'Papua Barat', 'PB', 1),
(4234, 100, 'Sulawesi Barat', 'SR', 1),
(4235, 100, 'Kepulauan Riau', 'KR', 1),
(4236, 105, 'Barletta-Andria-Trani', 'BT', 1),
(4237, 105, 'Fermo', 'FM', 1),
(4238, 105, 'Monza Brianza', 'MB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zone_to_geo_zone`
--

CREATE TABLE `zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zone_to_geo_zone`
--

INSERT INTO `zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `api`
--
ALTER TABLE `api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `api_ip`
--
ALTER TABLE `api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `api_session`
--
ALTER TABLE `api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `attribute_description`
--
ALTER TABLE `attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `attribute_group`
--
ALTER TABLE `attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `attribute_group_description`
--
ALTER TABLE `attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `banner_image`
--
ALTER TABLE `banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `category_description`
--
ALTER TABLE `category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `category_filter`
--
ALTER TABLE `category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `category_path`
--
ALTER TABLE `category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `category_to_layout`
--
ALTER TABLE `category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `category_to_store`
--
ALTER TABLE `category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `coupon_category`
--
ALTER TABLE `coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `coupon_history`
--
ALTER TABLE `coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `coupon_product`
--
ALTER TABLE `coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_activity`
--
ALTER TABLE `customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `customer_affiliate`
--
ALTER TABLE `customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_approval`
--
ALTER TABLE `customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `customer_group`
--
ALTER TABLE `customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `customer_group_description`
--
ALTER TABLE `customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `customer_history`
--
ALTER TABLE `customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `customer_ip`
--
ALTER TABLE `customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `customer_login`
--
ALTER TABLE `customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `customer_online`
--
ALTER TABLE `customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `customer_reward`
--
ALTER TABLE `customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `customer_search`
--
ALTER TABLE `customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `customer_transaction`
--
ALTER TABLE `customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `customer_wishlist`
--
ALTER TABLE `customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `custom_field`
--
ALTER TABLE `custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `custom_field_customer_group`
--
ALTER TABLE `custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `custom_field_description`
--
ALTER TABLE `custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `custom_field_value`
--
ALTER TABLE `custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `custom_field_value_description`
--
ALTER TABLE `custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `download_description`
--
ALTER TABLE `download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `extension`
--
ALTER TABLE `extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `extension_install`
--
ALTER TABLE `extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `extension_path`
--
ALTER TABLE `extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `filter_description`
--
ALTER TABLE `filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `filter_group`
--
ALTER TABLE `filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `filter_group_description`
--
ALTER TABLE `filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `geo_zone`
--
ALTER TABLE `geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `information_description`
--
ALTER TABLE `information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `information_to_layout`
--
ALTER TABLE `information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `information_to_store`
--
ALTER TABLE `information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `layout`
--
ALTER TABLE `layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `layout_module`
--
ALTER TABLE `layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `layout_route`
--
ALTER TABLE `layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `length_class`
--
ALTER TABLE `length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `length_class_description`
--
ALTER TABLE `length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `manufacturer_to_store`
--
ALTER TABLE `manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `marketing`
--
ALTER TABLE `marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `modification`
--
ALTER TABLE `modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `option_description`
--
ALTER TABLE `option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `option_value`
--
ALTER TABLE `option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `option_value_description`
--
ALTER TABLE `option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_history`
--
ALTER TABLE `order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `order_option`
--
ALTER TABLE `order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `order_recurring`
--
ALTER TABLE `order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `order_recurring_transaction`
--
ALTER TABLE `order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `order_shipment`
--
ALTER TABLE `order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `order_total`
--
ALTER TABLE `order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `order_voucher`
--
ALTER TABLE `order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_attribute`
--
ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `product_description`
--
ALTER TABLE `product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `product_discount`
--
ALTER TABLE `product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_filter`
--
ALTER TABLE `product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_option`
--
ALTER TABLE `product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `product_option_value`
--
ALTER TABLE `product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `product_recurring`
--
ALTER TABLE `product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `product_related`
--
ALTER TABLE `product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `product_reward`
--
ALTER TABLE `product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `product_special`
--
ALTER TABLE `product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_to_download`
--
ALTER TABLE `product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `product_to_layout`
--
ALTER TABLE `product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `product_to_store`
--
ALTER TABLE `product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `recurring`
--
ALTER TABLE `recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `recurring_description`
--
ALTER TABLE `recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `return`
--
ALTER TABLE `return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `return_action`
--
ALTER TABLE `return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Indexes for table `return_history`
--
ALTER TABLE `return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `return_reason`
--
ALTER TABLE `return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Indexes for table `return_status`
--
ALTER TABLE `return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `revslider_attachment_images`
--
ALTER TABLE `revslider_attachment_images`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `revslider_css`
--
ALTER TABLE `revslider_css`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revslider_layer_animations`
--
ALTER TABLE `revslider_layer_animations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revslider_navigations`
--
ALTER TABLE `revslider_navigations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revslider_options`
--
ALTER TABLE `revslider_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revslider_sliders`
--
ALTER TABLE `revslider_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revslider_slides`
--
ALTER TABLE `revslider_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revslider_static_slides`
--
ALTER TABLE `revslider_static_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_url`
--
ALTER TABLE `seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `shipping_courier`
--
ALTER TABLE `shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `stock_status`
--
ALTER TABLE `stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `tax_class`
--
ALTER TABLE `tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `tax_rate`
--
ALTER TABLE `tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `tax_rate_to_customer_group`
--
ALTER TABLE `tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `tax_rule`
--
ALTER TABLE `tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `translation`
--
ALTER TABLE `translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `voucher_history`
--
ALTER TABLE `voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `voucher_theme`
--
ALTER TABLE `voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `voucher_theme_description`
--
ALTER TABLE `voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `weight_class`
--
ALTER TABLE `weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `weight_class_description`
--
ALTER TABLE `weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `zone_to_geo_zone`
--
ALTER TABLE `zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `api_ip`
--
ALTER TABLE `api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api_session`
--
ALTER TABLE `api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute`
--
ALTER TABLE `attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attribute_group`
--
ALTER TABLE `attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `banner_image`
--
ALTER TABLE `banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupon_history`
--
ALTER TABLE `coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_product`
--
ALTER TABLE `coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_activity`
--
ALTER TABLE `customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_approval`
--
ALTER TABLE `customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_group`
--
ALTER TABLE `customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_history`
--
ALTER TABLE `customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_ip`
--
ALTER TABLE `customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_login`
--
ALTER TABLE `customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_reward`
--
ALTER TABLE `customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_search`
--
ALTER TABLE `customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_transaction`
--
ALTER TABLE `customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_field`
--
ALTER TABLE `custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_field_value`
--
ALTER TABLE `custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `extension`
--
ALTER TABLE `extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `extension_install`
--
ALTER TABLE `extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `extension_path`
--
ALTER TABLE `extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `filter`
--
ALTER TABLE `filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `filter_group`
--
ALTER TABLE `filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `geo_zone`
--
ALTER TABLE `geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `layout`
--
ALTER TABLE `layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `layout_module`
--
ALTER TABLE `layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `layout_route`
--
ALTER TABLE `layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `length_class`
--
ALTER TABLE `length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `marketing`
--
ALTER TABLE `marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modification`
--
ALTER TABLE `modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `option`
--
ALTER TABLE `option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `option_value`
--
ALTER TABLE `option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_history`
--
ALTER TABLE `order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_option`
--
ALTER TABLE `order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_recurring`
--
ALTER TABLE `order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_recurring_transaction`
--
ALTER TABLE `order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_shipment`
--
ALTER TABLE `order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `order_total`
--
ALTER TABLE `order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_voucher`
--
ALTER TABLE `order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `product_discount`
--
ALTER TABLE `product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=441;

--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2352;

--
-- AUTO_INCREMENT for table `product_option`
--
ALTER TABLE `product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `product_option_value`
--
ALTER TABLE `product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_reward`
--
ALTER TABLE `product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=546;

--
-- AUTO_INCREMENT for table `product_special`
--
ALTER TABLE `product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;

--
-- AUTO_INCREMENT for table `recurring`
--
ALTER TABLE `recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return`
--
ALTER TABLE `return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_action`
--
ALTER TABLE `return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `return_history`
--
ALTER TABLE `return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_reason`
--
ALTER TABLE `return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `return_status`
--
ALTER TABLE `return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revslider_attachment_images`
--
ALTER TABLE `revslider_attachment_images`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revslider_css`
--
ALTER TABLE `revslider_css`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `revslider_layer_animations`
--
ALTER TABLE `revslider_layer_animations`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revslider_navigations`
--
ALTER TABLE `revslider_navigations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revslider_options`
--
ALTER TABLE `revslider_options`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `revslider_sliders`
--
ALTER TABLE `revslider_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revslider_slides`
--
ALTER TABLE `revslider_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revslider_static_slides`
--
ALTER TABLE `revslider_static_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seo_url`
--
ALTER TABLE `seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=844;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_status`
--
ALTER TABLE `stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_class`
--
ALTER TABLE `tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tax_rate`
--
ALTER TABLE `tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `tax_rule`
--
ALTER TABLE `tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `theme`
--
ALTER TABLE `theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `voucher_history`
--
ALTER TABLE `voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `voucher_theme`
--
ALTER TABLE `voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `weight_class`
--
ALTER TABLE `weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4239;

--
-- AUTO_INCREMENT for table `zone_to_geo_zone`
--
ALTER TABLE `zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
